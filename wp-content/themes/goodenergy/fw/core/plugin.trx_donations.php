<?php
/* ThemeREX Donations support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('themerex_trx_donations_theme_setup')) {
	add_action( 'themerex_action_before_init_theme', 'themerex_trx_donations_theme_setup' );
	function themerex_trx_donations_theme_setup() {

		// Add shortcode in the shortcodes list
		if (themerex_exists_trx_donations()) {
			// Detect current page type, taxonomy and title (for custom post_types use priority < 10 to fire it handles early, than for standard post types)
			add_filter('themerex_filter_get_blog_type',			'themerex_trx_donations_get_blog_type', 9, 2);
			add_filter('themerex_filter_get_blog_title',		'themerex_trx_donations_get_blog_title', 9, 2);
			add_filter('themerex_filter_get_current_taxonomy',	'themerex_trx_donations_get_current_taxonomy', 9, 2);
			add_filter('themerex_filter_is_taxonomy',			'themerex_trx_donations_is_taxonomy', 9, 2);
			add_filter('themerex_filter_get_stream_page_title',	'themerex_trx_donations_get_stream_page_title', 9, 2);
			add_filter('themerex_filter_get_stream_page_link',	'themerex_trx_donations_get_stream_page_link', 9, 2);
			add_filter('themerex_filter_get_stream_page_id',	'themerex_trx_donations_get_stream_page_id', 9, 2);
			add_filter('themerex_filter_query_add_filters',		'themerex_trx_donations_query_add_filters', 9, 2);
			add_filter('themerex_filter_detect_inheritance_key','themerex_trx_donations_detect_inheritance_key', 9, 1);
			// Add shortcodes in the list
			add_action('themerex_action_shortcodes_list',		'themerex_trx_donations_reg_shortcodes');
			add_action('themerex_action_shortcodes_list_vc',	'themerex_trx_donations_reg_shortcodes_vc');
		}
	}
}

if ( !function_exists( 'themerex_trx_donations_settings_theme_setup2' ) ) {
	add_action( 'themerex_action_before_init_theme', 'themerex_trx_donations_settings_theme_setup2', 3 );
	function themerex_trx_donations_settings_theme_setup2() {
		// Add Donations post type and taxonomy into theme inheritance list
		if (themerex_exists_trx_donations()) {
			themerex_add_theme_inheritance( array('donations' => array(
				'stream_template' => 'blog-donations',
				'single_template' => 'single-donation',
				'taxonomy' => array(THEMEREX_Donations::TAXONOMY),
				'taxonomy_tags' => array(),
				'post_type' => array(THEMEREX_Donations::POST_TYPE),
				'override' => 'page'
				) )
			);
		}
	}
}

// Check if ThemeREX Donations installed and activated
if ( !function_exists( 'themerex_exists_trx_donations' ) ) {
	function themerex_exists_trx_donations() {
		return class_exists('THEMEREX_Donations');
	}
}


// Return true, if current page is donations page
if ( !function_exists( 'themerex_is_trx_donations_page' ) ) {
	function themerex_is_trx_donations_page() {
		return (is_single() && get_query_var('post_type') == THEMEREX_Donations::POST_TYPE) || is_post_type_archive(THEMEREX_Donations::POST_TYPE) || is_tax(THEMEREX_Donations::TAXONOMY);
	}
}

// Filter to detect current page inheritance key
if ( !function_exists( 'themerex_trx_donations_detect_inheritance_key' ) ) {
	//add_filter('themerex_filter_detect_inheritance_key',	'themerex_trx_donations_detect_inheritance_key', 9, 1);
	function themerex_trx_donations_detect_inheritance_key($key) {
		if (!empty($key)) return $key;
		return themerex_is_trx_donations_page() ? 'donations' : '';
	}
}

// Filter to detect current page slug
if ( !function_exists( 'themerex_trx_donations_get_blog_type' ) ) {
	//add_filter('themerex_filter_get_blog_type',	'themerex_trx_donations_get_blog_type', 9, 2);
	function themerex_trx_donations_get_blog_type($page, $query=null) {
		if (!empty($page)) return $page;
		if ($query && $query->is_tax(THEMEREX_Donations::TAXONOMY) || is_tax(THEMEREX_Donations::TAXONOMY))
			$page = 'donations_category';
		else if ($query && $query->get('post_type')==THEMEREX_Donations::POST_TYPE || get_query_var('post_type')==THEMEREX_Donations::POST_TYPE)
			$page = $query && $query->is_single() || is_single() ? 'donations_item' : 'donations';
		return $page;
	}
}

// Filter to detect current page title
if ( !function_exists( 'themerex_trx_donations_get_blog_title' ) ) {
	//add_filter('themerex_filter_get_blog_title',	'themerex_trx_donations_get_blog_title', 9, 2);
	function themerex_trx_donations_get_blog_title($title, $page) {
		if (!empty($title)) return $title;
		if ( themerex_strpos($page, 'donations')!==false ) {
			if ( $page == 'donations_category' ) {
				$term = get_term_by( 'slug', get_query_var( THEMEREX_Donations::TAXONOMY ), THEMEREX_Donations::TAXONOMY, OBJECT);
				$title = $term->name;
			} else if ( $page == 'donations_item' ) {
				$title = themerex_get_post_title();
			} else {
				$title = esc_html__('All donations', 'themerex');
			}
		}

		return $title;
	}
}

// Filter to detect stream page title
if ( !function_exists( 'themerex_trx_donations_get_stream_page_title' ) ) {
	//add_filter('themerex_filter_get_stream_page_title',	'themerex_trx_donations_get_stream_page_title', 9, 2);
	function themerex_trx_donations_get_stream_page_title($title, $page) {
		if (!empty($title)) return $title;
		if (themerex_strpos($page, 'donations')!==false) {
			if (($page_id = themerex_trx_donations_get_stream_page_id(0, $page=='donations' ? 'blog-donations' : $page)) > 0)
				$title = themerex_get_post_title($page_id);
			else
				$title = esc_html__('All donations', 'themerex');
		}
		return $title;
	}
}

// Filter to detect stream page ID
if ( !function_exists( 'themerex_trx_donations_get_stream_page_id' ) ) {
	//add_filter('themerex_filter_get_stream_page_id',	'themerex_trx_donations_get_stream_page_id', 9, 2);
	function themerex_trx_donations_get_stream_page_id($id, $page) {
		if (!empty($id)) return $id;
		if (themerex_strpos($page, 'donations')!==false) $id = themerex_get_template_page_id('blog-donations');
		return $id;
	}
}

// Filter to detect stream page URL
if ( !function_exists( 'themerex_trx_donations_get_stream_page_link' ) ) {
	//add_filter('themerex_filter_get_stream_page_link',	'themerex_trx_donations_get_stream_page_link', 9, 2);
	function themerex_trx_donations_get_stream_page_link($url, $page) {
		if (!empty($url)) return $url;
		if (themerex_strpos($page, 'donations')!==false) {
			$id = themerex_get_template_page_id('blog-donations');
			if ($id) $url = get_permalink($id);
		}
		return $url;
	}
}

// Filter to detect current taxonomy
if ( !function_exists( 'themerex_trx_donations_get_current_taxonomy' ) ) {
	//add_filter('themerex_filter_get_current_taxonomy',	'themerex_trx_donations_get_current_taxonomy', 9, 2);
	function themerex_trx_donations_get_current_taxonomy($tax, $page) {
		if (!empty($tax)) return $tax;
		if ( themerex_strpos($page, 'donations')!==false ) {
			$tax = THEMEREX_Donations::TAXONOMY;
		}
		return $tax;
	}
}

// Return taxonomy name (slug) if current page is this taxonomy page
if ( !function_exists( 'themerex_trx_donations_is_taxonomy' ) ) {
	//add_filter('themerex_filter_is_taxonomy',	'themerex_trx_donations_is_taxonomy', 9, 2);
	function themerex_trx_donations_is_taxonomy($tax, $query=null) {
		if (!empty($tax))
			return $tax;
		else 
			return $query && $query->get(THEMEREX_Donations::TAXONOMY)!='' || is_tax(THEMEREX_Donations::TAXONOMY) ? THEMEREX_Donations::TAXONOMY : '';
	}
}

// Add custom post type and/or taxonomies arguments to the query
if ( !function_exists( 'themerex_trx_donations_query_add_filters' ) ) {
	//add_filter('themerex_filter_query_add_filters',	'themerex_trx_donations_query_add_filters', 9, 2);
	function themerex_trx_donations_query_add_filters($args, $filter) {
		if ($filter == 'donations') {
			$args['post_type'] = THEMEREX_Donations::POST_TYPE;
		}
		return $args;
	}
}


// Add shortcode in the shortcodes list
if (!function_exists('themerex_trx_donations_reg_shortcodes')) {
	//add_filter('themerex_action_shortcodes_list',	'themerex_trx_donations_reg_shortcodes');
	function themerex_trx_donations_reg_shortcodes() {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['shortcodes'])) {

			$plugin = THEMEREX_Donations::get_instance();
			$donations_groups = themerex_get_list_terms(false, THEMEREX_Donations::TAXONOMY);

			themerex_array_insert_before($THEMEREX_GLOBALS['shortcodes'], 'trx_dropcaps', array(

				// ThemeREX Donations form
				"trx_donations_form" => array(
					"title" => esc_html__("Donations form", "themerex"),
					"desc" => esc_html__("Insert ThemeREX Donations form", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title for the donations form", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", "themerex"),
							"desc" => esc_html__("Subtitle for the donations form", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", "themerex"),
							"desc" => esc_html__("Short description for the donations form", "themerex"),
							"value" => "",
							"type" => "textarea"
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Alignment of the donations form", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"account" => array(
							"title" => esc_html__("PayPal account", "themerex"),
							"desc" => esc_html__("PayPal account's e-mail. If empty - used from ThemeREX Donations settings", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"sandbox" => array(
							"title" => esc_html__("Sandbox mode", "themerex"),
							"desc" => esc_html__("Use PayPal sandbox to test payments", "themerex"),
							"dependency" => array(
								'account' => array('not_empty')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"amount" => array(
							"title" => esc_html__("Default amount", "themerex"),
							"desc" => esc_html__("Specify amount, initially selected in the form", "themerex"),
							"dependency" => array(
								'account' => array('not_empty')
							),
							"value" => 5,
							"min" => 1,
							"step" => 5,
							"type" => "spinner"
						),
						"currency" => array(
							"title" => esc_html__("Currency", "themerex"),
							"desc" => esc_html__("Select payment's currency", "themerex"),
							"dependency" => array(
								'account' => array('not_empty')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => themerex_array_merge(array(0 => esc_html__('- Select currency -', 'themerex')), $plugin->currency_codes)
						),
						"width" => themerex_shortcodes_width(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
				
				
				// ThemeREX Donations form
				"trx_donations_list" => array(
					"title" => esc_html__("Donations list", "themerex"),
					"desc" => esc_html__("Insert ThemeREX Doantions list", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title for the donations list", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", "themerex"),
							"desc" => esc_html__("Subtitle for the donations list", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", "themerex"),
							"desc" => esc_html__("Short description for the donations list", "themerex"),
							"value" => "",
							"type" => "textarea"
						),
						"link" => array(
							"title" => esc_html__("Button URL", "themerex"),
							"desc" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", "themerex"),
							"desc" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"style" => array(
							"title" => esc_html__("List style", "themerex"),
							"desc" => esc_html__("Select style to display donations", "themerex"),
							"value" => "excerpt",
							"type" => "select",
							"options" => array(
								'excerpt' => esc_html__('Excerpt', 'themerex')
							)
						),
						"readmore" => array(
							"title" => esc_html__("Read more text", "themerex"),
							"desc" => esc_html__("Text of the 'Read more' link", "themerex"),
							"value" => esc_html__('Read more', 'themerex'),
							"type" => "text"
						),
						"cat" => array(
							"title" => esc_html__("Categories", "themerex"),
							"desc" => esc_html__("Select categories (groups) to show donations. If empty - select donations from any category (group) or from IDs list", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => themerex_array_merge(array(0 => esc_html__('- Select category -', 'themerex')), $donations_groups)
						),
						"count" => array(
							"title" => esc_html__("Number of donations", "themerex"),
							"desc" => esc_html__("How many donations will be displayed? If used IDs - this parameter ignored.", "themerex"),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns use to show donations list", "themerex"),
							"value" => 3,
							"min" => 2,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", "themerex"),
							"desc" => esc_html__("Skip posts before select next part.", "themerex"),
							"dependency" => array(
								'custom' => array('no')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Donadions order by", "themerex"),
							"desc" => esc_html__("Select desired sorting method", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Donations order", "themerex"),
							"desc" => esc_html__("Select donations order", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						),
						"ids" => array(
							"title" => esc_html__("Donations IDs list", "themerex"),
							"desc" => esc_html__("Comma separated list of donations ID. If set - parameters above are ignored!", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				)

			));
		}
	}
}


// Add shortcode in the VC shortcodes list
if (!function_exists('themerex_trx_donations_reg_shortcodes_vc')) {
	//add_filter('themerex_action_shortcodes_list_vc',	'themerex_trx_donations_reg_shortcodes_vc');
	function themerex_trx_donations_reg_shortcodes_vc() {
		global $THEMEREX_GLOBALS;

		$plugin = THEMEREX_Donations::get_instance();
		$donations_groups = themerex_get_list_terms(false, THEMEREX_Donations::TAXONOMY);

		// ThemeREX Donations form
		vc_map( array(
				"base" => "trx_donations_form",
				"name" => esc_html__("Donations form", "themerex"),
				"description" => esc_html__("Insert ThemeREX Donations form", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_donations_form',
				"class" => "trx_sc_single trx_sc_donations_form",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for the donations form", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", "themerex"),
						"description" => esc_html__("Subtitle for the donations form", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", "themerex"),
						"description" => esc_html__("Description for the donations form", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Alignment of the donations form", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "account",
						"heading" => esc_html__("PayPal account", "themerex"),
						"description" => esc_html__("PayPal account's e-mail. If empty - used from ThemeREX Donations settings", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('PayPal', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "sandbox",
						"heading" => esc_html__("Sandbox mode", "themerex"),
						"description" => esc_html__("Use PayPal sandbox to test payments", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('PayPal', 'themerex'),
						'dependency' => array(
							'element' => 'account',
							'not_empty' => true
						),
						"class" => "",
						"value" => array("Sandbox mode" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "amount",
						"heading" => esc_html__("Default amount", "themerex"),
						"description" => esc_html__("Specify amount, initially selected in the form", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('PayPal', 'themerex'),
						"class" => "",
						"value" => "5",
						"type" => "textfield"
					),
					array(
						"param_name" => "currency",
						"heading" => esc_html__("Currency", "themerex"),
						"description" => esc_html__("Select payment's currency", "themerex"),
						"class" => "",
						"value" => array_flip(themerex_array_merge(array(0 => esc_html__('- Select currency -', 'themerex')), $plugin->currency_codes)),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
		class WPBakeryShortCode_Trx_Donations_Form extends THEMEREX_VC_ShortCodeSingle {}



		// ThemeREX Donations list
		vc_map( array(
				"base" => "trx_donations_list",
				"name" => esc_html__("Donations list", "themerex"),
				"description" => esc_html__("Insert ThemeREX Donations list", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_donations_list',
				"class" => "trx_sc_single trx_sc_donations_list",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("List style", "themerex"),
						"description" => esc_html__("Select style to display donations", "themerex"),
						"class" => "",
						"value" => array(
                            esc_html__('Excerpt', 'themerex') => 'excerpt'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for the donations form", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", "themerex"),
						"description" => esc_html__("Subtitle for the donations form", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", "themerex"),
						"description" => esc_html__("Description for the donations form", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", "themerex"),
						"description" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", "themerex"),
						"description" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "readmore",
						"heading" => esc_html__("Read more text", "themerex"),
						"description" => esc_html__("Text of the 'Read more' link", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => esc_html__('Read more', 'themerex'),
						"type" => "textfield"
					),
					array(
						"param_name" => "cat",
						"heading" => esc_html__("Categories", "themerex"),
						"description" => esc_html__("Select category to show donations. If empty - select donations from any category (group) or from IDs list", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => array_flip(themerex_array_merge(array(0 => esc_html__('- Select category -', 'themerex')), $donations_groups)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns", "themerex"),
						"description" => esc_html__("How many columns use to show donations", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"admin_label" => true,
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Number of posts", "themerex"),
						"description" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "offset",
						"heading" => esc_html__("Offset before select posts", "themerex"),
						"description" => esc_html__("Skip posts before select next part.", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "orderby",
						"heading" => esc_html__("Post sorting", "themerex"),
						"description" => esc_html__("Select desired posts sorting method", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['sorting']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "order",
						"heading" => esc_html__("Post order", "themerex"),
						"description" => esc_html__("Select desired posts order", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "ids",
						"heading" => esc_html__("client's IDs list", "themerex"),
						"description" => esc_html__("Comma separated list of donation's ID. If set - parameters above (category, count, order, etc.)  are ignored!", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						'dependency' => array(
							'element' => 'cats',
							'is_empty' => true
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),

					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom']
				)
			) );
			
		class WPBakeryShortCode_Trx_Donations_List extends THEMEREX_VC_ShortCodeSingle {}

	}
}
?>