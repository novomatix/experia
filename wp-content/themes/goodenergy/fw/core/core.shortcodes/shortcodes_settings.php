<?php

// Check if shortcodes settings are now used
if ( !function_exists( 'themerex_shortcodes_is_used' ) ) {
	function themerex_shortcodes_is_used() {
		return themerex_options_is_used() 															// All modes when Theme Options are used
			|| (is_admin() && isset($_POST['action']) 
					&& in_array($_POST['action'], array('vc_edit_form', 'wpb_show_edit_form')))		// AJAX query when save post/page
			|| themerex_vc_is_frontend();															// VC Frontend editor mode
	}
}

// Width and height params
if ( !function_exists( 'themerex_shortcodes_width' ) ) {
	function themerex_shortcodes_width($w="") {
		return array(
			"title" => esc_html__("Width", "themerex"),
			"divider" => true,
			"value" => $w,
			"type" => "text"
		);
	}
}
if ( !function_exists( 'themerex_shortcodes_height' ) ) {
	function themerex_shortcodes_height($h='') {
		return array(
			"title" => esc_html__("Height", "themerex"),
			"desc" => esc_html__("Width (in pixels or percent) and height (only in pixels) of element", "themerex"),
			"value" => $h,
			"type" => "text"
		);
	}
}

/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'themerex_shortcodes_settings_theme_setup' ) ) {
//	if ( themerex_vc_is_frontend() )
	if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') || (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline') )
		add_action( 'themerex_action_before_init_theme', 'themerex_shortcodes_settings_theme_setup', 20 );
	else
		add_action( 'themerex_action_after_init_theme', 'themerex_shortcodes_settings_theme_setup' );
	function themerex_shortcodes_settings_theme_setup() {
		if (themerex_shortcodes_is_used()) {
			global $THEMEREX_GLOBALS;

            // Sort templates alphabetically
            ksort($THEMEREX_GLOBALS['registered_templates']);

			// Prepare arrays 
			$THEMEREX_GLOBALS['sc_params'] = array(
			
				// Current element id
				'id' => array(
					"title" => esc_html__("Element ID", "themerex"),
					"desc" => esc_html__("ID for current element", "themerex"),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
			
				// Current element class
				'class' => array(
					"title" => esc_html__("Element CSS class", "themerex"),
					"desc" => esc_html__("CSS class for current element (optional)", "themerex"),
					"value" => "",
					"type" => "text"
				),
			
				// Current element style
				'css' => array(
					"title" => esc_html__("CSS styles", "themerex"),
					"desc" => esc_html__("Any additional CSS rules (if need)", "themerex"),
					"value" => "",
					"type" => "text"
				),
			
				// Margins params
				'top' => array(
					"title" => esc_html__("Top margin", "themerex"),
					"divider" => true,
					"value" => "",
					"type" => "text"
				),
			
				'bottom' => array(
					"title" => esc_html__("Bottom margin", "themerex"),
					"value" => "",
					"type" => "text"
				),
			
				'left' => array(
					"title" => esc_html__("Left margin", "themerex"),
					"value" => "",
					"type" => "text"
				),
			
				'right' => array(
					"title" => esc_html__("Right margin", "themerex"),
					"desc" => esc_html__("Margins around list (in pixels).", "themerex"),
					"value" => "",
					"type" => "text"
				),
			
				// Switcher choises
				'list_styles' => array(
					'ul'	=> esc_html__('Unordered', 'themerex'),
					'ol'	=> esc_html__('Ordered', 'themerex'),
					'iconed'=> esc_html__('Iconed', 'themerex')
				),
				'yes_no'	=> themerex_get_list_yesno(),
				'on_off'	=> themerex_get_list_onoff(),
				'dir' 		=> themerex_get_list_directions(),
				'align'		=> themerex_get_list_alignments(),
				'float'		=> themerex_get_list_floats(),
				'show_hide'	=> themerex_get_list_showhide(),
				'sorting' 	=> themerex_get_list_sortings(),
				'ordering' 	=> themerex_get_list_orderings(),
				'shapes'	=> themerex_get_list_shapes(),
				'sizes'		=> themerex_get_list_sizes(),
				'sliders'	=> themerex_get_list_sliders(),
				'revo_sliders' => themerex_get_list_revo_sliders(),
				'categories'=> themerex_get_list_categories(),
				'columns'	=> themerex_get_list_columns(),
				'images'	=> array_merge(array('none'=>"none"), themerex_get_list_files("images/icons", "png")),
				'icons'		=> array_merge(array("inherit", "none"), themerex_get_list_icons()),
				'locations'	=> themerex_get_list_dedicated_locations(),
				'filters'	=> themerex_get_list_portfolio_filters(),
				'formats'	=> themerex_get_list_post_formats_filters(),
				'hovers'	=> themerex_get_list_hovers(true),
				'hovers_dir'=> themerex_get_list_hovers_directions(true),
				'schemes'	=> themerex_get_list_color_schemes(true),
				'animations'		=> themerex_get_list_animations_in(),
				'blogger_styles'	=> themerex_get_list_templates_blogger(),
				'forms'		=> themerex_get_list_templates_forms(),
				'posts_types'		=> themerex_get_list_posts_types(),
				'googlemap_styles'	=> themerex_get_list_googlemap_styles(),
				'field_types'		=> themerex_get_list_field_types(),
				'label_positions'	=> themerex_get_list_label_positions()
			);

			$THEMEREX_GLOBALS['sc_params']['animation'] = array(
				"title" => esc_html__("Animation",  'themerex'),
				"desc" => esc_html__('Select animation while object enter in the visible area of page',  'themerex'),
				"value" => "none",
				"type" => "select",
				"options" => $THEMEREX_GLOBALS['sc_params']['animations']
			);
	
			// Shortcodes list
			//------------------------------------------------------------------
			$THEMEREX_GLOBALS['shortcodes'] = array(
			
				// Accordion
				"trx_accordion" => array(
					"title" => esc_html__("Accordion", "themerex"),
					"desc" => esc_html__("Accordion items", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Accordion style", "themerex"),
							"desc" => esc_html__("Select style for display accordion", "themerex"),
							"value" => 1,
							"options" => themerex_get_list_styles(1, 2),
							"type" => "radio"
						),
						"counter" => array(
							"title" => esc_html__("Counter", "themerex"),
							"desc" => esc_html__("Display counter before each accordion title", "themerex"),
							"value" => "off",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['on_off']
						),
						"initial" => array(
							"title" => esc_html__("Initially opened item", "themerex"),
							"desc" => esc_html__("Number of initially opened item", "themerex"),
							"value" => 1,
							"min" => 0,
							"type" => "spinner"
						),
						"icon_before_title" => array(
							"title" => esc_html__("Icon before title",  'themerex'),
							"desc" => esc_html__('Select icon for the title accordion item from Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
                        "icon_closed" => array(
                            "title" => __("Icon while closed",  'themerex'),
                            "desc" => esc_html__('Select icon for the closed accordion item from Fontello icons set',  'themerex'),
                            "value" => "",
                            "type" => "icons",
                            "options" => $THEMEREX_GLOBALS['sc_params']['icons']
                        ),
						"icon_opened" => array(
							"title" => esc_html__("Icon while opened",  'themerex'),
							"desc" => esc_html__('Select icon for the opened accordion item from Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_accordion_item",
						"title" => esc_html__("Item", "themerex"),
						"desc" => esc_html__("Accordion item", "themerex"),
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Accordion item title", "themerex"),
								"desc" => esc_html__("Title for current accordion item", "themerex"),
								"value" => "",
								"type" => "text"
							),
                            "icon_before_title" => array(
                                "title" => esc_html__("Icon before title",  'themerex'),
                                "desc" => esc_html__('Select icon for the title accordion item from Fontello icons set',  'themerex'),
                                "value" => "",
                                "type" => "icons",
                                "options" => $THEMEREX_GLOBALS['sc_params']['icons']
                            ),
							"icon_closed" => array(
								"title" => esc_html__("Icon while closed",  'themerex'),
								"desc" => esc_html__('Select icon for the closed accordion item from Fontello icons set',  'themerex'),
								"value" => "",
								"type" => "icons",
								"options" => $THEMEREX_GLOBALS['sc_params']['icons']
							),
							"icon_opened" => array(
								"title" => esc_html__("Icon while opened",  'themerex'),
								"desc" => esc_html__('Select icon for the opened accordion item from Fontello icons set',  'themerex'),
								"value" => "",
								"type" => "icons",
								"options" => $THEMEREX_GLOBALS['sc_params']['icons']
							),
							"_content_" => array(
								"title" => esc_html__("Accordion item content", "themerex"),
								"desc" => esc_html__("Current accordion item content", "themerex"),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Anchor
				"trx_anchor" => array(
					"title" => esc_html__("Anchor", "themerex"),
					"desc" => esc_html__("Insert anchor for the TOC (table of content)", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"icon" => array(
							"title" => esc_html__("Anchor's icon",  'themerex'),
							"desc" => esc_html__('Select icon for the anchor from Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"title" => array(
							"title" => esc_html__("Short title", "themerex"),
							"desc" => esc_html__("Short title of the anchor (for the table of content)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Long description", "themerex"),
							"desc" => __("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"url" => array(
							"title" => esc_html__("External URL", "themerex"),
							"desc" => esc_html__("External URL for this TOC item", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"separator" => array(
							"title" => esc_html__("Add separator", "themerex"),
							"desc" => esc_html__("Add separator under item in the TOC", "themerex"),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"id" => $THEMEREX_GLOBALS['sc_params']['id']
					)
				),
			
			
				// Audio
				"trx_audio" => array(
					"title" => esc_html__("Audio", "themerex"),
					"desc" => esc_html__("Insert audio player", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"url" => array(
							"title" => esc_html__("URL for audio file", "themerex"),
							"desc" => esc_html__("URL for audio file", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose audio', 'themerex'),
								'action' => 'media_upload',
								'type' => 'audio',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose audio file', 'themerex'),
									'update' => esc_html__('Select audio file', 'themerex')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"image" => array(
							"title" => esc_html__("Cover image", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for audio cover", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title of the audio file", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"author" => array(
							"title" => esc_html__("Author", "themerex"),
							"desc" => esc_html__("Author of the audio file", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"controls" => array(
							"title" => esc_html__("Show controls", "themerex"),
							"desc" => esc_html__("Show controls in audio player", "themerex"),
							"divider" => true,
							"size" => "medium",
							"value" => "show",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['show_hide']
						),
						"autoplay" => array(
							"title" => esc_html__("Autoplay audio", "themerex"),
							"desc" => esc_html__("Autoplay audio on page load", "themerex"),
							"value" => "off",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['on_off']
						),
						"align" => array(
							"title" => esc_html__("Align", "themerex"),
							"desc" => esc_html__("Select block alignment", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Block
				"trx_block" => array(
					"title" => esc_html__("Block container", "themerex"),
					"desc" => esc_html__("Container for any block ([section] analog - to enable nesting)", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"dedicated" => array(
							"title" => esc_html__("Dedicated", "themerex"),
							"desc" => esc_html__("Use this block as dedicated content - show it before post title on single page", "themerex"),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Align", "themerex"),
							"desc" => esc_html__("Select block alignment", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"columns" => array(
							"title" => esc_html__("Columns emulation", "themerex"),
							"desc" => esc_html__("Select width for columns emulation", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['columns']
						), 
						"pan" => array(
							"title" => esc_html__("Use pan effect", "themerex"),
							"desc" => esc_html__("Use pan effect to show section content", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", "themerex"),
							"desc" => esc_html__("Use scroller to show section content", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"scroll_dir" => array(
							"title" => esc_html__("Scroll direction", "themerex"),
							"desc" => esc_html__("Scroll direction (if Use scroller = yes)", "themerex"),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "horizontal",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['dir']
						),
						"scroll_controls" => array(
							"title" => esc_html__("Scroll controls", "themerex"),
							"desc" => esc_html__("Show scroll controls (if Use scroller = yes)", "themerex"),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", "themerex"),
							"desc" => esc_html__("Select color scheme for this block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['schemes']
						),
						"color" => array(
							"title" => esc_html__("Fore color", "themerex"),
							"desc" => esc_html__("Any color for objects in this section", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", "themerex"),
							"desc" => esc_html__("Any background color for this section", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image URL", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for the background", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
                        "bg_tile" => array(
                           "title" => esc_html__("Tile background image", "themerex"),
                            "desc" => esc_html__("Do you want tile background image or image cover whole block?", "themerex"),
                            "value" => "no",
                            "dependency" => array(
                            'bg_image' => array('not_empty')
                             ),
                            "type" => "switch",
                            "options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
                             ),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", "themerex"),
							"desc" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", "themerex"),
							"desc" => esc_html__("Predefined texture style from 1 to 11. 0 - without texture.", "themerex"),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_padding" => array(
							"title" => esc_html__("Paddings around content", "themerex"),
							"desc" => esc_html__("Add paddings around content in this section (only if bg_color or bg_image enabled).", "themerex"),
							"value" => "yes",
							"dependency" => array(
								'compare' => 'or',
								'bg_color' => array('not_empty'),
								'bg_texture' => array('not_empty'),
								'bg_image' => array('not_empty')
							),
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"font_size" => array(
							"title" => esc_html__("Font size", "themerex"),
							"desc" => esc_html__("Font size of the text (default - in pixels, allows any CSS units of measure)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", "themerex"),
							"desc" => esc_html__("Font weight of the text", "themerex"),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'100' => esc_html__('Thin (100)', 'themerex'),
								'300' => esc_html__('Light (300)', 'themerex'),
								'400' => esc_html__('Normal (400)', 'themerex'),
								'700' => esc_html__('Bold (700)', 'themerex')
							)
						),
						"_content_" => array(
							"title" => esc_html__("Container content", "themerex"),
							"desc" => esc_html__("Content for section container", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Blogger
				"trx_blogger" => array(
					"title" => esc_html__("Blogger", "themerex"),
					"desc" => esc_html__("Insert posts (pages) in many styles from desired categories or directly from ids", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", "themerex"),
							"desc" => esc_html__("Subtitle for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", "themerex"),
							"desc" => esc_html__("Short description for the block", "themerex"),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Posts output style", "themerex"),
							"desc" => esc_html__("Select desired style for posts output", "themerex"),
							"value" => "regular",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['blogger_styles']
						),
						"filters" => array(
							"title" => esc_html__("Show filters", "themerex"),
							"desc" => esc_html__("Use post's tags or categories as filter buttons", "themerex"),
							"value" => "no",
							"dir" => "horizontal",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['filters']
						),
						"hover" => array(
							"title" => esc_html__("Hover effect", "themerex"),
							"desc" => esc_html__("Select hover effect (only if style=Portfolio)", "themerex"),
							"dependency" => array(
								'style' => array('portfolio','grid','square','short','colored')
							),
							"value" => "",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['hovers']
						),
						"hover_dir" => array(
							"title" => esc_html__("Hover direction", "themerex"),
							"desc" => esc_html__("Select hover direction (only if style=Portfolio and hover=Circle|Square)", "themerex"),
							"dependency" => array(
								'style' => array('portfolio','grid','square','short','colored'),
								'hover' => array('square','circle')
							),
							"value" => "left_to_right",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['hovers_dir']
						),
						"dir" => array(
							"title" => esc_html__("Posts direction", "themerex"),
							"desc" => esc_html__("Display posts in horizontal or vertical direction", "themerex"),
							"value" => "horizontal",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['dir']
						),
						"post_type" => array(
							"title" => esc_html__("Post type", "themerex"),
							"desc" => esc_html__("Select post type to show", "themerex"),
							"value" => "post",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['posts_types']
						),
						"ids" => array(
							"title" => esc_html__("Post IDs list", "themerex"),
							"desc" => esc_html__("Comma separated list of posts ID. If set - parameters above are ignored!", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"cat" => array(
							"title" => esc_html__("Categories list", "themerex"),
							"desc" => esc_html__("Select the desired categories. If not selected - show posts from any category or from IDs list", "themerex"),
							"dependency" => array(
								'ids' => array('is_empty'),
								'post_type' => array('refresh')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => themerex_array_merge(array(0 => esc_html__('- Select category -', 'themerex')), $THEMEREX_GLOBALS['sc_params']['categories'])
						),
						"count" => array(
							"title" => esc_html__("Total posts to show", "themerex"),
							"desc" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", "themerex"),
							"dependency" => array(
								'ids' => array('is_empty')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns number", "themerex"),
							"desc" => esc_html__("How many columns used to show posts? If empty or 0 - equal to posts number", "themerex"),
							"dependency" => array(
								'dir' => array('horizontal')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Offset before select posts", "themerex"),
							"desc" => esc_html__("Skip posts before select next part.", "themerex"),
							"dependency" => array(
								'ids' => array('is_empty')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Post order by", "themerex"),
							"desc" => esc_html__("Select desired posts sorting method", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Post order", "themerex"),
							"desc" => esc_html__("Select desired posts order", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						),
						"only" => array(
							"title" => esc_html__("Select posts only", "themerex"),
							"desc" => esc_html__("Select posts only with reviews, videos, audios, thumbs or galleries", "themerex"),
							"value" => "no",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['formats']
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", "themerex"),
							"desc" => esc_html__("Use scroller to show all posts", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"controls" => array(
							"title" => esc_html__("Show slider controls", "themerex"),
							"desc" => esc_html__("Show arrows to control scroll slider", "themerex"),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"location" => array(
							"title" => esc_html__("Dedicated content location", "themerex"),
							"desc" => esc_html__("Select position for dedicated content (only for style=excerpt)", "themerex"),
							"divider" => true,
							"dependency" => array(
								'style' => array('excerpt')
							),
							"value" => "default",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['locations']
						),
						"rating" => array(
							"title" => esc_html__("Show rating stars", "themerex"),
							"desc" => esc_html__("Show rating stars under post's header", "themerex"),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"info" => array(
							"title" => esc_html__("Show post info block", "themerex"),
							"desc" => esc_html__("Show post info block (author, date, tags, etc.)", "themerex"),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"links" => array(
							"title" => esc_html__("Allow links on the post", "themerex"),
							"desc" => esc_html__("Allow links on the post from each blogger item", "themerex"),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"descr" => array(
							"title" => esc_html__("Description length", "themerex"),
							"desc" => esc_html__("How many characters are displayed from post excerpt? If 0 - don't show description", "themerex"),
							"value" => 0,
							"min" => 0,
							"step" => 10,
							"type" => "spinner"
						),
						"readmore" => array(
							"title" => esc_html__("More link text", "themerex"),
							"desc" => esc_html__("Read more link text. If empty - show 'More', else - used as link text", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"link" => array(
							"title" => esc_html__("Button URL", "themerex"),
							"desc" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", "themerex"),
							"desc" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Br
				"trx_br" => array(
					"title" => esc_html__("Break", "themerex"),
					"desc" => esc_html__("Line break with clear floating (if need)", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"clear" => 	array(
							"title" => esc_html__("Clear floating", "themerex"),
							"desc" => esc_html__("Clear floating (if need)", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => array(
								'none' => esc_html__('None', 'themerex'),
								'left' => esc_html__('Left', 'themerex'),
								'right' => esc_html__('Right', 'themerex'),
								'both' => esc_html__('Both', 'themerex')
							)
						)
					)
				),
			
			
			
			
				// Button
				"trx_button" => array(
					"title" => esc_html__("Button", "themerex"),
					"desc" => esc_html__("Button with link", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Caption", "themerex"),
							"desc" => esc_html__("Button caption", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"type" => array(
							"title" => esc_html__("Button's shape", "themerex"),
							"desc" => esc_html__("Select button's shape", "themerex"),
							"value" => "square",
							"size" => "medium",
							"options" => array(
								'square' => esc_html__('Square', 'themerex'),
								'round' => esc_html__('Round', 'themerex')
							),
							"type" => "switch"
						), 
						"style" => array(
							"title" => esc_html__("Button's style", "themerex"),
							"desc" => esc_html__("Select button's style", "themerex"),
							"value" => "default",
							"dir" => "horizontal",
							"options" => array(
								'filled' => esc_html__('Filled', 'themerex'),
								'border' => esc_html__('Border', 'themerex')
							),
							"type" => "checklist"
						),
                        "bg_color_style" => array(
                            "title" => esc_html__("Background style", "themerex"),
                            "desc" => esc_html__("Select background color style", "themerex"),
                            "value" => "bg_style1",
                            "dir" => "horizontal",
                            "options" => array(
                                'bg_style1' => esc_html__('Style1', 'themerex'),
                                'bg_style2' => esc_html__('Style2', 'themerex')
                            ),
                            "type" => "checklist"
                        ),
                        "size" => array(
							"title" => esc_html__("Button's size", "themerex"),
							"desc" => esc_html__("Select button's size", "themerex"),
							"value" => "small",
							"dir" => "horizontal",
							"options" => array(
								'small' => esc_html__('Small', 'themerex'),
								'medium' => esc_html__('Medium', 'themerex'),
								'large' => esc_html__('Large', 'themerex')
							),
							"type" => "checklist"
						), 
						"icon" => array(
							"title" => esc_html__("Button's icon",  'themerex'),
							"desc" => esc_html__('Select icon for the title from Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"color" => array(
							"title" => esc_html__("Button's text color", "themerex"),
							"desc" => esc_html__("Any color for button's caption", "themerex"),
							"std" => "",
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Button's backcolor", "themerex"),
							"desc" => esc_html__("Any color for button's background", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"align" => array(
							"title" => esc_html__("Button's alignment", "themerex"),
							"desc" => esc_html__("Align button to left, center or right", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						), 
						"link" => array(
							"title" => esc_html__("Link URL", "themerex"),
							"desc" => esc_html__("URL for link on button click", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"target" => array(
							"title" => esc_html__("Link target", "themerex"),
							"desc" => esc_html__("Target for link on button click", "themerex"),
							"dependency" => array(
								'link' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"popup" => array(
							"title" => esc_html__("Open link in popup", "themerex"),
							"desc" => esc_html__("Open link target in popup window", "themerex"),
							"dependency" => array(
								'link' => array('not_empty')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						), 
						"rel" => array(
							"title" => esc_html__("Rel attribute", "themerex"),
							"desc" => esc_html__("Rel attribute for button's link (if need)", "themerex"),
							"dependency" => array(
								'link' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),




				// Call to Action block
				"trx_call_to_action" => array(
					"title" => esc_html__("Call to action", "themerex"),
					"desc" => esc_html__("Insert call to action block in your page (post)", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", "themerex"),
							"desc" => esc_html__("Subtitle for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", "themerex"),
							"desc" => esc_html__("Short description for the block", "themerex"),
							"value" => "",
							"type" => "textarea"
						),
						"style" => array(
							"title" => esc_html__("Style", "themerex"),
							"desc" => esc_html__("Select style to display block", "themerex"),
							"value" => "1",
							"type" => "checklist",
							"options" => themerex_get_list_styles(1, 2)
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Alignment elements in the block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"accent" => array(
							"title" => esc_html__("Accented", "themerex"),
							"desc" => esc_html__("Fill entire block with Accent1 color from current color scheme", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"custom" => array(
							"title" => esc_html__("Custom", "themerex"),
							"desc" => esc_html__("Allow get featured image or video from inner shortcodes (custom) or get it from shortcode parameters below", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"image" => array(
							"title" => esc_html__("Image", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site to include image into this block", "themerex"),
							"divider" => true,
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"video" => array(
							"title" => esc_html__("URL for video file", "themerex"),
							"desc" => esc_html__("Select video from media library or paste URL for video file from other site to include video into this block", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose video', 'themerex'),
								'action' => 'media_upload',
								'type' => 'video',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose video file', 'themerex'),
									'update' => esc_html__('Select video file', 'themerex')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"link" => array(
							"title" => esc_html__("Button URL", "themerex"),
							"desc" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", "themerex"),
							"desc" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"link2" => array(
							"title" => esc_html__("Button 2 URL", "themerex"),
							"desc" => esc_html__("Link URL for the second button at the bottom of the block", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"link2_caption" => array(
							"title" => esc_html__("Button 2 caption", "themerex"),
							"desc" => esc_html__("Caption for the second button at the bottom of the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Chat
				"trx_chat" => array(
					"title" => esc_html__("Chat", "themerex"),
					"desc" => esc_html__("Chat message", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Item title", "themerex"),
							"desc" => esc_html__("Chat item title", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"photo" => array(
							"title" => esc_html__("Item photo", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for the item photo (avatar)", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"link" => array(
							"title" => esc_html__("Item link", "themerex"),
							"desc" => esc_html__("Chat item link", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Chat item content", "themerex"),
							"desc" => esc_html__("Current chat item content", "themerex"),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
				// Columns
				"trx_columns" => array(
					"title" => esc_html__("Columns", "themerex"),
					"desc" => esc_html__("Insert up to 5 columns in your page (post)", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"fluid" => array(
							"title" => esc_html__("Fluid columns", "themerex"),
							"desc" => esc_html__("To squeeze the columns when reducing the size of the window (fluid=yes) or to rebuild them (fluid=no)", "themerex"),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
                        "margins" => array(
                           "title" => esc_html__("Margins between columns", "themerex"),
                            "desc" => esc_html__("Add margins between columns", "themerex"),
                            "value" => "yes",
                           "type" => "switch",
                            "options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
                             ),
                        "dark" => array(
                            "title" => esc_html__("Dark section", "themerex"),
                            "desc" => esc_html__("Color of font is white", "themerex"),
                            "value" => "no",
                            "type" => "switch",
                            "options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
                        ),
                        "width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_column_item",
						"title" => esc_html__("Column", "themerex"),
						"desc" => esc_html__("Column item", "themerex"),
						"container" => true,
						"params" => array(
							"span" => array(
								"title" => esc_html__("Merge columns", "themerex"),
								"desc" => esc_html__("Count merged columns from current", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"align" => array(
								"title" => esc_html__("Alignment", "themerex"),
								"desc" => esc_html__("Alignment text in the column", "themerex"),
								"value" => "",
								"type" => "checklist",
								"dir" => "horizontal",
								"options" => $THEMEREX_GLOBALS['sc_params']['align']
							),
							"color" => array(
								"title" => esc_html__("Fore color", "themerex"),
								"desc" => esc_html__("Any color for objects in this column", "themerex"),
								"value" => "",
								"type" => "color"
							),
							"bg_color" => array(
								"title" => esc_html__("Background color", "themerex"),
								"desc" => esc_html__("Any background color for this column", "themerex"),
								"value" => "",
								"type" => "color"
							),
							"bg_image" => array(
								"title" => esc_html__("URL for background image file", "themerex"),
								"desc" => esc_html__("Select or upload image or write URL from other site for the background", "themerex"),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							),
                            "bg_tile" => array(
                                "title" => esc_html__("Tile background image", "themerex"),
                                "desc" => esc_html__("Do you want tile background image or image cover whole column?", "themerex"),
                                "value" => "no",
                                "dependency" => array(
                                    'bg_image' => array('not_empty')
                                       ),
                                "type" => "switch",
                                "options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
                                    ),
							"_content_" => array(
								"title" => esc_html__("Column item content", "themerex"),
								"desc" => esc_html__("Current column item content", "themerex"),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Contact form
				"trx_form" => array(
					"title" => esc_html__("Form", "themerex"),
					"desc" => esc_html__("Insert form with specified style or with set of custom fields", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", "themerex"),
							"desc" => esc_html__("Subtitle for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", "themerex"),
							"desc" => esc_html__("Short description for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"style" => array(
							"title" => esc_html__("Style", "themerex"),
							"desc" => esc_html__("Select style of the form (if 'style' is not equal 'custom' - all tabs 'Field NN' are ignored!", "themerex"),
							"value" => 'custom',
							"options" => $THEMEREX_GLOBALS['sc_params']['forms'],
							"type" => "checklist"
						), 
						"scheme" => array(
							"title" => esc_html__("Color scheme", "themerex"),
							"desc" => esc_html__("Select color scheme for this block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['schemes']
						),
						"action" => array(
							"title" => esc_html__("Action", "themerex"),
							"desc" => esc_html__("Contact form action (URL to handle form data). If empty - use internal action", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"align" => array(
							"title" => esc_html__("Align", "themerex"),
							"desc" => esc_html__("Select form alignment", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"width" => themerex_shortcodes_width(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_form_item",
						"title" => esc_html__("Field", "themerex"),
						"desc" => esc_html__("Custom field", "themerex"),
						"container" => false,
						"params" => array(
							"type" => array(
								"title" => esc_html__("Type", "themerex"),
								"desc" => esc_html__("Type of the custom field", "themerex"),
								"value" => "text",
								"type" => "checklist",
								"dir" => "horizontal",
								"options" => $THEMEREX_GLOBALS['sc_params']['field_types']
							), 
							"name" => array(
								"title" => esc_html__("Name", "themerex"),
								"desc" => esc_html__("Name of the custom field", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"value" => array(
								"title" => esc_html__("Default value", "themerex"),
								"desc" => esc_html__("Default value of the custom field", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"options" => array(
								"title" => esc_html__("Options", "themerex"),
								"desc" => esc_html__("Field options. For example: big=My daddy|middle=My brother|small=My little sister", "themerex"),
								"dependency" => array(
									'type' => array('radio', 'checkbox', 'select')
								),
								"value" => "",
								"type" => "text"
							),
							"label" => array(
								"title" => esc_html__("Label", "themerex"),
								"desc" => esc_html__("Label for the custom field", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"label_position" => array(
								"title" => esc_html__("Label position", "themerex"),
								"desc" => esc_html__("Label position relative to the field", "themerex"),
								"value" => "top",
								"type" => "checklist",
								"dir" => "horizontal",
								"options" => $THEMEREX_GLOBALS['sc_params']['label_positions']
							), 
							"top" => $THEMEREX_GLOBALS['sc_params']['top'],
							"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
							"left" => $THEMEREX_GLOBALS['sc_params']['left'],
							"right" => $THEMEREX_GLOBALS['sc_params']['right'],
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Content block on fullscreen page
				"trx_content" => array(
					"title" => esc_html__("Content block", "themerex"),
					"desc" => esc_html__("Container for main content block with desired class and style (use it only on fullscreen pages)", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"scheme" => array(
							"title" => esc_html__("Color scheme", "themerex"),
							"desc" => esc_html__("Select color scheme for this block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['schemes']
						),
						"_content_" => array(
							"title" => esc_html__("Container content", "themerex"),
							"desc" => esc_html__("Content for section container", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Countdown
				"trx_countdown" => array(
					"title" => esc_html__("Countdown", "themerex"),
					"desc" => esc_html__("Insert countdown object", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"date" => array(
							"title" => esc_html__("Date", "themerex"),
							"desc" => esc_html__("Upcoming date (format: yyyy-mm-dd)", "themerex"),
							"value" => "",
							"format" => "yy-mm-dd",
							"type" => "date"
						),
						"time" => array(
							"title" => esc_html__("Time", "themerex"),
							"desc" => esc_html__("Upcoming time (format: HH:mm:ss)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"style" => array(
							"title" => esc_html__("Style", "themerex"),
							"desc" => esc_html__("Countdown style", "themerex"),
							"value" => "1",
							"type" => "checklist",
							"options" => themerex_get_list_styles(1, 2)
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Align counter to left, center or right", "themerex"),
							"divider" => true,
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						), 
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Dropcaps
				"trx_dropcaps" => array(
					"title" => esc_html__("Dropcaps", "themerex"),
					"desc" => esc_html__("Make first letter as dropcaps", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", "themerex"),
							"desc" => esc_html__("Dropcaps style", "themerex"),
							"value" => "1",
							"type" => "checklist",
							"options" => themerex_get_list_styles(1, 4)
						),
						"_content_" => array(
							"title" => esc_html__("Paragraph content", "themerex"),
							"desc" => esc_html__("Paragraph with dropcaps content", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Emailer
				"trx_emailer" => array(
					"title" => esc_html__("E-mail collector", "themerex"),
					"desc" => esc_html__("Collect the e-mail address into specified group", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"group" => array(
							"title" => esc_html__("Group", "themerex"),
							"desc" => esc_html__("The name of group to collect e-mail address", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"open" => array(
							"title" => esc_html__("Open", "themerex"),
							"desc" => esc_html__("Initially open the input field on show object", "themerex"),
							"divider" => true,
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Align object to left, center or right", "themerex"),
							"divider" => true,
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						), 
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Gap
				"trx_gap" => array(
					"title" => esc_html__("Gap", "themerex"),
					"desc" => esc_html__("Insert gap (fullwidth area) in the post content. Attention! Use the gap only in the posts (pages) without left or right sidebar", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Gap content", "themerex"),
							"desc" => esc_html__("Gap inner content", "themerex"),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						)
					)
				),
			
			
			
			
			
				// Google map
				"trx_googlemap" => array(
					"title" => esc_html__("Google map", "themerex"),
					"desc" => esc_html__("Insert Google map with specified markers", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"zoom" => array(
							"title" => esc_html__("Zoom", "themerex"),
							"desc" => esc_html__("Map zoom factor", "themerex"),
							"divider" => true,
							"value" => 16,
							"min" => 1,
							"max" => 20,
							"type" => "spinner"
						),
						"style" => array(
							"title" => esc_html__("Map style", "themerex"),
							"desc" => esc_html__("Select map style", "themerex"),
							"value" => "default",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['googlemap_styles']
						),
						"width" => themerex_shortcodes_width('100%'),
						"height" => themerex_shortcodes_height(240),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_googlemap_marker",
						"title" => esc_html__("Google map marker", "themerex"),
						"desc" => esc_html__("Google map marker", "themerex"),
						"decorate" => false,
						"container" => true,
						"params" => array(
							"address" => array(
								"title" => esc_html__("Address", "themerex"),
								"desc" => esc_html__("Address of this marker", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"latlng" => array(
								"title" => esc_html__("Latitude and Longtitude", "themerex"),
								"desc" => esc_html__("Comma separated marker's coorditanes (instead Address)", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"point" => array(
								"title" => esc_html__("URL for marker image file", "themerex"),
								"desc" => esc_html__("Select or upload image or write URL from other site for this marker. If empty - use default marker", "themerex"),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							),
							"title" => array(
								"title" => esc_html__("``", "themerex"),
								"desc" => esc_html__("Title for this marker", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"_content_" => array(
								"title" => esc_html__("Description", "themerex"),
								"desc" => esc_html__("Description for this marker", "themerex"),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id']
						)
					)
				),
			
			
			
				// Hide or show any block
				"trx_hide" => array(
					"title" => esc_html__("Hide/Show any block", "themerex"),
					"desc" => esc_html__("Hide or Show any block with desired CSS-selector", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"selector" => array(
							"title" => esc_html__("Selector", "themerex"),
							"desc" => esc_html__("Any block's CSS-selector", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"hide" => array(
							"title" => esc_html__("Hide or Show", "themerex"),
							"desc" => esc_html__("New state for the block: hide or show", "themerex"),
							"value" => "yes",
							"size" => "small",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						)
					)
				),
			
			
			
				// Highlght text
				"trx_highlight" => array(
					"title" => esc_html__("Highlight text", "themerex"),
					"desc" => esc_html__("Highlight text with selected color, background color and other styles", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"type" => array(
							"title" => esc_html__("Type", "themerex"),
							"desc" => esc_html__("Highlight type", "themerex"),
							"value" => "1",
							"type" => "checklist",
							"options" => array(
								0 => esc_html__('Custom', 'themerex'),
								1 => esc_html__('Type 1', 'themerex'),
								2 => esc_html__('Type 2', 'themerex'),
								3 => esc_html__('Type 3', 'themerex')
							)
						),
						"color" => array(
							"title" => esc_html__("Color", "themerex"),
							"desc" => esc_html__("Color for the highlighted text", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", "themerex"),
							"desc" => esc_html__("Background color for the highlighted text", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"font_size" => array(
							"title" => esc_html__("Font size", "themerex"),
							"desc" => esc_html__("Font size of the highlighted text (default - in pixels, allows any CSS units of measure)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Highlighting content", "themerex"),
							"desc" => esc_html__("Content for highlight", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Icon
				"trx_icon" => array(
					"title" => esc_html__("Icon", "themerex"),
					"desc" => esc_html__("Insert icon", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"icon" => array(
							"title" => esc_html__('Icon',  'themerex'),
							"desc" => esc_html__('Select font icon from the Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"color" => array(
							"title" => esc_html__("Icon's color", "themerex"),
							"desc" => esc_html__("Icon's color", "themerex"),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "color"
						),
						"bg_shape" => array(
							"title" => esc_html__("Background shape", "themerex"),
							"desc" => esc_html__("Shape of the icon background", "themerex"),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "none",
							"type" => "radio",
							"options" => array(
								'none' => esc_html__('None', 'themerex'),
								'round' => esc_html__('Round', 'themerex'),
								'square' => esc_html__('Square', 'themerex')
							)
						),
						"bg_color" => array(
							"title" => esc_html__("Icon's background color", "themerex"),
							"desc" => esc_html__("Icon's background color", "themerex"),
							"dependency" => array(
								'icon' => array('not_empty'),
								'background' => array('round','square')
							),
							"value" => "",
							"type" => "color"
						),
						"font_size" => array(
							"title" => esc_html__("Font size", "themerex"),
							"desc" => esc_html__("Icon's font size", "themerex"),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "spinner",
							"min" => 8,
							"max" => 240
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", "themerex"),
							"desc" => esc_html__("Icon font weight", "themerex"),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'100' => esc_html__('Thin (100)', 'themerex'),
								'300' => esc_html__('Light (300)', 'themerex'),
								'400' => esc_html__('Normal (400)', 'themerex'),
								'700' => esc_html__('Bold (700)', 'themerex')
							)
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Icon text alignment", "themerex"),
							"dependency" => array(
								'icon' => array('not_empty')
							),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						), 
						"link" => array(
							"title" => esc_html__("Link URL", "themerex"),
							"desc" => esc_html__("Link URL from this icon (if not empty)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Image
				"trx_image" => array(
					"title" => esc_html__("Image", "themerex"),
					"desc" => esc_html__("Insert image into your post (page)", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"url" => array(
							"title" => esc_html__("URL for image file", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'sizes' => true		// If you want allow user select thumb size for image. Otherwise, thumb size is ignored - image fullsize used
							)
						),
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Image title (if need)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"icon" => array(
							"title" => esc_html__("Icon before title",  'themerex'),
							"desc" => esc_html__('Select icon for the title from Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"align" => array(
							"title" => esc_html__("Float image", "themerex"),
							"desc" => esc_html__("Float image to left or right side", "themerex"),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['float']
						), 
						"shape" => array(
							"title" => esc_html__("Image Shape", "themerex"),
							"desc" => esc_html__("Shape of the image: square (rectangle) or round", "themerex"),
							"value" => "square",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								"square" => esc_html__('Square', 'themerex'),
								"round" => esc_html__('Round', 'themerex')
							)
						), 
						"link" => array(
							"title" => esc_html__("Link", "themerex"),
							"desc" => esc_html__("The link URL from the image", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Infobox
				"trx_infobox" => array(
					"title" => esc_html__("Infobox", "themerex"),
					"desc" => esc_html__("Insert infobox into your post (page)", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", "themerex"),
							"desc" => esc_html__("Infobox style", "themerex"),
							"value" => "regular",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'regular' => esc_html__('Regular', 'themerex'),
								'info' => esc_html__('Info', 'themerex'),
								'success' => esc_html__('Success', 'themerex'),
								'error' => esc_html__('Error', 'themerex')
							)
						),
						"closeable" => array(
							"title" => esc_html__("Closeable box", "themerex"),
							"desc" => esc_html__("Create closeable box (with close button)", "themerex"),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"icon" => array(
							"title" => esc_html__("Custom icon",  'themerex'),
							"desc" => esc_html__('Select icon for the infobox from Fontello icons set. If empty - use default icon',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"color" => array(
							"title" => esc_html__("Text color", "themerex"),
							"desc" => esc_html__("Any color for text and headers", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", "themerex"),
							"desc" => esc_html__("Any background color for this infobox", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"_content_" => array(
							"title" => esc_html__("Infobox content", "themerex"),
							"desc" => esc_html__("Content for infobox", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Line
				"trx_line" => array(
					"title" => esc_html__("Line", "themerex"),
					"desc" => esc_html__("Insert Line into your post (page)", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", "themerex"),
							"desc" => esc_html__("Line style", "themerex"),
							"value" => "solid",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'solid' => esc_html__('Solid', 'themerex'),
								'dashed' => esc_html__('Dashed', 'themerex'),
								'dotted' => esc_html__('Dotted', 'themerex'),
								'double' => esc_html__('Double', 'themerex')
							)
						),
						"color" => array(
							"title" => esc_html__("Color", "themerex"),
							"desc" => esc_html__("Line color", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// List
				"trx_list" => array(
					"title" => esc_html__("List", "themerex"),
					"desc" => esc_html__("List items with specific bullets", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Bullet's style", "themerex"),
							"desc" => esc_html__("Bullet's style for each list item", "themerex"),
							"value" => "ul",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['list_styles']
						), 
						"color" => array(
							"title" => esc_html__("Color", "themerex"),
							"desc" => esc_html__("List items color", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"icon" => array(
							"title" => esc_html__('List icon',  'themerex'),
							"desc" => esc_html__("Select list icon from Fontello icons set (only for style=Iconed)",  'themerex'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"icon_color" => array(
							"title" => esc_html__("Icon color", "themerex"),
							"desc" => esc_html__("List icons color", "themerex"),
							"value" => "",
							"dependency" => array(
								'style' => array('iconed')
							),
							"type" => "color"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_list_item",
						"title" => esc_html__("Item", "themerex"),
						"desc" => esc_html__("List item with specific bullet", "themerex"),
						"decorate" => false,
						"container" => true,
						"params" => array(
							"_content_" => array(
								"title" => esc_html__("List item content", "themerex"),
								"desc" => esc_html__("Current list item content", "themerex"),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"title" => array(
								"title" => esc_html__("List item title", "themerex"),
								"desc" => esc_html__("Current list item title (show it as tooltip)", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"color" => array(
								"title" => esc_html__("Color", "themerex"),
								"desc" => esc_html__("Text color for this item", "themerex"),
								"value" => "",
								"type" => "color"
							),
							"icon" => array(
								"title" => esc_html__('List icon',  'themerex'),
								"desc" => esc_html__("Select list item icon from Fontello icons set (only for style=Iconed)",  'themerex'),
								"value" => "",
								"type" => "icons",
								"options" => $THEMEREX_GLOBALS['sc_params']['icons']
							),
							"icon_color" => array(
								"title" => esc_html__("Icon color", "themerex"),
								"desc" => esc_html__("Icon color for this item", "themerex"),
								"value" => "",
								"type" => "color"
							),
							"link" => array(
								"title" => esc_html__("Link URL", "themerex"),
								"desc" => esc_html__("Link URL for the current list item", "themerex"),
								"divider" => true,
								"value" => "",
								"type" => "text"
							),
							"target" => array(
								"title" => esc_html__("Link target", "themerex"),
								"desc" => esc_html__("Link target for the current list item", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
				// Number
				"trx_number" => array(
					"title" => esc_html__("Number", "themerex"),
					"desc" => esc_html__("Insert number or any word as set separate characters", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"value" => array(
							"title" => esc_html__("Value", "themerex"),
							"desc" => esc_html__("Number or any word", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"align" => array(
							"title" => esc_html__("Align", "themerex"),
							"desc" => esc_html__("Select block alignment", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Parallax
				"trx_parallax" => array(
					"title" => esc_html__("Parallax", "themerex"),
					"desc" => esc_html__("Create the parallax container (with asinc background image)", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"gap" => array(
							"title" => esc_html__("Create gap", "themerex"),
							"desc" => esc_html__("Create gap around parallax container", "themerex"),
							"value" => "no",
							"size" => "small",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						), 
						"dir" => array(
							"title" => esc_html__("Dir", "themerex"),
							"desc" => esc_html__("Scroll direction for the parallax background", "themerex"),
							"value" => "up",
							"size" => "medium",
							"options" => array(
								'up' => esc_html__('Up', 'themerex'),
								'down' => esc_html__('Down', 'themerex')
							),
							"type" => "switch"
						), 
						"speed" => array(
							"title" => esc_html__("Speed", "themerex"),
							"desc" => esc_html__("Image motion speed (from 0.0 to 1.0)", "themerex"),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0.3",
							"type" => "spinner"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", "themerex"),
							"desc" => esc_html__("Select color scheme for this block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['schemes']
						),
						"color" => array(
							"title" => esc_html__("Text color", "themerex"),
							"desc" => esc_html__("Select color for text object inside parallax block", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" =>esc_html__("Background color", "themerex"),
							"desc" => esc_html__("Select color for parallax background", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for the parallax background", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_image_x" => array(
							"title" => esc_html__("Image X position", "themerex"),
							"desc" => esc_html__("Image horizontal position (as background of the parallax block) - in percent", "themerex"),
							"min" => "0",
							"max" => "100",
							"value" => "50",
							"type" => "spinner"
						),
						"bg_video" => array(
							"title" => esc_html__("Video background", "themerex"),
							"desc" => esc_html__("Select video from media library or paste URL for video file from other site to show it as parallax background", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose video', 'themerex'),
								'action' => 'media_upload',
								'type' => 'video',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose video file', 'themerex'),
									'update' => esc_html__('Select video file', 'themerex')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"bg_video_ratio" => array(
							"title" => esc_html__("Video ratio", "themerex"),
							"desc" => esc_html__("Specify ratio of the video background. For example: 16:9 (default), 4:3, etc.", "themerex"),
							"value" => "16:9",
							"type" => "text"
						),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", "themerex"),
							"desc" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
                        "reverse" => array(
                            "title" => esc_html__("Change the color", "themerex"),
                            "desc" => esc_html__("Change the color of text", "themerex"),
                            "value" => "no",
                            "size" => "small",
                            "options" => $THEMEREX_GLOBALS['sc_params']['yes_no'],
                            "type" => "switch"
                        ),
                        "bg_texture" => array(
							"title" => esc_html__("Texture", "themerex"),
							"desc" => esc_html__("Predefined texture style from 1 to 11. 0 - without texture.", "themerex"),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"_content_" => array(
							"title" => esc_html__("Content", "themerex"),
							"desc" => esc_html__("Content for the parallax container", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Popup
				"trx_popup" => array(
					"title" => esc_html__("Popup window", "themerex"),
					"desc" => esc_html__("Container for any html-block with desired class and style for popup window", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Container content", "themerex"),
							"desc" => esc_html__("Content for section container", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Price
				"trx_price" => array(
					"title" => esc_html__("Price", "themerex"),
					"desc" => esc_html__("Insert price with decoration", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"money" => array(
							"title" => esc_html__("Money", "themerex"),
							"desc" => esc_html__("Money value (dot or comma separated)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"currency" => array(
							"title" => esc_html__("Currency", "themerex"),
							"desc" => esc_html__("Currency character", "themerex"),
							"value" => "$",
							"type" => "text"
						),
						"period" => array(
							"title" => esc_html__("Period", "themerex"),
							"desc" => esc_html__("Period text (if need). For example: monthly, daily, etc.", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Align price to left or right side", "themerex"),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['float']
						), 
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
				// Price block
				"trx_price_block" => array(
					"title" => esc_html__("Price block", "themerex"),
					"desc" => esc_html__("Insert price block with title, price and description", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Block title", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"link" => array(
							"title" => esc_html__("Link URL", "themerex"),
							"desc" => esc_html__("URL for link from button (at bottom of the block)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"link_text" => array(
							"title" => esc_html__("Link text", "themerex"),
							"desc" => esc_html__("Text (caption) for the link button (at bottom of the block). If empty - button not showed", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"icon" => array(
							"title" => esc_html__("Icon",  'themerex'),
							"desc" => esc_html__('Select icon from Fontello icons set (placed before/instead price)',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"money" => array(
							"title" => esc_html__("Money", "themerex"),
							"desc" => esc_html__("Money value (dot or comma separated)", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"currency" => array(
							"title" => esc_html__("Currency", "themerex"),
							"desc" => esc_html__("Currency character", "themerex"),
							"value" => "$",
							"type" => "text"
						),
						"period" => array(
							"title" => esc_html__("Period", "themerex"),
							"desc" => esc_html__("Period text (if need). For example: monthly, daily, etc.", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", "themerex"),
							"desc" => esc_html__("Select color scheme for this block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['schemes']
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Align price to left or right side", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['float']
						), 
						"_content_" => array(
							"title" => esc_html__("Description", "themerex"),
							"desc" => esc_html__("Description for this price block", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Quote
				"trx_quote" => array(
					"title" => esc_html__("Quote", "themerex"),
					"desc" => esc_html__("Quote text", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
                        "style" => array(
                            "title" => esc_html__("Style", "themerex"),
                            "desc" => esc_html__("Quote style", "themerex"),
                            "value" => "1",
                            "type" => "checklist",
                            "options" => themerex_get_list_styles(1, 2)
                        ),
						"cite" => array(
							"title" => esc_html__("Quote cite", "themerex"),
							"desc" => esc_html__("URL for quote cite", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"title" => array(
							"title" => esc_html__("Title (author)", "themerex"),
							"desc" => esc_html__("Quote title (author name)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Quote content", "themerex"),
							"desc" => esc_html__("Quote content", "themerex"),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => themerex_shortcodes_width(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Reviews
				"trx_reviews" => array(
					"title" => esc_html__("Reviews", "themerex"),
					"desc" => esc_html__("Insert reviews block in the single post", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Align counter to left, center or right", "themerex"),
							"divider" => true,
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						), 
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Search
				"trx_search" => array(
					"title" => esc_html__("Search", "themerex"),
					"desc" => esc_html__("Show search form", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Style", "themerex"),
							"desc" => esc_html__("Select style to display search field", "themerex"),
							"value" => "regular",
							"options" => array(
								"regular" => esc_html__('Regular', 'themerex'),
								"rounded" => esc_html__('Rounded', 'themerex')
							),
							"type" => "checklist"
						),
						"state" => array(
							"title" => esc_html__("State", "themerex"),
							"desc" => esc_html__("Select search field initial state", "themerex"),
							"value" => "fixed",
							"options" => array(
								"fixed"  => esc_html__('Fixed',  'themerex'),
								"opened" => esc_html__('Opened', 'themerex'),
								"closed" => esc_html__('Closed', 'themerex')
							),
							"type" => "checklist"
						),
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title (placeholder) for the search field", "themerex"),
							"value" => esc_html__("Search &hellip;", 'themerex'),
							"type" => "text"
						),
						"ajax" => array(
							"title" => esc_html__("AJAX", "themerex"),
							"desc" => esc_html__("Search via AJAX or reload page", "themerex"),
							"value" => "yes",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Section
				"trx_section" => array(
					"title" => esc_html__("Section container", "themerex"),
					"desc" => esc_html__("Container for any block with desired class and style", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"dedicated" => array(
							"title" => esc_html__("Dedicated", "themerex"),
							"desc" => esc_html__("Use this block as dedicated content - show it before post title on single page", "themerex"),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
                        "reverse" => array(
                            "title" => esc_html__("Change the color", "themerex"),
                            "desc" => esc_html__("Change the color of text", "themerex"),
                            "value" => "no",
                            "size" => "small",
                            "options" => $THEMEREX_GLOBALS['sc_params']['yes_no'],
                            "type" => "switch"
                        ),
						"align" => array(
							"title" => esc_html__("Align", "themerex"),
							"desc" => esc_html__("Select block alignment", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"columns" => array(
							"title" => esc_html__("Columns emulation", "themerex"),
							"desc" => esc_html__("Select width for columns emulation", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['columns']
						), 
						"pan" => array(
							"title" => esc_html__("Use pan effect", "themerex"),
							"desc" => esc_html__("Use pan effect to show section content", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", "themerex"),
							"desc" => esc_html__("Use scroller to show section content", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"scroll_dir" => array(
							"title" => esc_html__("Scroll and Pan direction", "themerex"),
							"desc" => esc_html__("Scroll and Pan direction (if Use scroller = yes or Pan = yes)", "themerex"),
							"dependency" => array(
								'pan' => array('yes'),
								'scroll' => array('yes')
							),
							"value" => "horizontal",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['dir']
						),
						"scroll_controls" => array(
							"title" => esc_html__("Scroll controls", "themerex"),
							"desc" => esc_html__("Show scroll controls (if Use scroller = yes)", "themerex"),
							"dependency" => array(
								'scroll' => array('yes')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", "themerex"),
							"desc" => esc_html__("Select color scheme for this block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['schemes']
						),
						"color" => array(
							"title" => esc_html__("Fore color", "themerex"),
							"desc" => esc_html__("Any color for objects in this section", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", "themerex"),
							"desc" => esc_html__("Any background color for this section", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image URL", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for the background", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
                        "bg_tile" => array(
                           "title" => esc_html__("Tile background image", "themerex"),
                            "desc" => esc_html__("Do you want tile background image or image cover whole block?", "themerex"),
                            "value" => "no",
                            "dependency" => array(
                             'bg_image' => array('not_empty')
                             ),
                            "type" => "switch",
                            "options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
                             ),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", "themerex"),
							"desc" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", "themerex"),
							"desc" => esc_html__("Predefined texture style from 1 to 11. 0 - without texture.", "themerex"),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_padding" => array(
							"title" => esc_html__("Paddings around content", "themerex"),
							"desc" => esc_html__("Add paddings around content in this section (only if bg_color or bg_image enabled).", "themerex"),
							"value" => "yes",
							"dependency" => array(
								'compare' => 'or',
								'bg_color' => array('not_empty'),
								'bg_texture' => array('not_empty'),
								'bg_image' => array('not_empty')
							),
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"font_size" => array(
                            "title" => esc_html__("Font size", "themerex"),
							"desc" => esc_html__("Font size of the text (default - in pixels, allows any CSS units of measure)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", "themerex"),
							"desc" => esc_html__("Font weight of the text", "themerex"),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'100' => esc_html__('Thin (100)', 'themerex'),
								'300' => esc_html__('Light (300)', 'themerex'),
								'400' => esc_html__('Normal (400)', 'themerex'),
								'700' => esc_html__('Bold (700)', 'themerex')
							)
						),
						"_content_" => array(
							"title" => esc_html__("Container content", "themerex"),
							"desc" => esc_html__("Content for section container", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
				// Skills
				"trx_skills" => array(
					"title" => esc_html__("Skills", "themerex"),
					"desc" => esc_html__("Insert skills diagramm in your page (post)", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"max_value" => array(
							"title" => esc_html__("Max value", "themerex"),
							"desc" => esc_html__("Max value for skills items", "themerex"),
							"value" => 100,
							"min" => 1,
							"type" => "spinner"
						),
						"type" => array(
							"title" => esc_html__("Skills type", "themerex"),
							"desc" => esc_html__("Select type of skills block", "themerex"),
							"value" => "bar",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'bar' => esc_html__('Bar', 'themerex'),
								'pie' => esc_html__('Pie chart', 'themerex'),
								'counter' => esc_html__('Counter', 'themerex'),
								'arc' => esc_html__('Arc', 'themerex')
							)
						), 
						"layout" => array(
							"title" => esc_html__("Skills layout", "themerex"),
							"desc" => esc_html__("Select layout of skills block", "themerex"),
							"dependency" => array(
								'type' => array('counter','pie','bar')
							),
							"value" => "rows",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								'rows' => esc_html__('Rows', 'themerex'),
								'columns' => esc_html__('Columns', 'themerex')
							)
						),
						"dir" => array(
							"title" => esc_html__("Direction", "themerex"),
							"desc" => esc_html__("Select direction of skills block", "themerex"),
							"dependency" => array(
								'type' => array('counter','pie','bar')
							),
							"value" => "horizontal",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['dir']
						), 
						"style" => array(
							"title" => esc_html__("Counters style", "themerex"),
							"desc" => esc_html__("Select style of skills items (only for type=counter)", "themerex"),
							"dependency" => array(
								'type' => array('counter')
							),
							"value" => 1,
							"options" => themerex_get_list_styles(1, 4),
							"type" => "checklist"
						), 
						// "columns" - autodetect, not set manual
						"color" => array(
							"title" => esc_html__("Skills items color", "themerex"),
							"desc" => esc_html__("Color for all skills items", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "color"
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", "themerex"),
							"desc" => esc_html__("Background color for all skills items (only for type=pie)", "themerex"),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => "",
							"type" => "color"
						),
						"border_color" => array(
							"title" => esc_html__("Border color", "themerex"),
							"desc" => esc_html__("Border color for all skills items (only for type=pie)", "themerex"),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => "",
							"type" => "color"
						),
						"align" => array(
							"title" => esc_html__("Align skills block", "themerex"),
							"desc" => esc_html__("Align skills block to left or right side", "themerex"),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['float']
						), 
						"arc_caption" => array(
							"title" => esc_html__("Arc Caption", "themerex"),
							"desc" => esc_html__("Arc caption - text in the center of the diagram", "themerex"),
							"dependency" => array(
								'type' => array('arc')
							),
							"value" => "",
							"type" => "text"
						),
						"pie_compact" => array(
							"title" => esc_html__("Pie compact", "themerex"),
							"desc" => esc_html__("Show all skills in one diagram or as separate diagrams", "themerex"),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"pie_cutout" => array(
							"title" => esc_html__("Pie cutout", "themerex"),
							"desc" => esc_html__("Pie cutout (0-99). 0 - without cutout, 99 - max cutout", "themerex"),
							"dependency" => array(
								'type' => array('pie')
							),
							"value" => 0,
							"min" => 0,
							"max" => 99,
							"type" => "spinner"
						),
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Title for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"subtitle" => array(
							"title" => esc_html__("Subtitle", "themerex"),
							"desc" => esc_html__("Subtitle for the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"description" => array(
							"title" => esc_html__("Description", "themerex"),
							"desc" => esc_html__("Short description for the block", "themerex"),
							"value" => "",
							"type" => "textarea"
						),
						"link" => array(
							"title" => esc_html__("Button URL", "themerex"),
							"desc" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"link_caption" => array(
							"title" => esc_html__("Button caption", "themerex"),
							"desc" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_skills_item",
						"title" => esc_html__("Skill", "themerex"),
						"desc" => esc_html__("Skills item", "themerex"),
						"container" => false,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Title", "themerex"),
								"desc" => esc_html__("Current skills item title", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"value" => array(
								"title" => esc_html__("Value", "themerex"),
								"desc" => esc_html__("Current skills level", "themerex"),
								"value" => 50,
								"min" => 0,
								"step" => 1,
								"type" => "spinner"
							),
							"color" => array(
								"title" => esc_html__("Color", "themerex"),
								"desc" => esc_html__("Current skills item color", "themerex"),
								"value" => "",
								"type" => "color"
							),
							"bg_color" => array(
								"title" => esc_html__("Background color", "themerex"),
								"desc" => esc_html__("Current skills item background color (only for type=pie)", "themerex"),
								"value" => "",
								"type" => "color"
							),
							"border_color" => array(
								"title" => esc_html__("Border color", "themerex"),
								"desc" => esc_html__("Current skills item border color (only for type=pie)", "themerex"),
								"value" => "",
								"type" => "color"
							),
							"style" => array(
								"title" => esc_html__("Counter style", "themerex"),
								"desc" => esc_html__("Select style for the current skills item (only for type=counter)", "themerex"),
								"value" => 1,
								"options" => themerex_get_list_styles(1, 4),
								"type" => "checklist"
							), 
							"icon" => array(
								"title" => esc_html__("Counter icon",  'themerex'),
								"desc" => esc_html__('Select icon from Fontello icons set, placed above counter (only for type=counter)',  'themerex'),
								"value" => "",
								"type" => "icons",
								"options" => $THEMEREX_GLOBALS['sc_params']['icons']
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Slider
				"trx_slider" => array(
					"title" => esc_html__("Slider", "themerex"),
					"desc" => esc_html__("Insert slider into your post (page)", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array_merge(array(
						"engine" => array(
							"title" => esc_html__("Slider engine", "themerex"),
							"desc" => esc_html__("Select engine for slider. Attention! Swiper is built-in engine, all other engines appears only if corresponding plugings are installed", "themerex"),
							"value" => "swiper",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['sliders']
						),
						"align" => array(
							"title" => esc_html__("Float slider", "themerex"),
							"desc" => esc_html__("Float slider to left or right side", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['float']
						),
						"custom" => array(
							"title" => esc_html__("Custom slides", "themerex"),
							"desc" => esc_html__("Make custom slides from inner shortcodes (prepare it on tabs) or prepare slides from posts thumbnails", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						)
						),
						themerex_exists_revslider() ? array(
						"alias" => array(
							"title" => esc_html__("Revolution slider alias", "themerex"),
							"desc" => esc_html__("Select Revolution slider to display", "themerex"),
							"dependency" => array(
								'engine' => array('revo')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['revo_sliders']
						)) : array(), array(
						"cat" => array(
							"title" => esc_html__("Swiper: Category list", "themerex"),
							"desc" => esc_html__("Select category to show post's images. If empty - select posts from any category or from IDs list", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"divider" => true,
							"value" => "",
							"type" => "select",
							"style" => "list",
							"multiple" => true,
							"options" => themerex_array_merge(array(0 => esc_html__('- Select category -', 'themerex')), $THEMEREX_GLOBALS['sc_params']['categories'])
						),
						"count" => array(
							"title" => esc_html__("Swiper: Number of posts", "themerex"),
							"desc" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 3,
							"min" => 1,
							"max" => 100,
							"type" => "spinner"
						),
						"offset" => array(
							"title" => esc_html__("Swiper: Offset before select posts", "themerex"),
							"desc" => esc_html__("Skip posts before select next part.", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 0,
							"min" => 0,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Swiper: Post order by", "themerex"),
							"desc" => esc_html__("Select desired posts sorting method", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "date",
							"type" => "select",
							"options" => $THEMEREX_GLOBALS['sc_params']['sorting']
						),
						"order" => array(
							"title" => esc_html__("Swiper: Post order", "themerex"),
							"desc" => esc_html__("Select desired posts order", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						),
						"ids" => array(
							"title" => esc_html__("Swiper: Post IDs list", "themerex"),
							"desc" => esc_html__("Comma separated list of posts ID. If set - parameters above are ignored!", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "",
							"type" => "text"
						),
						"controls" => array(
							"title" => esc_html__("Swiper: Show slider controls", "themerex"),
							"desc" => esc_html__("Show arrows inside slider", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"pagination" => array(
							"title" => esc_html__("Swiper: Show slider pagination", "themerex"),
							"desc" => esc_html__("Show bullets for switch slides", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "no",
							"type" => "checklist",
							"options" => array(
								'no'   => esc_html__('None', 'themerex'),
								'yes'  => esc_html__('Dots', 'themerex'),
								'full' => esc_html__('Side Titles', 'themerex'),
								'over' => esc_html__('Over Titles', 'themerex')
							)
						),
						"titles" => array(
							"title" => esc_html__("Swiper: Show titles section", "themerex"),
							"desc" => esc_html__("Show section with post's title and short post's description", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"divider" => true,
							"value" => "no",
							"type" => "checklist",
							"options" => array(
								"no"    => esc_html__('Not show', 'themerex'),
								"slide" => esc_html__('Show/Hide info', 'themerex'),
								"fixed" => esc_html__('Fixed info', 'themerex')
							)
						),
						"descriptions" => array(
							"title" => esc_html__("Swiper: Post descriptions", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"desc" => esc_html__("Show post's excerpt max length (characters)", "themerex"),
							"value" => 0,
							"min" => 0,
							"max" => 1000,
							"step" => 10,
							"type" => "spinner"
						),
						"links" => array(
							"title" => esc_html__("Swiper: Post's title as link", "themerex"),
							"desc" => esc_html__("Make links from post's titles", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"crop" => array(
							"title" => esc_html__("Swiper: Crop images", "themerex"),
							"desc" => esc_html__("Crop images in each slide or live it unchanged", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"autoheight" => array(
							"title" => esc_html__("Swiper: Autoheight", "themerex"),
							"desc" => esc_html__("Change whole slider's height (make it equal current slide's height)", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"slides_per_view" => array(
							"title" => esc_html__("Swiper: Slides per view", "themerex"),
							"desc" => esc_html__("Slides per view showed in this slider", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 1,
							"min" => 1,
							"max" => 6,
							"step" => 1,
							"type" => "spinner"
						),
						"slides_space" => array(
							"title" => esc_html__("Swiper: Space between slides", "themerex"),
							"desc" => esc_html__("Size of space (in px) between slides", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 0,
							"min" => 0,
							"max" => 100,
							"step" => 10,
							"type" => "spinner"
						),
						"interval" => array(
							"title" => esc_html__("Swiper: Slides change interval", "themerex"),
							"desc" => esc_html__("Slides change interval (in milliseconds: 1000ms = 1s)", "themerex"),
							"dependency" => array(
								'engine' => array('swiper')
							),
							"value" => 5000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)),
					"children" => array(
						"name" => "trx_slider_item",
						"title" => esc_html__("Slide", "themerex"),
						"desc" => esc_html__("Slider item", "themerex"),
						"container" => false,
						"params" => array(
							"src" => array(
								"title" => esc_html__("URL (source) for image file", "themerex"),
								"desc" => esc_html__("Select or upload image or write URL from other site for the current slide", "themerex"),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
				// Socials
				"trx_socials" => array(
					"title" => esc_html__("Social icons", "themerex"),
					"desc" => esc_html__("List of social icons (with hovers)", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"type" => array(
							"title" => esc_html__("Icon's type", "themerex"),
							"desc" => esc_html__("Type of the icons - images or font icons", "themerex"),
							"value" => themerex_get_theme_setting('socials_type'),
							"options" => array(
								'icons' => esc_html__('Icons', 'themerex'),
								'images' => esc_html__('Images', 'themerex')
							),
							"type" => "checklist"
						), 
						"size" => array(
							"title" => esc_html__("Icon's size", "themerex"),
							"desc" => esc_html__("Size of the icons", "themerex"),
							"value" => "small",
							"options" => $THEMEREX_GLOBALS['sc_params']['sizes'],
							"type" => "checklist"
						), 
						"shape" => array(
							"title" => esc_html__("Icon's shape", "themerex"),
							"desc" => esc_html__("Shape of the icons", "themerex"),
							"value" => "square",
							"options" => $THEMEREX_GLOBALS['sc_params']['shapes'],
							"type" => "checklist"
						), 
						"socials" => array(
							"title" => esc_html__("Manual socials list", "themerex"),
							"desc" => esc_html__("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebooc.com/my_profile. If empty - use socials from Theme options.", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "text"
						),
						"custom" => array(
							"title" => esc_html__("Custom socials", "themerex"),
							"desc" => esc_html__("Make custom icons from inner shortcodes (prepare it on tabs)", "themerex"),
							"divider" => true,
							"value" => "no",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no'],
							"type" => "switch"
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_social_item",
						"title" => esc_html__("Custom social item", "themerex"),
						"desc" => esc_html__("Custom social item: name, profile url and icon url", "themerex"),
						"decorate" => false,
						"container" => false,
						"params" => array(
							"name" => array(
								"title" => esc_html__("Social name", "themerex"),
								"desc" => esc_html__("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"url" => array(
								"title" => esc_html__("Your profile URL", "themerex"),
								"desc" => esc_html__("URL of your profile in specified social network", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"icon" => array(
								"title" => esc_html__("URL (source) for icon file", "themerex"),
								"desc" => esc_html__("Select or upload image or write URL from other site for the current social icon", "themerex"),
								"readonly" => false,
								"value" => "",
								"type" => "media"
							)
						)
					)
				),
			
			
			
			
				// Table
				"trx_table" => array(
					"title" => esc_html__("Table", "themerex"),
					"desc" => esc_html__("Insert a table into post (page). ", "themerex"),
					"decorate" => true,
					"container" => true,
					"params" => array(
						"align" => array(
							"title" => esc_html__("Content alignment", "themerex"),
							"desc" => esc_html__("Select alignment for each table cell", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"_content_" => array(
							"title" => esc_html__("Table content", "themerex"),
							"desc" => esc_html__("Content, created with any table-generator", "themerex"),
							"divider" => true,
							"rows" => 8,
							"value" => "Paste here table content, generated on one of many public internet resources, for example: http://www.impressivewebs.com/html-table-code-generator/ or http://html-tables.com/",
							"type" => "textarea"
						),
						"width" => themerex_shortcodes_width(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Tabs
				"trx_tabs" => array(
					"title" => esc_html__("Tabs", "themerex"),
					"desc" => esc_html__("Insert tabs in your page (post)", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Tabs style", "themerex"),
							"desc" => esc_html__("Select style for tabs items", "themerex"),
							"value" => 1,
							"options" => themerex_get_list_styles(1, 2),
							"type" => "radio"
						),

						"initial" => array(
							"title" => esc_html__("Initially opened tab", "themerex"),
							"desc" => esc_html__("Number of initially opened tab", "themerex"),
							"divider" => true,
							"value" => 1,
							"min" => 0,
							"type" => "spinner"
						),
						"scroll" => array(
							"title" => esc_html__("Use scroller", "themerex"),
							"desc" => esc_html__("Use scroller to show tab content (height parameter required)", "themerex"),
							"divider" => true,
							"value" => "no",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_tab",
						"title" => esc_html__("Tab", "themerex"),
						"desc" => esc_html__("Tab item", "themerex"),
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Tab title", "themerex"),
								"desc" => esc_html__("Current tab title", "themerex"),
								"value" => "",
								"type" => "text"
							),
                            "icon_title" => array(
                                "title" => esc_html__("Icon before title",  'themerex'),
                                "desc" => esc_html__('Select icon for the title tabs item from Fontello icons set',  'themerex'),
                                "value" => "",
                                "type" => "icons",
                                "options" => $THEMEREX_GLOBALS['sc_params']['icons']
                            ),
							"_content_" => array(
								"title" => esc_html__("Tab content", "themerex"),
								"desc" => esc_html__("Current tab content", "themerex"),
								"divider" => true,
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			


				
			
			
				// Title
				"trx_title" => array(
					"title" => esc_html__("Title", "themerex"),
					"desc" => esc_html__("Create header tag (1-6 level) with many styles", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"_content_" => array(
							"title" => esc_html__("Title content", "themerex"),
							"desc" => esc_html__("Title content", "themerex"),
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"type" => array(
							"title" => esc_html__("Title type", "themerex"),
							"desc" => esc_html__("Title type (header level)", "themerex"),
							"divider" => true,
							"value" => "1",
							"type" => "select",
							"options" => array(
								'1' => esc_html__('Header 1', 'themerex'),
								'2' => esc_html__('Header 2', 'themerex'),
								'3' => esc_html__('Header 3', 'themerex'),
								'4' => esc_html__('Header 4', 'themerex'),
								'5' => esc_html__('Header 5', 'themerex'),
								'6' => esc_html__('Header 6', 'themerex'),
							)
						),
                        "devider_styling" => array(
                            "title" => esc_html__('Additional devider', "themerex"),
                            "desc" => esc_html__("Select devider for title.", "themerex"),
                            "readonly" => false,
                            "value" => "0",
                            "type" => "select",
                            "options" => array(
                                '0' => esc_html__('None', 'themerex'),
                                '1' => esc_html__('Line before title', 'themerex'),
                                '2' => esc_html__('Devider after title', 'themerex')
                            )
                        ),
						"style" => array(
							"title" => esc_html__("Title style", "themerex"),
							"desc" => esc_html__("Title style", "themerex"),
							"value" => "regular",
							"type" => "select",
							"options" => array(
								'regular' => esc_html__('Regular', 'themerex'),
								'underline' => esc_html__('Underline', 'themerex'),
								'divider' => esc_html__('Divider', 'themerex'),
								'iconed' => esc_html__('With icon (image)', 'themerex')
							)
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Title text alignment", "themerex"),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						), 
						"font_size" => array(
							"title" => esc_html__("Font_size", "themerex"),
							"desc" => esc_html__("Custom font size. If empty - use theme default", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"font_weight" => array(
							"title" => esc_html__("Font weight", "themerex"),
							"desc" => esc_html__("Custom font weight. If empty or inherit - use theme default", "themerex"),
							"value" => "",
							"type" => "select",
							"size" => "medium",
							"options" => array(
								'inherit' => esc_html__('Default', 'themerex'),
								'100' => esc_html__('Thin (100)', 'themerex'),
								'300' => esc_html__('Light (300)', 'themerex'),
								'400' => esc_html__('Normal (400)', 'themerex'),
								'600' => esc_html__('Semibold (600)', 'themerex'),
								'700' => esc_html__('Bold (700)', 'themerex'),
								'900' => esc_html__('Black (900)', 'themerex')
							)
						),
						"color" => array(
							"title" => esc_html__("Title color", "themerex"),
							"desc" => esc_html__("Select color for the title", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"icon" => array(
							"title" => esc_html__('Title font icon',  'themerex'),
							"desc" => esc_html__("Select font icon for the title from Fontello icons set (if style=iconed)",  'themerex'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"image" => array(
							"title" => esc_html__('or image icon',  'themerex'),
							"desc" => esc_html__("Select image icon for the title instead icon above (if style=iconed)",  'themerex'),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "",
							"type" => "images",
							"size" => "small",
							"options" => $THEMEREX_GLOBALS['sc_params']['images']
						),
						"picture" => array(
							"title" => esc_html__('or URL for image file', "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site (if style=iconed)", "themerex"),
							"dependency" => array(
								'style' => array('iconed')
							),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"image_size" => array(
							"title" => esc_html__('Image (picture) size', "themerex"),
							"desc" => esc_html__("Select image (picture) size (if style='iconed')", "themerex"),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "small",
							"type" => "checklist",
							"options" => array(
								'small' => esc_html__('Small', 'themerex'),
								'medium' => esc_html__('Medium', 'themerex'),
								'large' => esc_html__('Large', 'themerex')
							)
						),
						"position" => array(
							"title" => esc_html__('Icon (image) position', "themerex"),
							"desc" => esc_html__("Select icon (image) position (if style=iconed)", "themerex"),
							"dependency" => array(
								'style' => array('iconed')
							),
							"value" => "left",
							"type" => "checklist",
							"options" => array(
								'top' => esc_html__('Top', 'themerex'),
								'left' => esc_html__('Left', 'themerex')
							)
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
			
				// Toggles
				"trx_toggles" => array(
					"title" => esc_html__("Toggles", "themerex"),
					"desc" => esc_html__("Toggles items", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"style" => array(
							"title" => esc_html__("Toggles style", "themerex"),
							"desc" => esc_html__("Select style for display toggles", "themerex"),
							"value" => 1,
							"options" => themerex_get_list_styles(1, 2),
							"type" => "radio"
						),
						"counter" => array(
							"title" => esc_html__("Counter", "themerex"),
							"desc" => esc_html__("Display counter before each toggles title", "themerex"),
							"value" => "off",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['on_off']
						),
						"icon_closed" => array(
							"title" => esc_html__("Icon while closed",  'themerex'),
							"desc" => esc_html__('Select icon for the closed toggles item from Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"icon_opened" => array(
							"title" => esc_html__("Icon while opened",  'themerex'),
							"desc" => esc_html__('Select icon for the opened toggles item from Fontello icons set',  'themerex'),
							"value" => "",
							"type" => "icons",
							"options" => $THEMEREX_GLOBALS['sc_params']['icons']
						),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					),
					"children" => array(
						"name" => "trx_toggles_item",
						"title" => esc_html__("Toggles item", "themerex"),
						"desc" => esc_html__("Toggles item", "themerex"),
						"container" => true,
						"params" => array(
							"title" => array(
								"title" => esc_html__("Toggles item title", "themerex"),
								"desc" => esc_html__("Title for current toggles item", "themerex"),
								"value" => "",
								"type" => "text"
							),
							"open" => array(
								"title" => esc_html__("Open on show", "themerex"),
								"desc" => esc_html__("Open current toggles item on show", "themerex"),
								"value" => "no",
								"type" => "switch",
								"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
							),
							"icon_closed" => array(
								"title" => esc_html__("Icon while closed",  'themerex'),
								"desc" => esc_html__('Select icon for the closed toggles item from Fontello icons set',  'themerex'),
								"value" => "",
								"type" => "icons",
								"options" => $THEMEREX_GLOBALS['sc_params']['icons']
							),
							"icon_opened" => array(
								"title" => esc_html__("Icon while opened",  'themerex'),
								"desc" => esc_html__('Select icon for the opened toggles item from Fontello icons set',  'themerex'),
								"value" => "",
								"type" => "icons",
								"options" => $THEMEREX_GLOBALS['sc_params']['icons']
							),
							"_content_" => array(
								"title" => esc_html__("Toggles item content", "themerex"),
								"desc" => esc_html__("Current toggles item content", "themerex"),
								"rows" => 4,
								"value" => "",
								"type" => "textarea"
							),
							"id" => $THEMEREX_GLOBALS['sc_params']['id'],
							"class" => $THEMEREX_GLOBALS['sc_params']['class'],
							"css" => $THEMEREX_GLOBALS['sc_params']['css']
						)
					)
				),
			
			
			
			
			
				// Tooltip
				"trx_tooltip" => array(
					"title" => esc_html__("Tooltip", "themerex"),
					"desc" => esc_html__("Create tooltip for selected text", "themerex"),
					"decorate" => false,
					"container" => true,
					"params" => array(
						"title" => array(
							"title" => esc_html__("Title", "themerex"),
							"desc" => esc_html__("Tooltip title (required)", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"_content_" => array(
							"title" => esc_html__("Tipped content", "themerex"),
							"desc" => esc_html__("Highlighted content with tooltip", "themerex"),
							"divider" => true,
							"rows" => 4,
							"value" => "",
							"type" => "textarea"
						),
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Twitter
				"trx_twitter" => array(
					"title" => esc_html__("Twitter", "themerex"),
					"desc" => esc_html__("Insert twitter feed into post (page)", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"user" => array(
							"title" => esc_html__("Twitter Username", "themerex"),
							"desc" => esc_html__("Your username in the twitter account. If empty - get it from Theme Options.", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"consumer_key" => array(
							"title" => esc_html__("Consumer Key", "themerex"),
							"desc" => esc_html__("Consumer Key from the twitter account", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"consumer_secret" => array(
							"title" => esc_html__("Consumer Secret", "themerex"),
							"desc" => esc_html__("Consumer Secret from the twitter account", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"token_key" => array(
							"title" => esc_html__("Token Key", "themerex"),
							"desc" => esc_html__("Token Key from the twitter account", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"token_secret" => array(
							"title" => esc_html__("Token Secret", "themerex"),
							"desc" => esc_html__("Token Secret from the twitter account", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"count" => array(
							"title" => esc_html__("Tweets number", "themerex"),
							"desc" => esc_html__("Tweets number to show", "themerex"),
							"divider" => true,
							"value" => 3,
							"max" => 20,
							"min" => 1,
							"type" => "spinner"
						),
						"controls" => array(
							"title" => esc_html__("Show arrows", "themerex"),
							"desc" => esc_html__("Show control buttons", "themerex"),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"interval" => array(
							"title" => esc_html__("Tweets change interval", "themerex"),
							"desc" => esc_html__("Tweets change interval (in milliseconds: 1000ms = 1s)", "themerex"),
							"value" => 7000,
							"step" => 500,
							"min" => 0,
							"type" => "spinner"
						),
						"align" => array(
							"title" => esc_html__("Alignment", "themerex"),
							"desc" => esc_html__("Alignment of the tweets block", "themerex"),
							"divider" => true,
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"autoheight" => array(
							"title" => esc_html__("Autoheight", "themerex"),
							"desc" => esc_html__("Change whole slider's height (make it equal current slide's height)", "themerex"),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						),
						"scheme" => array(
							"title" => esc_html__("Color scheme", "themerex"),
							"desc" => esc_html__("Select color scheme for this block", "themerex"),
							"value" => "",
							"type" => "checklist",
							"options" => $THEMEREX_GLOBALS['sc_params']['schemes']
						),
						"bg_color" => array(
							"title" => esc_html__("Background color", "themerex"),
							"desc" => esc_html__("Any background color for this section", "themerex"),
							"value" => "",
							"type" => "color"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image URL", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for the background", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_overlay" => array(
							"title" => esc_html__("Overlay", "themerex"),
							"desc" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
							"min" => "0",
							"max" => "1",
							"step" => "0.1",
							"value" => "0",
							"type" => "spinner"
						),
						"bg_texture" => array(
							"title" => esc_html__("Texture", "themerex"),
							"desc" => esc_html__("Predefined texture style from 1 to 11. 0 - without texture.", "themerex"),
							"min" => "0",
							"max" => "11",
							"step" => "1",
							"value" => "0",
							"type" => "spinner"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
				// Video
				"trx_video" => array(
					"title" => esc_html__("Video", "themerex"),
					"desc" => esc_html__("Insert video player", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"url" => array(
							"title" => esc_html__("URL for video file", "themerex"),
							"desc" => esc_html__("Select video from media library or paste URL for video file from other site", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media",
							"before" => array(
								'title' => esc_html__('Choose video', 'themerex'),
								'action' => 'media_upload',
								'type' => 'video',
								'multiple' => false,
								'linked_field' => '',
								'captions' => array( 	
									'choose' => esc_html__('Choose video file', 'themerex'),
									'update' => esc_html__('Select video file', 'themerex')
								)
							),
							"after" => array(
								'icon' => 'icon-cancel',
								'action' => 'media_reset'
							)
						),
						"ratio" => array(
							"title" => esc_html__("Ratio", "themerex"),
							"desc" => esc_html__("Ratio of the video", "themerex"),
							"value" => "16:9",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => array(
								"16:9" => esc_html__("16:9", 'themerex'),
								"4:3" => esc_html__("4:3", 'themerex')
							)
						),
						"autoplay" => array(
							"title" => esc_html__("Autoplay video", "themerex"),
							"desc" => esc_html__("Autoplay video on page load", "themerex"),
							"value" => "off",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['on_off']
						),
						"align" => array(
							"title" => esc_html__("Align", "themerex"),
							"desc" => esc_html__("Select block alignment", "themerex"),
							"value" => "none",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['align']
						),
						"image" => array(
							"title" => esc_html__("Cover image", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for video preview", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_image" => array(
							"title" => esc_html__("Background image", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for video background. Attention! If you use background image - specify paddings below from background margins to video block in percents!", "themerex"),
							"divider" => true,
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_top" => array(
							"title" => esc_html__("Top offset", "themerex"),
							"desc" => esc_html__("Top offset (padding) inside background image to video block (in percent). For example: 3%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_bottom" => array(
							"title" => esc_html__("Bottom offset", "themerex"),
							"desc" => esc_html__("Bottom offset (padding) inside background image to video block (in percent). For example: 3%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_left" => array(
							"title" => esc_html__("Left offset", "themerex"),
							"desc" => esc_html__("Left offset (padding) inside background image to video block (in percent). For example: 20%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_right" => array(
							"title" => esc_html__("Right offset", "themerex"),
							"desc" => esc_html__("Right offset (padding) inside background image to video block (in percent). For example: 12%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				),
			
			
			
			
				// Zoom
				"trx_zoom" => array(
					"title" => esc_html__("Zoom", "themerex"),
					"desc" => esc_html__("Insert the image with zoom/lens effect", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"effect" => array(
							"title" => esc_html__("Effect", "themerex"),
							"desc" => esc_html__("Select effect to display overlapping image", "themerex"),
							"value" => "lens",
							"size" => "medium",
							"type" => "switch",
							"options" => array(
								"lens" => esc_html__('Lens', 'themerex'),
								"zoom" => esc_html__('Zoom', 'themerex')
							)
						),
						"url" => array(
							"title" => esc_html__("Main image", "themerex"),
							"desc" => esc_html__("Select or upload main image", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"over" => array(
							"title" => esc_html__("Overlaping image", "themerex"),
							"desc" => esc_html__("Select or upload overlaping image", "themerex"),
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"align" => array(
							"title" => esc_html__("Float zoom", "themerex"),
							"desc" => esc_html__("Float zoom to left or right side", "themerex"),
							"value" => "",
							"type" => "checklist",
							"dir" => "horizontal",
							"options" => $THEMEREX_GLOBALS['sc_params']['float']
						), 
						"bg_image" => array(
							"title" => esc_html__("Background image", "themerex"),
							"desc" => esc_html__("Select or upload image or write URL from other site for zoom block background. Attention! If you use background image - specify paddings below from background margins to zoom block in percents!", "themerex"),
							"divider" => true,
							"readonly" => false,
							"value" => "",
							"type" => "media"
						),
						"bg_top" => array(
							"title" => esc_html__("Top offset", "themerex"),
							"desc" => esc_html__("Top offset (padding) inside background image to zoom block (in percent). For example: 3%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_bottom" => array(
							"title" => esc_html__("Bottom offset", "themerex"),
							"desc" => esc_html__("Bottom offset (padding) inside background image to zoom block (in percent). For example: 3%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_left" => array(
							"title" => esc_html__("Left offset", "themerex"),
							"desc" => esc_html__("Left offset (padding) inside background image to zoom block (in percent). For example: 20%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"bg_right" => array(
							"title" => esc_html__("Right offset", "themerex"),
							"desc" => esc_html__("Right offset (padding) inside background image to zoom block (in percent). For example: 12%", "themerex"),
							"dependency" => array(
								'bg_image' => array('not_empty')
							),
							"value" => "",
							"type" => "text"
						),
						"width" => themerex_shortcodes_width(),
						"height" => themerex_shortcodes_height(),
						"top" => $THEMEREX_GLOBALS['sc_params']['top'],
						"bottom" => $THEMEREX_GLOBALS['sc_params']['bottom'],
						"left" => $THEMEREX_GLOBALS['sc_params']['left'],
						"right" => $THEMEREX_GLOBALS['sc_params']['right'],
						"id" => $THEMEREX_GLOBALS['sc_params']['id'],
						"class" => $THEMEREX_GLOBALS['sc_params']['class'],
						"animation" => $THEMEREX_GLOBALS['sc_params']['animation'],
						"css" => $THEMEREX_GLOBALS['sc_params']['css']
					)
				)
			);
	
			// Woocommerce Shortcodes list
			//------------------------------------------------------------------
			if (themerex_exists_woocommerce()) {
				
				// WooCommerce - Cart
				$THEMEREX_GLOBALS['shortcodes']["woocommerce_cart"] = array(
					"title" => esc_html__("Woocommerce: Cart", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show Cart page", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Checkout
				$THEMEREX_GLOBALS['shortcodes']["woocommerce_checkout"] = array(
					"title" => esc_html__("Woocommerce: Checkout", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show Checkout page", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - My Account
				$THEMEREX_GLOBALS['shortcodes']["woocommerce_my_account"] = array(
					"title" => esc_html__("Woocommerce: My Account", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show My Account page", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Order Tracking
				$THEMEREX_GLOBALS['shortcodes']["woocommerce_order_tracking"] = array(
					"title" => esc_html__("Woocommerce: Order Tracking", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show Order Tracking page", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Shop Messages
				$THEMEREX_GLOBALS['shortcodes']["shop_messages"] = array(
					"title" => esc_html__("Woocommerce: Shop Messages", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show shop messages", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array()
				);
				
				// WooCommerce - Product Page
				$THEMEREX_GLOBALS['shortcodes']["product_page"] = array(
					"title" => esc_html__("Woocommerce: Product Page", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: display single product page", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"sku" => array(
							"title" => esc_html__("SKU", "themerex"),
							"desc" => esc_html__("SKU code of displayed product", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"id" => array(
							"title" => esc_html__("ID", "themerex"),
							"desc" => esc_html__("ID of displayed product", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"posts_per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => "1",
							"min" => 1,
							"type" => "spinner"
						),
						"post_type" => array(
							"title" => esc_html__("Post type", "themerex"),
							"desc" => esc_html__("Post type for the WP query (leave 'product')", "themerex"),
							"value" => "product",
							"type" => "text"
						),
						"post_status" => array(
							"title" => esc_html__("Post status", "themerex"),
							"desc" => esc_html__("Display posts only with this status", "themerex"),
							"value" => "publish",
							"type" => "select",
							"options" => array(
								"publish" => esc_html__('Publish', 'themerex'),
								"protected" => esc_html__('Protected', 'themerex'),
								"private" => esc_html__('Private', 'themerex'),
								"pending" => esc_html__('Pending', 'themerex'),
								"draft" => esc_html__('Draft', 'themerex')
							)
						)
					)
				);
				
				// WooCommerce - Product
				$THEMEREX_GLOBALS['shortcodes']["product"] = array(
					"title" => esc_html__("Woocommerce: Product", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: display one product", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"sku" => array(
							"title" => esc_html__("SKU", "themerex"),
							"desc" => esc_html__("SKU code of displayed product", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"id" => array(
							"title" => esc_html__("ID", "themerex"),
							"desc" => esc_html__("ID of displayed product", "themerex"),
							"value" => "",
							"type" => "text"
						)
					)
				);
				
				// WooCommerce - Best Selling Products
				$THEMEREX_GLOBALS['shortcodes']["best_selling_products"] = array(
					"title" => esc_html__("Woocommerce: Best Selling Products", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show best selling products", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						)
					)
				);
				
				// WooCommerce - Recent Products
				$THEMEREX_GLOBALS['shortcodes']["recent_products"] = array(
					"title" => esc_html__("Woocommerce: Recent Products", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show recent products", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Related Products
				$THEMEREX_GLOBALS['shortcodes']["related_products"] = array(
					"title" => esc_html__("Woocommerce: Related Products", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show related products", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"posts_per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						)
					)
				);
				
				// WooCommerce - Featured Products
				$THEMEREX_GLOBALS['shortcodes']["featured_products"] = array(
					"title" => esc_html__("Woocommerce: Featured Products", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show featured products", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Top Rated Products
				$THEMEREX_GLOBALS['shortcodes']["featured_products"] = array(
					"title" => esc_html__("Woocommerce: Top Rated Products", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show top rated products", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Sale Products
				$THEMEREX_GLOBALS['shortcodes']["featured_products"] = array(
					"title" => esc_html__("Woocommerce: Sale Products", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: list products on sale", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Product Category
				$THEMEREX_GLOBALS['shortcodes']["product_category"] = array(
					"title" => esc_html__("Woocommerce: Products from category", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: list products in specified category(-ies)", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						),
						"category" => array(
							"title" => esc_html__("Categories", "themerex"),
							"desc" => esc_html__("Comma separated category slugs", "themerex"),
							"value" => '',
							"type" => "text"
						),
						"operator" => array(
							"title" => esc_html__("Operator", "themerex"),
							"desc" => esc_html__("Categories operator", "themerex"),
							"value" => "IN",
							"type" => "checklist",
							"size" => "medium",
							"options" => array(
								"IN" => esc_html__('IN', 'themerex'),
								"NOT IN" => esc_html__('NOT IN', 'themerex'),
								"AND" => esc_html__('AND', 'themerex')
							)
						)
					)
				);
				
				// WooCommerce - Products
				$THEMEREX_GLOBALS['shortcodes']["products"] = array(
					"title" => esc_html__("Woocommerce: Products", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: list all products", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"skus" => array(
							"title" => esc_html__("SKUs", "themerex"),
							"desc" => esc_html__("Comma separated SKU codes of products", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"ids" => array(
							"title" => esc_html__("IDs", "themerex"),
							"desc" => esc_html__("Comma separated ID of products", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						)
					)
				);
				
				// WooCommerce - Product attribute
				$THEMEREX_GLOBALS['shortcodes']["product_attribute"] = array(
					"title" => esc_html__("Woocommerce: Products by Attribute", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show products with specified attribute", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"per_page" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many products showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for products output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						),
						"attribute" => array(
							"title" => esc_html__("Attribute", "themerex"),
							"desc" => esc_html__("Attribute name", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"filter" => array(
							"title" => esc_html__("Filter", "themerex"),
							"desc" => esc_html__("Attribute value", "themerex"),
							"value" => "",
							"type" => "text"
						)
					)
				);
				
				// WooCommerce - Products Categories
				$THEMEREX_GLOBALS['shortcodes']["product_categories"] = array(
					"title" => esc_html__("Woocommerce: Product Categories", "themerex"),
					"desc" => esc_html__("WooCommerce shortcode: show categories with products", "themerex"),
					"decorate" => false,
					"container" => false,
					"params" => array(
						"number" => array(
							"title" => esc_html__("Number", "themerex"),
							"desc" => esc_html__("How many categories showed", "themerex"),
							"value" => 4,
							"min" => 1,
							"type" => "spinner"
						),
						"columns" => array(
							"title" => esc_html__("Columns", "themerex"),
							"desc" => esc_html__("How many columns per row use for categories output", "themerex"),
							"value" => 4,
							"min" => 2,
							"max" => 4,
							"type" => "spinner"
						),
						"orderby" => array(
							"title" => esc_html__("Order by", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "date",
							"type" => "select",
							"options" => array(
								"date" => esc_html__('Date', 'themerex'),
								"title" => esc_html__('Title', 'themerex')
							)
						),
						"order" => array(
							"title" => esc_html__("Order", "themerex"),
							"desc" => esc_html__("Sorting order for products output", "themerex"),
							"value" => "desc",
							"type" => "switch",
							"size" => "big",
							"options" => $THEMEREX_GLOBALS['sc_params']['ordering']
						),
						"parent" => array(
							"title" => esc_html__("Parent", "themerex"),
							"desc" => esc_html__("Parent category slug", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"ids" => array(
							"title" => esc_html__("IDs", "themerex"),
							"desc" => esc_html__("Comma separated ID of products", "themerex"),
							"value" => "",
							"type" => "text"
						),
						"hide_empty" => array(
							"title" => esc_html__("Hide empty", "themerex"),
							"desc" => esc_html__("Hide empty categories", "themerex"),
							"value" => "yes",
							"type" => "switch",
							"options" => $THEMEREX_GLOBALS['sc_params']['yes_no']
						)
					)
				);

			}
			
			do_action('themerex_action_shortcodes_list');

		}
	}
}
?>