<?php
/* Calculated fields form support functions
------------------------------------------------------------------------------- */

// Theme init
if (!function_exists('themerex_calcfields_form_theme_setup')) {
	add_action( 'themerex_action_before_init_theme', 'themerex_calcfields_form_theme_setup' );
	function themerex_calcfields_form_theme_setup() {
		// Add shortcode in the shortcodes list
		if (themerex_exists_calcfields_form()) {
			add_action('themerex_action_shortcodes_list',		'themerex_calcfields_form_reg_shortcodes');
			add_action('themerex_action_shortcodes_list_vc',	'themerex_calcfields_form_reg_shortcodes_vc');
		}
	}
}

// Check if plugin installed and activated
if ( !function_exists( 'themerex_exists_calcfields_form' ) ) {
	function themerex_exists_calcfields_form() {
		return function_exists('cp_calculated_fields_form_load_resources');
	}
}

// Return Calculated forms list list, prepended inherit (if need)
if ( !function_exists( 'themerex_get_list_calcfields_form' ) ) {
	function themerex_get_list_calcfields_form($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_calcfields_form']))
			$list = $THEMEREX_GLOBALS['list_calcfields_form'];
		else {
			$list = array();
			if (themerex_exists_calcfields_form()) {
				global $wpdb;
				$rows = $wpdb->get_results( "SELECT id, form_name FROM " . esc_sql($wpdb->prefix . CP_CALCULATEDFIELDSF_FORMS_TABLE) );
				if (is_array($rows) && count($rows) > 0) {
					foreach ($rows as $row) {
						$list[$row->id] = $row->form_name;
					}
				}
			}
			$THEMEREX_GLOBALS['list_calcfields_form'] = $list = apply_filters('themerex_filter_list_calcfields_form', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => esc_html__("Inherit", 'themerex')), $list) : $list;
	}
}



// Add shortcode in the shortcodes list
if (!function_exists('themerex_calcfields_form_reg_shortcodes')) {
	//add_filter('themerex_action_shortcodes_list',	'themerex_calcfields_form_reg_shortcodes');
	function themerex_calcfields_form_reg_shortcodes() {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['shortcodes'])) {

			$forms_list = themerex_get_list_calcfields_form();

			themerex_array_insert_after($THEMEREX_GLOBALS['shortcodes'], 'trx_button', array(

				// Calculated fields form
				'CP_CALCULATED_FIELDS' => array(
					"title" => esc_html__("Calculated fields form", "themerex"),
					"desc" => esc_html__("Insert calculated fields form", "themerex"),
					"decorate" => true,
					"container" => false,
					"params" => array(
						"id" => array(
							"title" => esc_html__("Form ID", "themerex"),
							"desc" => esc_html__("Select Form to insert into current page", "themerex"),
							"value" => "",
							"size" => "medium",
							"options" => $forms_list,
							"type" => "select"
						)
					)
				)

			));
		}
	}
}


// Add shortcode in the VC shortcodes list
if (!function_exists('themerex_calcfields_form_reg_shortcodes_vc')) {
	//add_filter('themerex_action_shortcodes_list_vc',	'themerex_calcfields_form_reg_shortcodes_vc');
	function themerex_calcfields_form_reg_shortcodes_vc() {
		global $THEMEREX_GLOBALS;

		$forms_list = themerex_get_list_calcfields_form();

		// Calculated fields form
		vc_map( array(
				"base" => "CP_CALCULATED_FIELDS",
				"name" => esc_html__("Calculated fields form", "themerex"),
				"description" => esc_html__("Insert calculated fields form", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_calcfields',
				"class" => "trx_sc_single trx_sc_calcfields",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "id",
						"heading" => esc_html__("Form ID", "themerex"),
						"description" => esc_html__("Select Form to insert into current page", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($forms_list),
						"type" => "dropdown"
					)
				)
			) );
			
		class WPBakeryShortCode_Cp_Calculated_Fields extends THEMEREX_VC_ShortCodeSingle {}

	}
}
?>