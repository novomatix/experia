<?php
if (is_admin() 
		|| (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true' )
		|| (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline')
	) {
	require_once themerex_get_file_dir('core/core.shortcodes/shortcodes_vc_classes.php');
}

// Width and height params
if ( !function_exists( 'themerex_vc_width' ) ) {
	function themerex_vc_width($w='') {
		return array(
			"param_name" => "width",
			"heading" => esc_html__("Width", "themerex"),
			"description" => esc_html__("Width (in pixels or percent) of the current element", "themerex"),
			"group" => esc_html__('Size &amp; Margins', 'themerex'),
			"value" => $w,
			"type" => "textfield"
		);
	}
}
if ( !function_exists( 'themerex_vc_height' ) ) {
	function themerex_vc_height($h='') {
		return array(
			"param_name" => "height",
			"heading" => esc_html__("Height", "themerex"),
			"description" => esc_html__("Height (only in pixels) of the current element", "themerex"),
			"group" => esc_html__('Size &amp; Margins', 'themerex'),
			"value" => $h,
			"type" => "textfield"
		);
	}
}

// Load scripts and styles for VC support
if ( !function_exists( 'themerex_shortcodes_vc_scripts_admin' ) ) {
	//add_action( 'admin_enqueue_scripts', 'themerex_shortcodes_vc_scripts_admin' );
	function themerex_shortcodes_vc_scripts_admin() {
		// Include CSS 
		themerex_enqueue_style ( 'shortcodes_vc-style', themerex_get_file_url('core/core.shortcodes/shortcodes_vc_admin.css'), array(), null );
		// Include JS
		themerex_enqueue_script( 'shortcodes_vc-script', themerex_get_file_url('core/core.shortcodes/shortcodes_vc_admin.js'), array(), null, true );
	}
}

// Load scripts and styles for VC support
if ( !function_exists( 'themerex_shortcodes_vc_scripts_front' ) ) {
	//add_action( 'wp_enqueue_scripts', 'themerex_shortcodes_vc_scripts_front' );
	function themerex_shortcodes_vc_scripts_front() {
		if (themerex_vc_is_frontend()) {
			// Include CSS 
			themerex_enqueue_style ( 'shortcodes_vc-style', themerex_get_file_url('core/core.shortcodes/shortcodes_vc_front.css'), array(), null );
			// Include JS
			themerex_enqueue_script( 'shortcodes_vc-script', themerex_get_file_url('core/core.shortcodes/shortcodes_vc_front.js'), array(), null, true );
		}
	}
}

// Add init script into shortcodes output in VC frontend editor
if ( !function_exists( 'themerex_shortcodes_vc_add_init_script' ) ) {
	//add_filter('themerex_shortcode_output', 'themerex_shortcodes_vc_add_init_script', 10, 4);
	function themerex_shortcodes_vc_add_init_script($output, $tag='', $atts=array(), $content='') {
		if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') && (isset($_POST['action']) && $_POST['action']=='vc_load_shortcode')
				&& ( isset($_POST['shortcodes'][0]['tag']) && $_POST['shortcodes'][0]['tag']==$tag )
		) {
			if (themerex_strpos($output, 'themerex_vc_init_shortcodes')===false) {
				$id = "themerex_vc_init_shortcodes_".str_replace('.', '', mt_rand());
				$output .= '
					<script id="'.esc_attr($id).'">
						try {
							themerex_init_post_formats();
							themerex_init_shortcodes(jQuery("body").eq(0));
							themerex_scroll_actions();
						} catch (e) { };
					</script>
				';
			}
		}
		return $output;
	}
}


/* Theme setup section
-------------------------------------------------------------------- */

if ( !function_exists( 'themerex_shortcodes_vc_theme_setup' ) ) {
	//if ( themerex_vc_is_frontend() )
	if ( (isset($_GET['vc_editable']) && $_GET['vc_editable']=='true') || (isset($_GET['vc_action']) && $_GET['vc_action']=='vc_inline') )
		add_action( 'themerex_action_before_init_theme', 'themerex_shortcodes_vc_theme_setup', 20 );
	else
		add_action( 'themerex_action_after_init_theme', 'themerex_shortcodes_vc_theme_setup' );
	function themerex_shortcodes_vc_theme_setup() {
	
		// Set dir with theme specific VC shortcodes
		if ( function_exists( 'vc_set_shortcodes_templates_dir' ) ) {
			vc_set_shortcodes_templates_dir( themerex_get_folder_dir('core/core.shortcodes/vc_shortcodes' ) );
		}
		
		// Add/Remove params in the standard VC shortcodes
		vc_add_param("vc_row", array(
					"param_name" => "scheme",
					"heading" => esc_html__("Color scheme", "themerex"),
					"description" => esc_html__("Select color scheme for this block", "themerex"),
					"group" => esc_html__('Color scheme', 'themerex'),
					"class" => "",
					"value" => array_flip(themerex_get_list_color_schemes(true)),
					"type" => "dropdown"
		));

		if (themerex_shortcodes_is_used()) {

			// Set VC as main editor for the theme
			vc_set_as_theme( true );
			
			// Enable VC on follow post types
			vc_set_default_editor_post_types( array('page', 'team') );
			
			// Disable frontend editor
			//vc_disable_frontend();

			// Load scripts and styles for VC support
			add_action( 'wp_enqueue_scripts',		'themerex_shortcodes_vc_scripts_front');
			add_action( 'admin_enqueue_scripts',	'themerex_shortcodes_vc_scripts_admin' );

			// Add init script into shortcodes output in VC frontend editor
			add_filter('themerex_shortcode_output', 'themerex_shortcodes_vc_add_init_script', 10, 4);

			// Remove standard VC shortcodes
			vc_remove_element("vc_button");
			vc_remove_element("vc_posts_slider");
			vc_remove_element("vc_gmaps");
			vc_remove_element("vc_teaser_grid");
			vc_remove_element("vc_progress_bar");
			vc_remove_element("vc_facebook");
			vc_remove_element("vc_tweetmeme");
			vc_remove_element("vc_googleplus");
			vc_remove_element("vc_facebook");
			vc_remove_element("vc_pinterest");
			vc_remove_element("vc_message");
			vc_remove_element("vc_posts_grid");
			vc_remove_element("vc_carousel");
			vc_remove_element("vc_flickr");
			vc_remove_element("vc_tour");
			vc_remove_element("vc_separator");
			vc_remove_element("vc_single_image");
			vc_remove_element("vc_cta_button");
//			vc_remove_element("vc_accordion");
//			vc_remove_element("vc_accordion_tab");
			vc_remove_element("vc_toggle");
			vc_remove_element("vc_tabs");
			vc_remove_element("vc_tab");
			vc_remove_element("vc_images_carousel");
			
			// Remove standard WP widgets
			vc_remove_element("vc_wp_archives");
			vc_remove_element("vc_wp_calendar");
			vc_remove_element("vc_wp_categories");
			vc_remove_element("vc_wp_custommenu");
			vc_remove_element("vc_wp_links");
			vc_remove_element("vc_wp_meta");
			vc_remove_element("vc_wp_pages");
			vc_remove_element("vc_wp_posts");
			vc_remove_element("vc_wp_recentcomments");
			vc_remove_element("vc_wp_rss");
			vc_remove_element("vc_wp_search");
			vc_remove_element("vc_wp_tagcloud");
			vc_remove_element("vc_wp_text");
			
			global $THEMEREX_GLOBALS;
			
			$THEMEREX_GLOBALS['vc_params'] = array(
				
				// Common arrays and strings
				'category' => esc_html__("ThemeREX shortcodes", "themerex"),
			
				// Current element id
				'id' => array(
					"param_name" => "id",
					"heading" => esc_html__("Element ID", "themerex"),
					"description" => esc_html__("ID for current element", "themerex"),
					"group" => esc_html__('ID &amp; Class', 'themerex'),
					"value" => "",
					"type" => "textfield"
				),
			
				// Current element class
				'class' => array(
					"param_name" => "class",
					"heading" => esc_html__("Element CSS class", "themerex"),
					"description" => esc_html__("CSS class for current element", "themerex"),
					"group" => esc_html__('ID &amp; Class', 'themerex'),
					"value" => "",
					"type" => "textfield"
				),

				// Current element animation
				'animation' => array(
					"param_name" => "animation",
					"heading" => esc_html__("Animation", "themerex"),
					"description" => esc_html__("Select animation while object enter in the visible area of page", "themerex"),
					"group" => esc_html__('ID &amp; Class', 'themerex'),
					"class" => "",
					"value" => array_flip($THEMEREX_GLOBALS['sc_params']['animations']),
					"type" => "dropdown"
				),
			
				// Current element style
				'css' => array(
					"param_name" => "css",
					"heading" => esc_html__("CSS styles", "themerex"),
					"description" => esc_html__("Any additional CSS rules (if need)", "themerex"),
					"group" => esc_html__('ID &amp; Class', 'themerex'),
					"class" => "",
					"value" => "",
					"type" => "textfield"
				),
			
				// Margins params
				'margin_top' => array(
					"param_name" => "top",
					"heading" => esc_html__("Top margin", "themerex"),
					"description" => esc_html__("Top margin (in pixels).", "themerex"),
					"group" => esc_html__('Size &amp; Margins', 'themerex'),
					"value" => "",
					"type" => "textfield"
				),
			
				'margin_bottom' => array(
					"param_name" => "bottom",
					"heading" => esc_html__("Bottom margin", "themerex"),
					"description" => esc_html__("Bottom margin (in pixels).", "themerex"),
					"group" => esc_html__('Size &amp; Margins', 'themerex'),
					"value" => "",
					"type" => "textfield"
				),
			
				'margin_left' => array(
					"param_name" => "left",
					"heading" => esc_html__("Left margin", "themerex"),
					"description" => esc_html__("Left margin (in pixels).", "themerex"),
					"group" => esc_html__('Size &amp; Margins', 'themerex'),
					"value" => "",
					"type" => "textfield"
				),
				
				'margin_right' => array(
					"param_name" => "right",
					"heading" => esc_html__("Right margin", "themerex"),
					"description" => esc_html__("Right margin (in pixels).", "themerex"),
					"group" => esc_html__('Size &amp; Margins', 'themerex'),
					"value" => "",
					"type" => "textfield"
				)
			);
	
	
	
			// Accordion
			//-------------------------------------------------------------------------------------
			vc_map( array(
				"base" => "trx_accordion",
				"name" => esc_html__("Accordion", "themerex"),
				"description" => esc_html__("Accordion items", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_accordion',
				"class" => "trx_sc_collection trx_sc_accordion",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_accordion_item'),	// Use only|except attributes to limit child shortcodes (separate multiple values with comma)
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Accordion style", "themerex"),
						"description" => esc_html__("Select style for display accordion", "themerex"),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip(themerex_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "counter",
						"heading" => esc_html__("Counter", "themerex"),
						"description" => esc_html__("Display counter before each accordion title", "themerex"),
						"class" => "",
						"value" => array("Add item numbers before each element" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "initial",
						"heading" => esc_html__("Initially opened item", "themerex"),
						"description" => esc_html__("Number of initially opened item", "themerex"),
						"class" => "",
						"value" => 1,
						"type" => "textfield"
					),
					array(
						"param_name" => "icon_before_title",
						"heading" => esc_html__("Icon before title", "themerex"),
						"description" => esc_html__("Select icon for the title accordion item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
                    array(
                        "param_name" => "icon_closed",
                        "heading" => esc_html__("Icon while closed", "themerex"),
                        "description" => esc_html__("Select icon for the closed accordion item from Fontello icons set", "themerex"),
                        "class" => "",
                        "value" => $THEMEREX_GLOBALS['sc_params']['icons'],
                        "type" => "dropdown"
                    ),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", "themerex"),
						"description" => esc_html__("Select icon for the opened accordion item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_accordion_item title="' . esc_html__( 'Item 1 title', 'themerex' ) . '"][/trx_accordion_item]
					[trx_accordion_item title="' . esc_html__( 'Item 2 title', 'themerex' ) . '"][/trx_accordion_item]
				',
				"custom_markup" => '
					<div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
						%content%
					</div>
					<div class="tab_controls">
						<button class="add_tab" title="'.esc_html__("Add item", "themerex").'">'.esc_html__("Add item", "themerex").'</button>
					</div>
				',
				'js_view' => 'VcTrxAccordionView'
			) );
			
			
			vc_map( array(
				"base" => "trx_accordion_item",
				"name" => esc_html__("Accordion item", "themerex"),
				"description" => esc_html__("Inner accordion item", "themerex"),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_accordion_item',
				"as_child" => array('only' => 'trx_accordion'), 	// Use only|except attributes to limit parent (separate multiple values with comma)
				"as_parent" => array('except' => 'trx_accordion'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for current accordion item", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
                    array(
                        "param_name" => "icon_before_title",
                        "heading" => esc_html__("Icon before title", "themerex"),
                        "description" => esc_html__("Select icon for the title accordion item from Fontello icons set", "themerex"),
                        "class" => "",
                        "value" => $THEMEREX_GLOBALS['sc_params']['icons'],
                        "type" => "dropdown"
                    ),
					array(
						"param_name" => "icon_closed",
						"heading" => esc_html__("Icon while closed", "themerex"),
						"description" => esc_html__("Select icon for the closed accordion item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", "themerex"),
						"description" => esc_html__("Select icon for the opened accordion item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css']
				),
			  'js_view' => 'VcTrxAccordionTabView'
			) );

			class WPBakeryShortCode_Trx_Accordion extends THEMEREX_VC_ShortCodeAccordion {}
			class WPBakeryShortCode_Trx_Accordion_Item extends THEMEREX_VC_ShortCodeAccordionItem {}
			
			
			
			
			
			
			// Anchor
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_anchor",
				"name" => esc_html__("Anchor", "themerex"),
				"description" => esc_html__("Insert anchor for the TOC (table of content)", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_anchor',
				"class" => "trx_sc_single trx_sc_anchor",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Anchor's icon", "themerex"),
						"description" => esc_html__("Select icon for the anchor from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Short title", "themerex"),
						"description" => esc_html__("Short title of the anchor (for the table of content)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Long description", "themerex"),
						"description" => esc_html__("Description for the popup (then hover on the icon). You can use:<br>'{{' and '}}' - to make the text italic,<br>'((' and '))' - to make the text bold,<br>'||' - to insert line break", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "url",
						"heading" => esc_html__("External URL", "themerex"),
						"description" => esc_html__("External URL for this TOC item", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "separator",
						"heading" => esc_html__("Add separator", "themerex"),
						"description" => esc_html__("Add separator under item in the TOC", "themerex"),
						"class" => "",
						"value" => array("Add separator" => "yes" ),
						"type" => "checkbox"
					),
					$THEMEREX_GLOBALS['vc_params']['id']
				),
			) );
			
			class WPBakeryShortCode_Trx_Anchor extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			// Audio
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_audio",
				"name" => esc_html__("Audio", "themerex"),
				"description" => esc_html__("Insert audio player", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_audio',
				"class" => "trx_sc_single trx_sc_audio",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "url",
						"heading" => esc_html__("URL for audio file", "themerex"),
						"description" => esc_html__("Put here URL for audio file", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("Cover image", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for audio cover", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title of the audio file", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "author",
						"heading" => esc_html__("Author", "themerex"),
						"description" => esc_html__("Author of the audio file", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Controls", "themerex"),
						"description" => esc_html__("Show/hide controls", "themerex"),
						"class" => "",
						"value" => array("Hide controls" => "hide" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "autoplay",
						"heading" => esc_html__("Autoplay", "themerex"),
						"description" => esc_html__("Autoplay audio on page load", "themerex"),
						"class" => "",
						"value" => array("Autoplay" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Select block alignment", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Audio extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Block
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_block",
				"name" => esc_html__("Block container", "themerex"),
				"description" => esc_html__("Container for any block ([section] analog - to enable nesting)", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_block',
				"class" => "trx_sc_collection trx_sc_block",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "dedicated",
						"heading" => esc_html__("Dedicated", "themerex"),
						"description" => esc_html__("Use this block as dedicated content - show it before post title on single page", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(esc_html__('Use as dedicated content', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Select block alignment", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns emulation", "themerex"),
						"description" => esc_html__("Select width for columns emulation", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['columns']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "pan",
						"heading" => esc_html__("Use pan effect", "themerex"),
						"description" => esc_html__("Use pan effect to show section content", "themerex"),
						"group" => esc_html__('Scroll', 'themerex'),
						"class" => "",
						"value" => array(esc_html__('Content scroller', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Use scroller", "themerex"),
						"description" => esc_html__("Use scroller to show section content", "themerex"),
						"group" => esc_html__('Scroll', 'themerex'),
						"admin_label" => true,
						"class" => "",
						"value" => array(esc_html__('Content scroller', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll_dir",
						"heading" => esc_html__("Scroll direction", "themerex"),
						"description" => esc_html__("Scroll direction (if Use scroller = yes)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"group" => esc_html__('Scroll', 'themerex'),
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['dir']),
						'dependency' => array(
							'element' => 'scroll',
							'not_empty' => true
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scroll_controls",
						"heading" => esc_html__("Scroll controls", "themerex"),
						"description" => esc_html__("Show scroll controls (if Use scroller = yes)", "themerex"),
						"class" => "",
						"group" => esc_html__('Scroll', 'themerex'),
						'dependency' => array(
							'element' => 'scroll',
							'not_empty' => true
						),
						"value" => array(esc_html__('Show scroll controls', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", "themerex"),
						"description" => esc_html__("Select color scheme for this block", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Fore color", "themerex"),
						"description" => esc_html__("Any color for objects in this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Any background color for this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image URL", "themerex"),
						"description" => esc_html__("Select background image from library for this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
                    array(
                       "param_name" => "bg_tile",
                        "heading" => esc_html__("Tile background image", "themerex"),
                        "description" => esc_html__("Do you want tile background image or image cover whole block?", "themerex"),
                        "group" => esc_html__('Colors and Images', 'themerex'),
                        "class" => "",
                        'dependency' => array(
                         'element' => 'bg_image',
                         'not_empty' => true
                          ),
                        "std" => "no",
                        "value" => array(esc_html__('Tile background image', 'themerex') => 'yes'),
                        "type" => "checkbox"
                        ),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", "themerex"),
						"description" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", "themerex"),
						"description" => esc_html__("Texture style from 1 to 11. Empty or 0 - without texture.", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_padding",
						"heading" => esc_html__("Paddings around content", "themerex"),
						"description" => esc_html__("Add paddings around content in this section (only if bg_color or bg_image enabled).", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						'dependency' => array(
							'element' => array('bg_color','bg_texture','bg_image'),
							'not_empty' => true
						),
						"std" => "yes",
						"value" => array(esc_html__('Disable padding around content in this block', 'themerex') => 'no'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", "themerex"),
						"description" => esc_html__("Font size of the text (default - in pixels, allows any CSS units of measure)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", "themerex"),
						"description" => esc_html__("Font weight of the text", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Default', 'themerex') => 'inherit',
							esc_html__('Thin (100)', 'themerex') => '100',
							esc_html__('Light (300)', 'themerex') => '300',
							esc_html__('Normal (400)', 'themerex') => '400',
							esc_html__('Bold (700)', 'themerex') => '700'
						),
						"type" => "dropdown"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", "themerex"),
						"description" => esc_html__("Content for section container", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Block extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			// Blogger
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_blogger",
				"name" => esc_html__("Blogger", "themerex"),
				"description" => esc_html__("Insert posts (pages) in many styles from desired categories or directly from ids", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_blogger',
				"class" => "trx_sc_single trx_sc_blogger",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Output style", "themerex"),
						"description" => esc_html__("Select desired style for posts output", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['blogger_styles']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "filters",
						"heading" => esc_html__("Show filters", "themerex"),
						"description" => esc_html__("Use post's tags or categories as filter buttons", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['filters']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "hover",
						"heading" => esc_html__("Hover effect", "themerex"),
						"description" => esc_html__("Select hover effect (only if style=Portfolio)", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['hovers']),
						'dependency' => array(
							'element' => 'style',
							'value' => array('portfolio_2','portfolio_3','portfolio_4','grid_2','grid_3','grid_4','square_2','square_3','square_4','short_2','short_3','short_4','colored_2','colored_3','colored_4')
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "hover_dir",
						"heading" => esc_html__("Hover direction", "themerex"),
						"description" => esc_html__("Select hover direction (only if style=Portfolio and hover=Circle|Square)", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['hovers_dir']),
						'dependency' => array(
							'element' => 'style',
							'value' => array('portfolio_2','portfolio_3','portfolio_4','grid_2','grid_3','grid_4','square_2','square_3','square_4','short_2','short_3','short_4','colored_2','colored_3','colored_4')
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "location",
						"heading" => esc_html__("Dedicated content location", "themerex"),
						"description" => esc_html__("Select position for dedicated content (only for style=excerpt)", "themerex"),
						"class" => "",
						'dependency' => array(
							'element' => 'style',
							'value' => array('excerpt')
						),
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['locations']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "dir",
						"heading" => esc_html__("Posts direction", "themerex"),
						"description" => esc_html__("Display posts in horizontal or vertical direction", "themerex"),
						"admin_label" => true,
						"class" => "",
						"std" => "horizontal",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['dir']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns number", "themerex"),
						"description" => esc_html__("How many columns used to display posts?", "themerex"),
						'dependency' => array(
							'element' => 'dir',
							'value' => 'horizontal'
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "rating",
						"heading" => esc_html__("Show rating stars", "themerex"),
						"description" => esc_html__("Show rating stars under post's header", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						"class" => "",
						"value" => array(esc_html__('Show rating', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "info",
						"heading" => esc_html__("Show post info block", "themerex"),
						"description" => esc_html__("Show post info block (author, date, tags, etc.)", "themerex"),
						"class" => "",
						"std" => 'yes',
						"value" => array(esc_html__('Show info', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "descr",
						"heading" => esc_html__("Description length", "themerex"),
						"description" => esc_html__("How many characters are displayed from post excerpt? If 0 - don't show description", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						"class" => "",
						"value" => 0,
						"type" => "textfield"
					),
					array(
						"param_name" => "links",
						"heading" => esc_html__("Allow links to the post", "themerex"),
						"description" => esc_html__("Allow links to the post from each blogger item", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						"class" => "",
						"std" => 'yes',
						"value" => array(esc_html__('Allow links', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "readmore",
						"heading" => esc_html__("More link text", "themerex"),
						"description" => esc_html__("Read more link text. If empty - show 'More', else - used as link text", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for the block", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", "themerex"),
						"description" => esc_html__("Subtitle for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", "themerex"),
						"description" => esc_html__("Description for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", "themerex"),
						"description" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", "themerex"),
						"description" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "post_type",
						"heading" => esc_html__("Post type", "themerex"),
						"description" => esc_html__("Select post type to show", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['posts_types']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "ids",
						"heading" => esc_html__("Post IDs list", "themerex"),
						"description" => esc_html__("Comma separated list of posts ID. If set - parameters above are ignored!", "themerex"),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "cat",
						"heading" => esc_html__("Categories list", "themerex"),
						"description" => esc_html__("Select category. If empty - show posts from any category or from IDs list", "themerex"),
						'dependency' => array(
							'element' => 'ids',
							'is_empty' => true
						),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => array_flip(themerex_array_merge(array(0 => esc_html__('- Select category -', 'themerex')), $THEMEREX_GLOBALS['sc_params']['categories'])),
						"type" => "dropdown"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Total posts to show", "themerex"),
						"description" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", "themerex"),
						'dependency' => array(
							'element' => 'ids',
							'is_empty' => true
						),
						"admin_label" => true,
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => 3,
						"type" => "textfield"
					),
					array(
						"param_name" => "offset",
						"heading" => esc_html__("Offset before select posts", "themerex"),
						"description" => esc_html__("Skip posts before select next part.", "themerex"),
						'dependency' => array(
							'element' => 'ids',
							'is_empty' => true
						),
						"group" => esc_html__('Query', 'themerex'),
						"class" => "",
						"value" => 0,
						"type" => "textfield"
					),
					array(
						"param_name" => "orderby",
						"heading" => esc_html__("Post order by", "themerex"),
						"description" => esc_html__("Select desired posts sorting method", "themerex"),
						"class" => "",
						"group" => esc_html__('Query', 'themerex'),
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['sorting']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "order",
						"heading" => esc_html__("Post order", "themerex"),
						"description" => esc_html__("Select desired posts order", "themerex"),
						"class" => "",
						"group" => esc_html__('Query', 'themerex'),
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "only",
						"heading" => esc_html__("Select posts only", "themerex"),
						"description" => esc_html__("Select posts only with reviews, videos, audios, thumbs or galleries", "themerex"),
						"class" => "",
						"group" => esc_html__('Query', 'themerex'),
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['formats']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Use scroller", "themerex"),
						"description" => esc_html__("Use scroller to show all posts", "themerex"),
						"group" => esc_html__('Scroll', 'themerex'),
						"class" => "",
						"value" => array(esc_html__('Use scroller', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Show slider controls", "themerex"),
						"description" => esc_html__("Show arrows to control scroll slider", "themerex"),
						"group" => esc_html__('Scroll', 'themerex'),
						"class" => "",
						"value" => array(esc_html__('Show controls', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Blogger extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			// Br
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_br",
				"name" => esc_html__("Line break", "themerex"),
				"description" => esc_html__("Line break or Clear Floating", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_br',
				"class" => "trx_sc_single trx_sc_br",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "clear",
						"heading" => esc_html__("Clear floating", "themerex"),
						"description" => esc_html__("Select clear side (if need)", "themerex"),
						"class" => "",
						"value" => "",
						"value" => array(
							esc_html__('None', 'themerex') => 'none',
							esc_html__('Left', 'themerex') => 'left',
							esc_html__('Right', 'themerex') => 'right',
							esc_html__('Both', 'themerex') => 'both'
						),
						"type" => "dropdown"
					)
				)
			) );
			
			class WPBakeryShortCode_Trx_Br extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Button
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_button",
				"name" => esc_html__("Button", "themerex"),
				"description" => esc_html__("Button with link", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_button',
				"class" => "trx_sc_single trx_sc_button",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "content",
						"heading" => esc_html__("Caption", "themerex"),
						"description" => esc_html__("Button caption", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "type",
						"heading" => esc_html__("Button's shape", "themerex"),
						"description" => esc_html__("Select button's shape", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Square', 'themerex') => 'square',
							esc_html__('Round', 'themerex') => 'round'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Button's style", "themerex"),
						"description" => esc_html__("Select button's style", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Filled', 'themerex') => 'filled',
							esc_html__('Border', 'themerex') => 'border'
						),
						"type" => "dropdown"
					),
                    array(
                        "param_name" => "bg_color_style",
                        "heading" => esc_html__("Background style", "themerex"),
                        "description" => esc_html__("Select background color style", "themerex"),
                        "class" => "",
                        "value" => array(
                            esc_html__('Style1', 'themerex') => 'bg_style1',
                            esc_html__('Style2', 'themerex') => 'bg_style2'
                        ),
                        "type" => "dropdown"
                    ),
					array(
						"param_name" => "size",
						"heading" => esc_html__("Button's size", "themerex"),
						"description" => esc_html__("Select button's size", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							esc_html__('Small', 'themerex') => 'small',
							esc_html__('Medium', 'themerex') => 'medium',
							esc_html__('Large', 'themerex') => 'large'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Button's icon", "themerex"),
						"description" => esc_html__("Select icon for the title from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Button's text color", "themerex"),
						"description" => esc_html__("Any color for button's caption", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Button's backcolor", "themerex"),
						"description" => esc_html__("Any color for button's background", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Button's alignment", "themerex"),
						"description" => esc_html__("Align button to left, center or right", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", "themerex"),
						"description" => esc_html__("URL for the link on button click", "themerex"),
						"class" => "",
						"group" => esc_html__('Link', 'themerex'),
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "target",
						"heading" => esc_html__("Link target", "themerex"),
						"description" => esc_html__("Target for the link on button click", "themerex"),
						"class" => "",
						"group" => esc_html__('Link', 'themerex'),
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "popup",
						"heading" => esc_html__("Open link in popup", "themerex"),
						"description" => esc_html__("Open link target in popup window", "themerex"),
						"class" => "",
						"group" => esc_html__('Link', 'themerex'),
						"value" => array(esc_html__('Open in popup', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "rel",
						"heading" => esc_html__("Rel attribute", "themerex"),
						"description" => esc_html__("Rel attribute for the button's link (if need", "themerex"),
						"class" => "",
						"group" => esc_html__('Link', 'themerex'),
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Button extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Call to Action block
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_call_to_action",
				"name" => esc_html__("Call to Action", "themerex"),
				"description" => esc_html__("Insert call to action block in your page (post)", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_call_to_action',
				"class" => "trx_sc_collection trx_sc_call_to_action",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Block's style", "themerex"),
						"description" => esc_html__("Select style to display this block", "themerex"),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip(themerex_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Select block alignment", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "accent",
						"heading" => esc_html__("Accent", "themerex"),
						"description" => esc_html__("Fill entire block with Accent1 color from current color scheme", "themerex"),
						"class" => "",
						"value" => array("Fill with Accent1 color" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom", "themerex"),
						"description" => esc_html__("Allow get featured image or video from inner shortcodes (custom) or get it from shortcode parameters below", "themerex"),
						"class" => "",
						"value" => array("Custom content" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("Image", "themerex"),
						"description" => esc_html__("Image to display inside block", "themerex"),
				        'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "video",
						"heading" => esc_html__("URL for video file", "themerex"),
						"description" => esc_html__("Paste URL for video file to display inside block", "themerex"),
				        'dependency' => array(
							'element' => 'custom',
							'is_empty' => true
						),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for the block", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", "themerex"),
						"description" => esc_html__("Subtitle for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", "themerex"),
						"description" => esc_html__("Description for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", "themerex"),
						"description" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", "themerex"),
						"description" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link2",
						"heading" => esc_html__("Button 2 URL", "themerex"),
						"description" => esc_html__("Link URL for the second button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link2_caption",
						"heading" => esc_html__("Button 2 caption", "themerex"),
						"description" => esc_html__("Caption for the second button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Call_To_Action extends THEMEREX_VC_ShortCodeCollection {}


			
			
			
			
			// Chat
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_chat",
				"name" => esc_html__("Chat", "themerex"),
				"description" => esc_html__("Chat message", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_chat',
				"class" => "trx_sc_container trx_sc_chat",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Item title", "themerex"),
						"description" => esc_html__("Title for current chat item", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "photo",
						"heading" => esc_html__("Item photo", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for the item photo (avatar)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", "themerex"),
						"description" => esc_html__("URL for the link on chat title click", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Chat item content", "themerex"),
						"description" => esc_html__("Current chat item content", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextContainerView'
			
			) );
			
			class WPBakeryShortCode_Trx_Chat extends THEMEREX_VC_ShortCodeContainer {}
			
			
			
			
			
			
			// Columns
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_columns",
				"name" => esc_html__("Columns", "themerex"),
				"description" => esc_html__("Insert columns with margins", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_columns',
				"class" => "trx_sc_columns",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_column_item'),
				"params" => array(
					array(
						"param_name" => "count",
						"heading" => esc_html__("Columns count", "themerex"),
						"description" => esc_html__("Number of the columns in the container.", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "2",
						"type" => "textfield"
					),
					array(
						"param_name" => "fluid",
						"heading" => esc_html__("Fluid columns", "themerex"),
						"description" => esc_html__("To squeeze the columns when reducing the size of the window (fluid=yes) or to rebuild them (fluid=no)", "themerex"),
						"class" => "",
						"value" => array(esc_html__('Fluid columns', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
                    array(
                        "param_name" => "margins",
                        "heading" => esc_html__("Margins between columns", "themerex"),
                        "description" => esc_html__("Add margins between columns", "themerex"),
                        "class" => "",
                        "std" => "yes",
                        "value" => array(esc_html__('Disable margins between columns', 'themerex') => 'no'),
                        "type" => "checkbox"
                         ),
                    array(
                        "param_name" => "dark",
                        "heading" => esc_html__("Dark section", "themerex"),
                        "description" => esc_html__("Color of font is white", "themerex"),
                        "class" => "",
                        "std" => "no",
                        "value" => array(esc_html__('Color of font is white', 'themerex') => 'yes'),
                        "type" => "checkbox"
                    ),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_column_item][/trx_column_item]
					[trx_column_item][/trx_column_item]
				',
				'js_view' => 'VcTrxColumnsView'
			) );
			
			
			vc_map( array(
				"base" => "trx_column_item",
				"name" => esc_html__("Column", "themerex"),
				"description" => esc_html__("Column item", "themerex"),
				"show_settings_on_create" => true,
				"class" => "trx_sc_collection trx_sc_column_item",
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_column_item',
				"as_child" => array('only' => 'trx_columns'),
				"as_parent" => array('except' => 'trx_columns'),
				"params" => array(
					array(
						"param_name" => "span",
						"heading" => esc_html__("Merge columns", "themerex"),
						"description" => esc_html__("Count merged columns from current", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Alignment text in the column", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Fore color", "themerex"),
						"description" => esc_html__("Any color for objects in this column", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Any background color for this column", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("URL for background image file", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for the background", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
                    array(
                        "param_name" => "bg_tile",
                        "heading" => esc_html__("Tile background image", "themerex"),
                        "description" => esc_html__("Do you want tile background image or image cover whole column?", "themerex"),
                        "class" => "",
                        'dependency' => array(
                            'element' => 'bg_image',
                            'not_empty' => true
                                ),
                        "std" => "no",
                        "value" => array(esc_html__('Tile background image', 'themerex') => 'yes'),
                        "type" => "checkbox"
                           ),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Column's content", "themerex"),
						"description" => esc_html__("Content of the current column", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxColumnItemView'
			) );
			
			class WPBakeryShortCode_Trx_Columns extends THEMEREX_VC_ShortCodeColumns {}
			class WPBakeryShortCode_Trx_Column_Item extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Contact form
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_form",
				"name" => esc_html__("Form", "themerex"),
				"description" => esc_html__("Insert form with specefied style of with set of custom fields", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_form',
				"class" => "trx_sc_collection trx_sc_form",
				"content_element" => true,
				"is_container" => true,
				"as_parent" => array('except' => 'trx_form'),
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", "themerex"),
						"description" => esc_html__("Select style of the form (if 'style' is not equal 'custom' - all tabs 'Field NN' are ignored!", "themerex"),
						"admin_label" => true,
						"class" => "",
						"std" => "custom",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['forms']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", "themerex"),
						"description" => esc_html__("Select color scheme for this block", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "action",
						"heading" => esc_html__("Action", "themerex"),
						"description" => esc_html__("Contact form action (URL to handle form data). If empty - use internal action", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Select form alignment", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for the block", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", "themerex"),
						"description" => esc_html__("Subtitle for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", "themerex"),
						"description" => esc_html__("Description for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			
			vc_map( array(
				"base" => "trx_form_item",
				"name" => esc_html__("Form item (custom field)", "themerex"),
				"description" => esc_html__("Custom field for the contact form", "themerex"),
				"class" => "trx_sc_item trx_sc_form_item",
				'icon' => 'icon_trx_form_item',
				//"allowed_container_element" => 'vc_row',
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				"as_child" => array('only' => 'trx_form,trx_column_item'), // Use only|except attributes to limit parent (separate multiple values with comma)
				"params" => array(
					array(
						"param_name" => "type",
						"heading" => esc_html__("Type", "themerex"),
						"description" => esc_html__("Select type of the custom field", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['field_types']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "name",
						"heading" => esc_html__("Name", "themerex"),
						"description" => esc_html__("Name of the custom field", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "value",
						"heading" => esc_html__("Default value", "themerex"),
						"description" => esc_html__("Default value of the custom field", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "options",
						"heading" => esc_html__("Options", "themerex"),
						"description" => esc_html__("Field options. For example: big=My daddy|middle=My brother|small=My little sister", "themerex"),
						'dependency' => array(
							'element' => 'type',
							'value' => array('radio','checkbox','select')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "label",
						"heading" => esc_html__("Label", "themerex"),
						"description" => esc_html__("Label for the custom field", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "label_position",
						"heading" => esc_html__("Label position", "themerex"),
						"description" => esc_html__("Label position relative to the field", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['label_positions']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Form extends THEMEREX_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Form_Item extends THEMEREX_VC_ShortCodeItem {}
			
			
			
			
			
			
			
			// Content block on fullscreen page
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_content",
				"name" => esc_html__("Content block", "themerex"),
				"description" => esc_html__("Container for main content block (use it only on fullscreen pages)", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_content',
				"class" => "trx_sc_collection trx_sc_content",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", "themerex"),
						"description" => esc_html__("Select color scheme for this block", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", "themerex"),
						"description" => esc_html__("Content for section container", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom']
				)
			) );
			
			class WPBakeryShortCode_Trx_Content extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Countdown
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_countdown",
				"name" => esc_html__("Countdown", "themerex"),
				"description" => esc_html__("Insert countdown object", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_countdown',
				"class" => "trx_sc_single trx_sc_countdown",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "date",
						"heading" => esc_html__("Date", "themerex"),
						"description" => esc_html__("Upcoming date (format: yyyy-mm-dd)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "time",
						"heading" => esc_html__("Time", "themerex"),
						"description" => esc_html__("Upcoming time (format: HH:mm:ss)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", "themerex"),
						"description" => esc_html__("Countdown style", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(themerex_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Align counter to left, center or right", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Countdown extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Dropcaps
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_dropcaps",
				"name" => esc_html__("Dropcaps", "themerex"),
				"description" => esc_html__("Make first letter of the text as dropcaps", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_dropcaps',
				"class" => "trx_sc_single trx_sc_dropcaps",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", "themerex"),
						"description" => esc_html__("Dropcaps style", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(themerex_get_list_styles(1, 4)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Paragraph text", "themerex"),
						"description" => esc_html__("Paragraph with dropcaps content", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTextView'
			
			) );
			
			class WPBakeryShortCode_Trx_Dropcaps extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Emailer
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_emailer",
				"name" => esc_html__("E-mail collector", "themerex"),
				"description" => esc_html__("Collect e-mails into specified group", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_emailer',
				"class" => "trx_sc_single trx_sc_emailer",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "group",
						"heading" => esc_html__("Group", "themerex"),
						"description" => esc_html__("The name of group to collect e-mail address", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "open",
						"heading" => esc_html__("Opened", "themerex"),
						"description" => esc_html__("Initially open the input field on show object", "themerex"),
						"class" => "",
						"value" => array(esc_html__('Initially opened', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Align field to left, center or right", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Emailer extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Gap
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_gap",
				"name" => esc_html__("Gap", "themerex"),
				"description" => esc_html__("Insert gap (fullwidth area) in the post content", "themerex"),
				"category" => esc_html__('Structure', 'js_composer'),
				'icon' => 'icon_trx_gap',
				"class" => "trx_sc_collection trx_sc_gap",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"params" => array(
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Gap content", "themerex"),
						"description" => esc_html__("Gap inner content", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					)
					*/
				)
			) );
			
			class WPBakeryShortCode_Trx_Gap extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Googlemap
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_googlemap",
				"name" => esc_html__("Google map", "themerex"),
				"description" => esc_html__("Insert Google map with desired address or coordinates", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_googlemap',
				"class" => "trx_sc_collection trx_sc_googlemap",
				"content_element" => true,
				"is_container" => true,
				"as_parent" => array('only' => 'trx_googlemap_marker'),
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "zoom",
						"heading" => esc_html__("Zoom", "themerex"),
						"description" => esc_html__("Map zoom factor", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "16",
						"type" => "textfield"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", "themerex"),
						"description" => esc_html__("Map custom style", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['googlemap_styles']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width('100%'),
					themerex_vc_height(240),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			vc_map( array(
				"base" => "trx_googlemap_marker",
				"name" => esc_html__("Googlemap marker", "themerex"),
				"description" => esc_html__("Insert new marker into Google map", "themerex"),
				"class" => "trx_sc_collection trx_sc_googlemap_marker",
				'icon' => 'icon_trx_googlemap_marker',
				//"allowed_container_element" => 'vc_row',
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => true,
				"as_child" => array('only' => 'trx_googlemap'), // Use only|except attributes to limit parent (separate multiple values with comma)
				"params" => array(
					array(
						"param_name" => "address",
						"heading" => esc_html__("Address", "themerex"),
						"description" => esc_html__("Address of this marker", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "latlng",
						"heading" => esc_html__("Latitude and Longtitude", "themerex"),
						"description" => esc_html__("Comma separated marker's coorditanes (instead Address)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for this marker", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "point",
						"heading" => esc_html__("URL for marker image file", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for this marker. If empty - use default marker", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					$THEMEREX_GLOBALS['vc_params']['id']
				)
			) );
			
			class WPBakeryShortCode_Trx_Googlemap extends THEMEREX_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Googlemap_Marker extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			
			
			// Highlight
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_highlight",
				"name" => esc_html__("Highlight text", "themerex"),
				"description" => esc_html__("Highlight text with selected color, background color and other styles", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_highlight',
				"class" => "trx_sc_single trx_sc_highlight",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "type",
						"heading" => esc_html__("Type", "themerex"),
						"description" => esc_html__("Highlight type", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								esc_html__('Custom', 'themerex') => 0,
								esc_html__('Type 1', 'themerex') => 1,
								esc_html__('Type 2', 'themerex') => 2

							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", "themerex"),
						"description" => esc_html__("Color for the highlighted text", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Background color for the highlighted text", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", "themerex"),
						"description" => esc_html__("Font size for the highlighted text (default - in pixels, allows any CSS units of measure)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Highlight text", "themerex"),
						"description" => esc_html__("Content for highlight", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Highlight extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			// Icon
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_icon",
				"name" => esc_html__("Icon", "themerex"),
				"description" => esc_html__("Insert the icon", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_icon',
				"class" => "trx_sc_single trx_sc_icon",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Icon", "themerex"),
						"description" => esc_html__("Select icon class from Fontello icons set", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", "themerex"),
						"description" => esc_html__("Icon's color", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Background color for the icon", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_shape",
						"heading" => esc_html__("Background shape", "themerex"),
						"description" => esc_html__("Shape of the icon background", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							esc_html__('None', 'themerex') => 'none',
							esc_html__('Round', 'themerex') => 'round',
							esc_html__('Square', 'themerex') => 'square'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", "themerex"),
						"description" => esc_html__("Icon's font size", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", "themerex"),
						"description" => esc_html__("Icon's font weight", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Default', 'themerex') => 'inherit',
							esc_html__('Thin (100)', 'themerex') => '100',
							esc_html__('Light (300)', 'themerex') => '300',
							esc_html__('Normal (400)', 'themerex') => '400',
							esc_html__('Bold (700)', 'themerex') => '700'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Icon's alignment", "themerex"),
						"description" => esc_html__("Align icon to left, center or right", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", "themerex"),
						"description" => esc_html__("Link URL from this icon (if not empty)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Icon extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Image
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_image",
				"name" => esc_html__("Image", "themerex"),
				"description" => esc_html__("Insert image", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_image',
				"class" => "trx_sc_single trx_sc_image",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "url",
						"heading" => esc_html__("Select image", "themerex"),
						"description" => esc_html__("Select image from library", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Image alignment", "themerex"),
						"description" => esc_html__("Align image to left or right side", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "shape",
						"heading" => esc_html__("Image shape", "themerex"),
						"description" => esc_html__("Shape of the image: square or round", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							esc_html__('Square', 'themerex') => 'square',
							esc_html__('Round', 'themerex') => 'round'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Image's title", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Title's icon", "themerex"),
						"description" => esc_html__("Select icon for the title from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link", "themerex"),
						"description" => esc_html__("The link URL from the image", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Image extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Infobox
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_infobox",
				"name" => esc_html__("Infobox", "themerex"),
				"description" => esc_html__("Box with info or error message", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_infobox',
				"class" => "trx_sc_container trx_sc_infobox",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", "themerex"),
						"description" => esc_html__("Infobox style", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								esc_html__('Regular', 'themerex') => 'regular',
								esc_html__('Info', 'themerex') => 'info',
								esc_html__('Success', 'themerex') => 'success',
								esc_html__('Error', 'themerex') => 'error',
								esc_html__('Result', 'themerex') => 'result'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "closeable",
						"heading" => esc_html__("Closeable", "themerex"),
						"description" => esc_html__("Create closeable box (with close button)", "themerex"),
						"class" => "",
						"value" => array(esc_html__('Close button', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Custom icon", "themerex"),
						"description" => esc_html__("Select icon for the infobox from Fontello icons set. If empty - use default icon", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", "themerex"),
						"description" => esc_html__("Any color for the text and headers", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Any background color for this infobox", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Message text", "themerex"),
						"description" => esc_html__("Message for the infobox", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextContainerView'
			) );
			
			class WPBakeryShortCode_Trx_Infobox extends THEMEREX_VC_ShortCodeContainer {}
			
			
			
			
			
			
			
			// Line
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_line",
				"name" => esc_html__("Line", "themerex"),
				"description" => esc_html__("Insert line (delimiter)", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				"class" => "trx_sc_single trx_sc_line",
				'icon' => 'icon_trx_line',
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", "themerex"),
						"description" => esc_html__("Line style", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								esc_html__('Solid', 'themerex') => 'solid',
								esc_html__('Dashed', 'themerex') => 'dashed',
								esc_html__('Dotted', 'themerex') => 'dotted',
								esc_html__('Double', 'themerex') => 'double',
								esc_html__('Shadow', 'themerex') => 'shadow'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Line color", "themerex"),
						"description" => esc_html__("Line color", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Line extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// List
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_list",
				"name" => esc_html__("List", "themerex"),
				"description" => esc_html__("List items with specific bullets", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				"class" => "trx_sc_collection trx_sc_list",
				'icon' => 'icon_trx_list',
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_list_item'),
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Bullet's style", "themerex"),
						"description" => esc_html__("Bullet's style for each list item", "themerex"),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['list_styles']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", "themerex"),
						"description" => esc_html__("List items color", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("List icon", "themerex"),
						"description" => esc_html__("Select list icon from Fontello icons set (only for style=Iconed)", "themerex"),
						"admin_label" => true,
						"class" => "",
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_color",
						"heading" => esc_html__("Icon color", "themerex"),
						"description" => esc_html__("List icons color", "themerex"),
						"class" => "",
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => "",
						"type" => "colorpicker"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_list_item]' . esc_html__( 'Item 1', 'themerex' ) . '[/trx_list_item]
					[trx_list_item]' . esc_html__( 'Item 2', 'themerex' ) . '[/trx_list_item]
				'
			) );
			
			
			vc_map( array(
				"base" => "trx_list_item",
				"name" => esc_html__("List item", "themerex"),
				"description" => esc_html__("List item with specific bullet", "themerex"),
				"class" => "trx_sc_single trx_sc_list_item",
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				'icon' => 'icon_trx_list_item',
				"as_child" => array('only' => 'trx_list'), // Use only|except attributes to limit parent (separate multiple values with comma)
				"as_parent" => array('except' => 'trx_list'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("List item title", "themerex"),
						"description" => esc_html__("Title for the current list item (show it as tooltip)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", "themerex"),
						"description" => esc_html__("Link URL for the current list item", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Link', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "target",
						"heading" => esc_html__("Link target", "themerex"),
						"description" => esc_html__("Link target for the current list item", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Link', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", "themerex"),
						"description" => esc_html__("Text color for this item", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("List item icon", "themerex"),
						"description" => esc_html__("Select list item icon from Fontello icons set (only for style=Iconed)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_color",
						"heading" => esc_html__("Icon color", "themerex"),
						"description" => esc_html__("Icon color for this item", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("List item text", "themerex"),
						"description" => esc_html__("Current list item content", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTextView'
			
			) );
			
			class WPBakeryShortCode_Trx_List extends THEMEREX_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_List_Item extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			
			
			// Number
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_number",
				"name" => esc_html__("Number", "themerex"),
				"description" => esc_html__("Insert number or any word as set of separated characters", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				"class" => "trx_sc_single trx_sc_number",
				'icon' => 'icon_trx_number',
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "value",
						"heading" => esc_html__("Value", "themerex"),
						"description" => esc_html__("Number or any word to separate", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Select block alignment", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Number extends THEMEREX_VC_ShortCodeSingle {}


			
			
			
			
			
			// Parallax
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_parallax",
				"name" => esc_html__("Parallax", "themerex"),
				"description" => esc_html__("Create the parallax container (with asinc background image)", "themerex"),
				"category" => esc_html__('Structure', 'js_composer'),
				'icon' => 'icon_trx_parallax',
				"class" => "trx_sc_collection trx_sc_parallax",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "gap",
						"heading" => esc_html__("Create gap", "themerex"),
						"description" => esc_html__("Create gap around parallax container (not need in fullscreen pages)", "themerex"),
						"class" => "",
						"value" => array(esc_html__('Create gap', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "dir",
						"heading" => esc_html__("Direction", "themerex"),
						"description" => esc_html__("Scroll direction for the parallax background", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
								esc_html__('Up', 'themerex') => 'up',
								esc_html__('Down', 'themerex') => 'down'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "speed",
						"heading" => esc_html__("Speed", "themerex"),
						"description" => esc_html__("Parallax background motion speed (from 0.0 to 1.0)", "themerex"),
						"class" => "",
						"value" => "0.3",
						"type" => "textfield"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", "themerex"),
						"description" => esc_html__("Select color scheme for this block", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Text color", "themerex"),
						"description" => esc_html__("Select color for text object inside parallax block", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Backgroud color", "themerex"),
						"description" => esc_html__("Select color for parallax background", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for the parallax background", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_image_x",
						"heading" => esc_html__("Image X position", "themerex"),
						"description" => esc_html__("Parallax background X position (in percents)", "themerex"),
						"class" => "",
						"value" => "50%",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_video",
						"heading" => esc_html__("Video background", "themerex"),
						"description" => esc_html__("Paste URL for video file to show it as parallax background", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),

					array(
						"param_name" => "bg_video_ratio",
						"heading" => esc_html__("Video ratio", "themerex"),
						"description" => esc_html__("Specify ratio of the video background. For example: 16:9 (default), 4:3, etc.", "themerex"),
						"class" => "",
						"value" => "16:9",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", "themerex"),
						"description" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
                    array(
                        "param_name" => "reverse",
                        "heading" => esc_html__("Change the color", "themerex"),
                        "description" => esc_html__("Change the color of text ", "themerex"),
                        "class" => "",
                        "value" => array(esc_html__('Change the color', 'themerex') => 'yes'),
                        "type" => "checkbox"
                    ),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", "themerex"),
						"description" => esc_html__("Texture style from 1 to 11. Empty or 0 - without texture.", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Content", "themerex"),
						"description" => esc_html__("Content for the parallax container", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Parallax extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			// Popup
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_popup",
				"name" => esc_html__("Popup window", "themerex"),
				"description" => esc_html__("Container for any html-block with desired class and style for popup window", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_popup',
				"class" => "trx_sc_collection trx_sc_popup",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", "themerex"),
						"description" => esc_html__("Content for popup container", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Popup extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Price
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_price",
				"name" => esc_html__("Price", "themerex"),
				"description" => esc_html__("Insert price with decoration", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_price',
				"class" => "trx_sc_single trx_sc_price",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "money",
						"heading" => esc_html__("Money", "themerex"),
						"description" => esc_html__("Money value (dot or comma separated)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "currency",
						"heading" => esc_html__("Currency symbol", "themerex"),
						"description" => esc_html__("Currency character", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "$",
						"type" => "textfield"
					),
					array(
						"param_name" => "period",
						"heading" => esc_html__("Period", "themerex"),
						"description" => esc_html__("Period text (if need). For example: monthly, daily, etc.", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Align price to left or right side", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Price extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Price block
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_price_block",
				"name" => esc_html__("Price block", "themerex"),
				"description" => esc_html__("Insert price block with title, price and description", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_price_block',
				"class" => "trx_sc_single trx_sc_price_block",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Block title", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Link URL", "themerex"),
						"description" => esc_html__("URL for link from button (at bottom of the block)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_text",
						"heading" => esc_html__("Link text", "themerex"),
						"description" => esc_html__("Text (caption) for the link button (at bottom of the block). If empty - button not showed", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Icon", "themerex"),
						"description" => esc_html__("Select icon from Fontello icons set (placed before/instead price)", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "money",
						"heading" => esc_html__("Money", "themerex"),
						"description" => esc_html__("Money value (dot or comma separated)", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Money', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "currency",
						"heading" => esc_html__("Currency symbol", "themerex"),
						"description" => esc_html__("Currency character", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Money', 'themerex'),
						"class" => "",
						"value" => "$",
						"type" => "textfield"
					),
					array(
						"param_name" => "period",
						"heading" => esc_html__("Period", "themerex"),
						"description" => esc_html__("Period text (if need). For example: monthly, daily, etc.", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Money', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", "themerex"),
						"description" => esc_html__("Select color scheme for this block", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Align price to left or right side", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Description", "themerex"),
						"description" => esc_html__("Description for this price block", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_PriceBlock extends THEMEREX_VC_ShortCodeSingle {}

			
			
			
			
			// Quote
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_quote",
				"name" => esc_html__("Quote", "themerex"),
				"description" => esc_html__("Quote text", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_quote',
				"class" => "trx_sc_single trx_sc_quote",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
                    array(
                        "param_name" => "style",
                        "heading" => esc_html__("Style", "themerex"),
                        "description" => esc_html__("Quote style", "themerex"),
                        "admin_label" => true,
                        "class" => "",
                        "value" => array_flip(themerex_get_list_styles(1, 2)),
                        "type" => "dropdown"
                    ),
					array(
						"param_name" => "cite",
						"heading" => esc_html__("Quote cite", "themerex"),
						"description" => esc_html__("URL for the quote cite link", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title (author)", "themerex"),
						"description" => esc_html__("Quote title (author name)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Quote content", "themerex"),
						"description" => esc_html__("Quote content", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Quote extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Reviews
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_reviews",
				"name" => esc_html__("Reviews", "themerex"),
				"description" => esc_html__("Insert reviews block in the single post", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_reviews',
				"class" => "trx_sc_single trx_sc_reviews",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Align counter to left, center or right", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Reviews extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Search
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_search",
				"name" => esc_html__("Search form", "themerex"),
				"description" => esc_html__("Insert search form", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_search',
				"class" => "trx_sc_single trx_sc_search",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Style", "themerex"),
						"description" => esc_html__("Select style to display search field", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Regular', 'themerex') => "regular",
							esc_html__('Flat', 'themerex') => "flat"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "state",
						"heading" => esc_html__("State", "themerex"),
						"description" => esc_html__("Select search field initial state", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Fixed', 'themerex')  => "fixed",
							esc_html__('Opened', 'themerex') => "opened",
							esc_html__('Closed', 'themerex') => "closed"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title (placeholder) for the search field", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => esc_html__("Search &hellip;", 'themerex'),
						"type" => "textfield"
					),
					array(
						"param_name" => "ajax",
						"heading" => esc_html__("AJAX", "themerex"),
						"description" => esc_html__("Search via AJAX or reload page", "themerex"),
						"class" => "",
						"value" => array(esc_html__('Use AJAX search', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Search extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Section
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_section",
				"name" => esc_html__("Section container", "themerex"),
				"description" => esc_html__("Container for any block ([block] analog - to enable nesting)", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				"class" => "trx_sc_collection trx_sc_section",
				'icon' => 'icon_trx_block',
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "dedicated",
						"heading" => esc_html__("Dedicated", "themerex"),
						"description" => esc_html__("Use this block as dedicated content - show it before post title on single page", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(esc_html__('Use as dedicated content', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
                    array(
                        "param_name" => "reverse",
                        "heading" => esc_html__("Change the color", "themerex"),
                        "description" => esc_html__("Change the color of text ", "themerex"),
                        "class" => "",
                        "value" => array(esc_html__('Change the color', 'themerex') => 'yes'),
                        "type" => "checkbox"
                    ),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Select block alignment", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns emulation", "themerex"),
						"description" => esc_html__("Select width for columns emulation", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['columns']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "pan",
						"heading" => esc_html__("Use pan effect", "themerex"),
						"description" => esc_html__("Use pan effect to show section content", "themerex"),
						"group" => esc_html__('Scroll', 'themerex'),
						"class" => "",
						"value" => array(esc_html__('Content scroller', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Use scroller", "themerex"),
						"description" => esc_html__("Use scroller to show section content", "themerex"),
						"group" => esc_html__('Scroll', 'themerex'),
						"admin_label" => true,
						"class" => "",
						"value" => array(esc_html__('Content scroller', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scroll_dir",
						"heading" => esc_html__("Scroll and Pan direction", "themerex"),
						"description" => esc_html__("Scroll and Pan direction (if Use scroller = yes or Pan = yes)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"group" => esc_html__('Scroll', 'themerex'),
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['dir']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "scroll_controls",
						"heading" => esc_html__("Scroll controls", "themerex"),
						"description" => esc_html__("Show scroll controls (if Use scroller = yes)", "themerex"),
						"class" => "",
						"group" => esc_html__('Scroll', 'themerex'),
						'dependency' => array(
							'element' => 'scroll',
							'not_empty' => true
						),
						"value" => array(esc_html__('Show scroll controls', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", "themerex"),
						"description" => esc_html__("Select color scheme for this block", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Fore color", "themerex"),
						"description" => esc_html__("Any color for objects in this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Any background color for this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image URL", "themerex"),
						"description" => esc_html__("Select background image from library for this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
                    array(
                        "param_name" => "bg_tile",
                        "heading" => esc_html__("Tile background image", "themerex"),
                        "description" => esc_html__("Do you want tile background image or image cover whole block?", "themerex"),
                        "group" => esc_html__('Colors and Images', 'themerex'),
                        "class" => "",
                        'dependency' => array(
                         'element' => 'bg_image',
                          'not_empty' => true
                          ),
                        "std" => "no",
                        "value" => array(esc_html__('Tile background image', 'themerex') => 'yes'),
                        "type" => "checkbox"
                        ),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", "themerex"),
						"description" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", "themerex"),
						"description" => esc_html__("Texture style from 1 to 11. Empty or 0 - without texture.", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_padding",
						"heading" => esc_html__("Paddings around content", "themerex"),
						"description" => esc_html__("Add paddings around content in this section (only if bg_color or bg_image enabled).", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						'dependency' => array(
							'element' => array('bg_color','bg_texture','bg_image'),
							'not_empty' => true
						),
						"std" => "yes",
						"value" => array(esc_html__('Disable padding around content in this block', 'themerex') => 'no'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", "themerex"),
						"description" => esc_html__("Font size of the text (default - in pixels, allows any CSS units of measure)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", "themerex"),
						"description" => esc_html__("Font weight of the text", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Default', 'themerex') => 'inherit',
							esc_html__('Thin (100)', 'themerex') => '100',
							esc_html__('Light (300)', 'themerex') => '300',
							esc_html__('Normal (400)', 'themerex') => '400',
							esc_html__('Bold (700)', 'themerex') => '700'
						),
						"type" => "dropdown"
					),
					/*
					array(
						"param_name" => "content",
						"heading" => esc_html__("Container content", "themerex"),
						"description" => esc_html__("Content for section container", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					*/
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Section extends THEMEREX_VC_ShortCodeCollection {}
			
			
			
			
			
			
			
			// Skills
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_skills",
				"name" => esc_html__("Skills", "themerex"),
				"description" => esc_html__("Insert skills diagramm", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_skills',
				"class" => "trx_sc_collection trx_sc_skills",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"as_parent" => array('only' => 'trx_skills_item'),
				"params" => array(
					array(
						"param_name" => "max_value",
						"heading" => esc_html__("Max value", "themerex"),
						"description" => esc_html__("Max value for skills items", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "100",
						"type" => "textfield"
					),
					array(
						"param_name" => "type",
						"heading" => esc_html__("Skills type", "themerex"),
						"description" => esc_html__("Select type of skills block", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							esc_html__('Bar', 'themerex') => 'bar',
							esc_html__('Pie chart', 'themerex') => 'pie',
							esc_html__('Counter', 'themerex') => 'counter',
							esc_html__('Arc', 'themerex') => 'arc'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "layout",
						"heading" => esc_html__("Skills layout", "themerex"),
						"description" => esc_html__("Select layout of skills block", "themerex"),
						"admin_label" => true,
						'dependency' => array(
							'element' => 'type',
							'value' => array('counter','bar','pie')
						),
						"class" => "",
						"value" => array(
							esc_html__('Rows', 'themerex') => 'rows',
							esc_html__('Columns', 'themerex') => 'columns'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "dir",
						"heading" => esc_html__("Direction", "themerex"),
						"description" => esc_html__("Select direction of skills block", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['dir']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Counters style", "themerex"),
						"description" => esc_html__("Select style of skills items (only for type=counter)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(themerex_get_list_styles(1, 4)),
						'dependency' => array(
							'element' => 'type',
							'value' => array('counter')
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "columns",
						"heading" => esc_html__("Columns count", "themerex"),
						"description" => esc_html__("Skills columns count (required)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", "themerex"),
						"description" => esc_html__("Color for all skills items", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Background color for all skills items (only for type=pie)", "themerex"),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "border_color",
						"heading" => esc_html__("Border color", "themerex"),
						"description" => esc_html__("Border color for all skills items (only for type=pie)", "themerex"),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Align skills block to left or right side", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "arc_caption",
						"heading" => esc_html__("Arc caption", "themerex"),
						"description" => esc_html__("Arc caption - text in the center of the diagram", "themerex"),
						'dependency' => array(
							'element' => 'type',
							'value' => array('arc')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "pie_compact",
						"heading" => esc_html__("Pie compact", "themerex"),
						"description" => esc_html__("Show all skills in one diagram or as separate diagrams", "themerex"),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => array(esc_html__('Show all skills in one diagram', 'themerex') => 'on'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "pie_cutout",
						"heading" => esc_html__("Pie cutout", "themerex"),
						"description" => esc_html__("Pie cutout (0-99). 0 - without cutout, 99 - max cutout", "themerex"),
						'dependency' => array(
							'element' => 'type',
							'value' => array('pie')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for the block", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "subtitle",
						"heading" => esc_html__("Subtitle", "themerex"),
						"description" => esc_html__("Subtitle for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "description",
						"heading" => esc_html__("Description", "themerex"),
						"description" => esc_html__("Description for the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textarea"
					),
					array(
						"param_name" => "link",
						"heading" => esc_html__("Button URL", "themerex"),
						"description" => esc_html__("Link URL for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "link_caption",
						"heading" => esc_html__("Button caption", "themerex"),
						"description" => esc_html__("Caption for the button at the bottom of the block", "themerex"),
						"group" => esc_html__('Captions', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			
			vc_map( array(
				"base" => "trx_skills_item",
				"name" => esc_html__("Skill", "themerex"),
				"description" => esc_html__("Skills item", "themerex"),
				"show_settings_on_create" => true,
				"class" => "trx_sc_single trx_sc_skills_item",
				"content_element" => true,
				"is_container" => false,
				"as_child" => array('only' => 'trx_skills'),
				"as_parent" => array('except' => 'trx_skills'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for the current skills item", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "value",
						"heading" => esc_html__("Value", "themerex"),
						"description" => esc_html__("Value for the current skills item", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Color", "themerex"),
						"description" => esc_html__("Color for current skills item", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Background color for current skills item (only for type=pie)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "border_color",
						"heading" => esc_html__("Border color", "themerex"),
						"description" => esc_html__("Border color for current skills item (only for type=pie)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Counter style", "themerex"),
						"description" => esc_html__("Select style for the current skills item (only for type=counter)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(themerex_get_list_styles(1, 4)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Counter icon", "themerex"),
						"description" => esc_html__("Select icon from Fontello icons set, placed before counter (only for type=counter)", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css']
				)
			) );
			
			class WPBakeryShortCode_Trx_Skills extends THEMEREX_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Skills_Item extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Slider
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_slider",
				"name" => esc_html__("Slider", "themerex"),
				"description" => esc_html__("Insert slider", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_slider',
				"class" => "trx_sc_collection trx_sc_slider",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"as_parent" => array('only' => 'trx_slider_item'),
				"params" => array_merge(array(
					array(
						"param_name" => "engine",
						"heading" => esc_html__("Engine", "themerex"),
						"description" => esc_html__("Select engine for slider. Attention! Swiper is built-in engine, all other engines appears only if corresponding plugings are installed", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['sliders']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Float slider", "themerex"),
						"description" => esc_html__("Float slider to left or right side", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom slides", "themerex"),
						"description" => esc_html__("Make custom slides from inner shortcodes (prepare it on tabs) or prepare slides from posts thumbnails", "themerex"),
						"class" => "",
						"value" => array(esc_html__('Custom slides', 'themerex') => 'yes'),
						"type" => "checkbox"
					)
					),
					themerex_exists_revslider() ? array(
					array(
						"param_name" => "alias",
						"heading" => esc_html__("Revolution slider alias", "themerex"),
						"description" => esc_html__("Select Revolution slider to display", "themerex"),
						"admin_label" => true,
						"class" => "",
						'dependency' => array(
							'element' => 'engine',
							'value' => array('revo')
						),
						"value" => array_flip(themerex_array_merge(array('none' => esc_html__('- Select slider -', 'themerex')), $THEMEREX_GLOBALS['sc_params']['revo_sliders'])),
						"type" => "dropdown"
					)) : array(), array(
					array(
						"param_name" => "cat",
						"heading" => esc_html__("Categories list", "themerex"),
						"description" => esc_html__("Select category. If empty - show posts from any category or from IDs list", "themerex"),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array_flip(themerex_array_merge(array(0 => esc_html__('- Select category -', 'themerex')), $THEMEREX_GLOBALS['sc_params']['categories'])),
						"type" => "dropdown"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Swiper: Number of posts", "themerex"),
						"description" => esc_html__("How many posts will be displayed? If used IDs - this parameter ignored.", "themerex"),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "3",
						"type" => "textfield"
					),
					array(
						"param_name" => "offset",
						"heading" => esc_html__("Swiper: Offset before select posts", "themerex"),
						"description" => esc_html__("Skip posts before select next part.", "themerex"),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "orderby",
						"heading" => esc_html__("Swiper: Post sorting", "themerex"),
						"description" => esc_html__("Select desired posts sorting method", "themerex"),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['sorting']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "order",
						"heading" => esc_html__("Swiper: Post order", "themerex"),
						"description" => esc_html__("Select desired posts order", "themerex"),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "ids",
						"heading" => esc_html__("Swiper: Post IDs list", "themerex"),
						"description" => esc_html__("Comma separated list of posts ID. If set - parameters above are ignored!", "themerex"),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Swiper: Show slider controls", "themerex"),
						"description" => esc_html__("Show arrows inside slider", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(esc_html__('Show controls', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "pagination",
						"heading" => esc_html__("Swiper: Show slider pagination", "themerex"),
						"description" => esc_html__("Show bullets or titles to switch slides", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"std" => "no",
						"value" => array(
								esc_html__('None', 'themerex') => 'no',
								esc_html__('Dots', 'themerex') => 'yes',
								esc_html__('Side Titles', 'themerex') => 'full',
								esc_html__('Over Titles', 'themerex') => 'over'
							),
						"type" => "dropdown"
					),
					array(
						"param_name" => "titles",
						"heading" => esc_html__("Swiper: Show titles section", "themerex"),
						"description" => esc_html__("Show section with post's title and short post's description", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(
								esc_html__('Not show', 'themerex') => "no",
								esc_html__('Show/Hide info', 'themerex') => "slide",
								esc_html__('Fixed info', 'themerex') => "fixed"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "descriptions",
						"heading" => esc_html__("Swiper: Post descriptions", "themerex"),
						"description" => esc_html__("Show post's excerpt max length (characters)", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "links",
						"heading" => esc_html__("Swiper: Post's title as link", "themerex"),
						"description" => esc_html__("Make links from post's titles", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(esc_html__('Titles as a links', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "crop",
						"heading" => esc_html__("Swiper: Crop images", "themerex"),
						"description" => esc_html__("Crop images in each slide or live it unchanged", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(esc_html__('Crop images', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "autoheight",
						"heading" => esc_html__("Swiper: Autoheight", "themerex"),
						"description" => esc_html__("Change whole slider's height (make it equal current slide's height)", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => array(esc_html__('Autoheight', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					array(
						"param_name" => "slides_per_view",
						"heading" => esc_html__("Swiper: Slides per view", "themerex"),
						"description" => esc_html__("Slides per view showed in this slider", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "1",
						"type" => "textfield"
					),
					array(
						"param_name" => "slides_space",
						"heading" => esc_html__("Swiper: Space between slides", "themerex"),
						"description" => esc_html__("Size of space (in px) between slides", "themerex"),
						"admin_label" => true,
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "0",
						"type" => "textfield"
					),
					array(
						"param_name" => "interval",
						"heading" => esc_html__("Swiper: Slides change interval", "themerex"),
						"description" => esc_html__("Slides change interval (in milliseconds: 1000ms = 1s)", "themerex"),
						"group" => esc_html__('Details', 'themerex'),
						'dependency' => array(
							'element' => 'engine',
							'value' => array('swiper')
						),
						"class" => "",
						"value" => "5000",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				))
			) );
			
			
			vc_map( array(
				"base" => "trx_slider_item",
				"name" => esc_html__("Slide", "themerex"),
				"description" => esc_html__("Slider item - single slide", "themerex"),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				'icon' => 'icon_trx_slider_item',
				"as_child" => array('only' => 'trx_slider'),
				"as_parent" => array('except' => 'trx_slider'),
				"params" => array(
					array(
						"param_name" => "src",
						"heading" => esc_html__("URL (source) for image file", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for the current slide", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css']
				)
			) );
			
			class WPBakeryShortCode_Trx_Slider extends THEMEREX_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Slider_Item extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Socials
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_socials",
				"name" => esc_html__("Social icons", "themerex"),
				"description" => esc_html__("Custom social icons", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_socials',
				"class" => "trx_sc_collection trx_sc_socials",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => true,
				"as_parent" => array('only' => 'trx_social_item'),
				"params" => array_merge(array(
					array(
						"param_name" => "type",
						"heading" => esc_html__("Icon's type", "themerex"),
						"description" => esc_html__("Type of the icons - images or font icons", "themerex"),
						"class" => "",
						"std" => themerex_get_theme_setting('socials_type'),
						"value" => array(
							esc_html__('Icons', 'themerex') => 'icons',
							esc_html__('Images', 'themerex') => 'images'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "size",
						"heading" => esc_html__("Icon's size", "themerex"),
						"description" => esc_html__("Size of the icons", "themerex"),
						"class" => "",
						"std" => "small",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['sizes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "shape",
						"heading" => esc_html__("Icon's shape", "themerex"),
						"description" => esc_html__("Shape of the icons", "themerex"),
						"class" => "",
						"std" => "square",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['shapes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "socials",
						"heading" => esc_html__("Manual socials list", "themerex"),
						"description" => esc_html__("Custom list of social networks. For example: twitter=http://twitter.com/my_profile|facebook=http://facebooc.com/my_profile. If empty - use socials from Theme options.", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "custom",
						"heading" => esc_html__("Custom socials", "themerex"),
						"description" => esc_html__("Make custom icons from inner shortcodes (prepare it on tabs)", "themerex"),
						"class" => "",
						"value" => array(esc_html__('Custom socials', 'themerex') => 'yes'),
						"type" => "checkbox"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				))
			) );
			
			
			vc_map( array(
				"base" => "trx_social_item",
				"name" => esc_html__("Custom social item", "themerex"),
				"description" => esc_html__("Custom social item: name, profile url and icon url", "themerex"),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => false,
				'icon' => 'icon_trx_social_item',
				"as_child" => array('only' => 'trx_socials'),
				"as_parent" => array('except' => 'trx_socials'),
				"params" => array(
					array(
						"param_name" => "name",
						"heading" => esc_html__("Social name", "themerex"),
						"description" => esc_html__("Name (slug) of the social network (twitter, facebook, linkedin, etc.)", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "url",
						"heading" => esc_html__("Your profile URL", "themerex"),
						"description" => esc_html__("URL of your profile in specified social network", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("URL (source) for icon file", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for the current social icon", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					)
				)
			) );
			
			class WPBakeryShortCode_Trx_Socials extends THEMEREX_VC_ShortCodeCollection {}
			class WPBakeryShortCode_Trx_Social_Item extends THEMEREX_VC_ShortCodeSingle {}
			

			
			
			
			
			
			// Table
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_table",
				"name" => esc_html__("Table", "themerex"),
				"description" => esc_html__("Insert a table", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_table',
				"class" => "trx_sc_container trx_sc_table",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "align",
						"heading" => esc_html__("Cells content alignment", "themerex"),
						"description" => esc_html__("Select alignment for each table cell", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "content",
						"heading" => esc_html__("Table content", "themerex"),
						"description" => esc_html__("Content, created with any table-generator", "themerex"),
						"class" => "",
						"value" => "Paste here table content, generated on one of many public internet resources, for example: http://www.impressivewebs.com/html-table-code-generator/ or http://html-tables.com/",
						"type" => "textarea_html"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextContainerView'
			) );
			
			class WPBakeryShortCode_Trx_Table extends THEMEREX_VC_ShortCodeContainer {}
			
			
			
			
			
			
			
			// Tabs
			//-------------------------------------------------------------------------------------
			
			$tab_id_1 = 'sc_tab_'.time() . '_1_' . rand( 0, 100 );
			$tab_id_2 = 'sc_tab_'.time() . '_2_' . rand( 0, 100 );
			vc_map( array(
				"base" => "trx_tabs",
				"name" => esc_html__("Tabs", "themerex"),
				"description" => esc_html__("Tabs", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_tabs',
				"class" => "trx_sc_collection trx_sc_tabs",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_tab'),
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Tabs style", "themerex"),
						"description" => esc_html__("Select style of tabs items", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip(themerex_get_list_styles(1, 2)),
						"type" => "dropdown"
					),

					array(
						"param_name" => "initial",
						"heading" => esc_html__("Initially opened tab", "themerex"),
						"description" => esc_html__("Number of initially opened tab", "themerex"),
						"class" => "",
						"value" => 1,
						"type" => "textfield"
					),
					array(
						"param_name" => "scroll",
						"heading" => esc_html__("Scroller", "themerex"),
						"description" => esc_html__("Use scroller to show tab content (height parameter required)", "themerex"),
						"class" => "",
						"value" => array("Use scroller" => "yes" ),
						"type" => "checkbox"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_tab title="' . esc_html__( 'Tab 1', 'themerex' ) . '" tab_id="'.esc_attr($tab_id_1).'"][/trx_tab]
					[trx_tab title="' . esc_html__( 'Tab 2', 'themerex' ) . '" tab_id="'.esc_attr($tab_id_2).'"][/trx_tab]
				',
				"custom_markup" => '
					<div class="wpb_tabs_holder wpb_holder vc_container_for_children">
						<ul class="tabs_controls">
						</ul>
						%content%
					</div>
				',
				'js_view' => 'VcTrxTabsView'
			) );
			
			
			vc_map( array(
				"base" => "trx_tab",
				"name" => esc_html__("Tab item", "themerex"),
				"description" => esc_html__("Single tab item", "themerex"),
				"show_settings_on_create" => true,
				"class" => "trx_sc_collection trx_sc_tab",
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_tab',
				"as_child" => array('only' => 'trx_tabs'),
				"as_parent" => array('except' => 'trx_tabs'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Tab title", "themerex"),
						"description" => esc_html__("Title for current tab", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
                    array(
                        "param_name" => "icon_title",
                        "heading" => esc_html__("Icon before title", "themerex"),
                        "description" => esc_html__("Select icon for the title tabs item from Fontello icons set", "themerex"),
                        "class" => "",
                        "value" => $THEMEREX_GLOBALS['sc_params']['icons'],
                        "type" => "dropdown"
                    ),
					array(
						"param_name" => "tab_id",
						"heading" => esc_html__("Tab ID", "themerex"),
						"description" => esc_html__("ID for current tab (required). Please, start it from letter.", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css']
				),
			  'js_view' => 'VcTrxTabView'
			) );
			class WPBakeryShortCode_Trx_Tabs extends THEMEREX_VC_ShortCodeTabs {}
			class WPBakeryShortCode_Trx_Tab extends THEMEREX_VC_ShortCodeTab {}
			
			
			
			
			
			
			
			// Title
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_title",
				"name" => esc_html__("Title", "themerex"),
				"description" => esc_html__("Create header tag (1-6 level) with many styles", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_title',
				"class" => "trx_sc_single trx_sc_title",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "content",
						"heading" => esc_html__("Title content", "themerex"),
						"description" => esc_html__("Title content", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textarea_html"
					),
					array(
						"param_name" => "type",
						"heading" => esc_html__("Title type", "themerex"),
						"description" => esc_html__("Title type (header level)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							esc_html__('Header 1', 'themerex') => '1',
							esc_html__('Header 2', 'themerex') => '2',
							esc_html__('Header 3', 'themerex') => '3',
							esc_html__('Header 4', 'themerex') => '4',
							esc_html__('Header 5', 'themerex') => '5',
							esc_html__('Header 6', 'themerex') => '6'
						),
						"type" => "dropdown"
					),
                    array(
                        "param_name" => "devider_styling",
                        "heading" => esc_html__("Additional devider", "themerex"),
                        "description" => esc_html__("Select devider for title.", "themerex"),
                        "class" => "",
                        "value" => array(
                            esc_html__('None', 'themerex') => '0',
                            esc_html__('Line before title', 'themerex') => '1',
                            esc_html__('Devider after title', 'themerex') => '2'
                        ),
                        "type" => "dropdown"
                    ),
					array(
						"param_name" => "style",
						"heading" => esc_html__("Title style", "themerex"),
						"description" => esc_html__("Title style: only text (regular) or with icon/image (iconed)", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array(
							esc_html__('Regular', 'themerex') => 'regular',
							esc_html__('Underline', 'themerex') => 'underline',
							esc_html__('Divider', 'themerex') => 'divider',
							esc_html__('With icon (image)', 'themerex') => 'iconed'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Title text alignment", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "font_size",
						"heading" => esc_html__("Font size", "themerex"),
						"description" => esc_html__("Custom font size. If empty - use theme default", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "font_weight",
						"heading" => esc_html__("Font weight", "themerex"),
						"description" => esc_html__("Custom font weight. If empty or inherit - use theme default", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('Default', 'themerex') => 'inherit',
							esc_html__('Thin (100)', 'themerex') => '100',
							esc_html__('Light (300)', 'themerex') => '300',
							esc_html__('Normal (400)', 'themerex') => '400',
							esc_html__('Semibold (600)', 'themerex') => '600',
							esc_html__('Bold (700)', 'themerex') => '700',
							esc_html__('Black (900)', 'themerex') => '900'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "color",
						"heading" => esc_html__("Title color", "themerex"),
						"description" => esc_html__("Select color for the title", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "icon",
						"heading" => esc_html__("Title font icon", "themerex"),
						"description" => esc_html__("Select font icon for the title from Fontello icons set (if style=iconed)", "themerex"),
						"class" => "",
						"group" => esc_html__('Icon &amp; Image', 'themerex'),
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("or image icon", "themerex"),
						"description" => esc_html__("Select image icon for the title instead icon above (if style=iconed)", "themerex"),
						"class" => "",
						"group" => esc_html__('Icon &amp; Image', 'themerex'),
						'dependency' => array(
							'element' => 'style',
							'value' => array('iconed')
						),
						"value" => $THEMEREX_GLOBALS['sc_params']['images'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "picture",
						"heading" => esc_html__("or select uploaded image", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site (if style=iconed)", "themerex"),
						"group" => esc_html__('Icon &amp; Image', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "image_size",
						"heading" => esc_html__("Image (picture) size", "themerex"),
						"description" => esc_html__("Select image (picture) size (if style=iconed)", "themerex"),
						"group" => esc_html__('Icon &amp; Image', 'themerex'),
						"class" => "",
						"value" => array(
							esc_html__('Small', 'themerex') => 'small',
							esc_html__('Medium', 'themerex') => 'medium',
							esc_html__('Large', 'themerex') => 'large'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "position",
						"heading" => esc_html__("Icon (image) position", "themerex"),
						"description" => esc_html__("Select icon (image) position (if style=iconed)", "themerex"),
						"group" => esc_html__('Icon &amp; Image', 'themerex'),
						"class" => "",
						"value" => array(
							esc_html__('Top', 'themerex') => 'top',
							esc_html__('Left', 'themerex') => 'left'
						),
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'js_view' => 'VcTrxTextView'
			) );
			
			class WPBakeryShortCode_Trx_Title extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Toggles
			//-------------------------------------------------------------------------------------
				
			vc_map( array(
				"base" => "trx_toggles",
				"name" => esc_html__("Toggles", "themerex"),
				"description" => esc_html__("Toggles items", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_toggles',
				"class" => "trx_sc_collection trx_sc_toggles",
				"content_element" => true,
				"is_container" => true,
				"show_settings_on_create" => false,
				"as_parent" => array('only' => 'trx_toggles_item'),
				"params" => array(
					array(
						"param_name" => "style",
						"heading" => esc_html__("Toggles style", "themerex"),
						"description" => esc_html__("Select style for display toggles", "themerex"),
						"class" => "",
						"admin_label" => true,
						"value" => array_flip(themerex_get_list_styles(1, 2)),
						"type" => "dropdown"
					),
					array(
						"param_name" => "counter",
						"heading" => esc_html__("Counter", "themerex"),
						"description" => esc_html__("Display counter before each toggles title", "themerex"),
						"class" => "",
						"value" => array("Add item numbers before each element" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "icon_closed",
						"heading" => esc_html__("Icon while closed", "themerex"),
						"description" => esc_html__("Select icon for the closed toggles item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", "themerex"),
						"description" => esc_html__("Select icon for the opened toggles item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
				'default_content' => '
					[trx_toggles_item title="' . esc_html__( 'Item 1 title', 'themerex' ) . '"][/trx_toggles_item]
					[trx_toggles_item title="' . esc_html__( 'Item 2 title', 'themerex' ) . '"][/trx_toggles_item]
				',
				"custom_markup" => '
					<div class="wpb_accordion_holder wpb_holder clearfix vc_container_for_children">
						%content%
					</div>
					<div class="tab_controls">
						<button class="add_tab" title="'.esc_html__("Add item", "themerex").'">'.esc_html__("Add item", "themerex").'</button>
					</div>
				',
				'js_view' => 'VcTrxTogglesView'
			) );
			
			
			vc_map( array(
				"base" => "trx_toggles_item",
				"name" => esc_html__("Toggles item", "themerex"),
				"description" => esc_html__("Single toggles item", "themerex"),
				"show_settings_on_create" => true,
				"content_element" => true,
				"is_container" => true,
				'icon' => 'icon_trx_toggles_item',
				"as_child" => array('only' => 'trx_toggles'),
				"as_parent" => array('except' => 'trx_toggles'),
				"params" => array(
					array(
						"param_name" => "title",
						"heading" => esc_html__("Title", "themerex"),
						"description" => esc_html__("Title for current toggles item", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "open",
						"heading" => esc_html__("Open on show", "themerex"),
						"description" => esc_html__("Open current toggle item on show", "themerex"),
						"class" => "",
						"value" => array("Opened" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "icon_closed",
						"heading" => esc_html__("Icon while closed", "themerex"),
						"description" => esc_html__("Select icon for the closed toggles item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					array(
						"param_name" => "icon_opened",
						"heading" => esc_html__("Icon while opened", "themerex"),
						"description" => esc_html__("Select icon for the opened toggles item from Fontello icons set", "themerex"),
						"class" => "",
						"value" => $THEMEREX_GLOBALS['sc_params']['icons'],
						"type" => "dropdown"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['css']
				),
				'js_view' => 'VcTrxTogglesTabView'
			) );
			class WPBakeryShortCode_Trx_Toggles extends THEMEREX_VC_ShortCodeToggles {}
			class WPBakeryShortCode_Trx_Toggles_Item extends THEMEREX_VC_ShortCodeTogglesItem {}
			
			
			
			
			
			
			// Twitter
			//-------------------------------------------------------------------------------------

			vc_map( array(
				"base" => "trx_twitter",
				"name" => esc_html__("Twitter", "themerex"),
				"description" => esc_html__("Insert twitter feed into post (page)", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_twitter',
				"class" => "trx_sc_single trx_sc_twitter",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "user",
						"heading" => esc_html__("Twitter Username", "themerex"),
						"description" => esc_html__("Your username in the twitter account. If empty - get it from Theme Options.", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "consumer_key",
						"heading" => esc_html__("Consumer Key", "themerex"),
						"description" => esc_html__("Consumer Key from the twitter account", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "consumer_secret",
						"heading" => esc_html__("Consumer Secret", "themerex"),
						"description" => esc_html__("Consumer Secret from the twitter account", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "token_key",
						"heading" => esc_html__("Token Key", "themerex"),
						"description" => esc_html__("Token Key from the twitter account", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "token_secret",
						"heading" => esc_html__("Token Secret", "themerex"),
						"description" => esc_html__("Token Secret from the twitter account", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "count",
						"heading" => esc_html__("Tweets number", "themerex"),
						"description" => esc_html__("Number tweets to show", "themerex"),
						"class" => "",
						"divider" => true,
						"value" => 3,
						"type" => "textfield"
					),
					array(
						"param_name" => "controls",
						"heading" => esc_html__("Show arrows", "themerex"),
						"description" => esc_html__("Show control buttons", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['yes_no']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "interval",
						"heading" => esc_html__("Tweets change interval", "themerex"),
						"description" => esc_html__("Tweets change interval (in milliseconds: 1000ms = 1s)", "themerex"),
						"class" => "",
						"value" => "7000",
						"type" => "textfield"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Alignment of the tweets block", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "autoheight",
						"heading" => esc_html__("Autoheight", "themerex"),
						"description" => esc_html__("Change whole slider's height (make it equal current slide's height)", "themerex"),
						"class" => "",
						"value" => array("Autoheight" => "yes" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "scheme",
						"heading" => esc_html__("Color scheme", "themerex"),
						"description" => esc_html__("Select color scheme for this block", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['schemes']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "bg_color",
						"heading" => esc_html__("Background color", "themerex"),
						"description" => esc_html__("Any background color for this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "colorpicker"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image URL", "themerex"),
						"description" => esc_html__("Select background image from library for this section", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_overlay",
						"heading" => esc_html__("Overlay", "themerex"),
						"description" => esc_html__("Overlay color opacity (from 0.0 to 1.0)", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_texture",
						"heading" => esc_html__("Texture", "themerex"),
						"description" => esc_html__("Texture style from 1 to 11. Empty or 0 - without texture.", "themerex"),
						"group" => esc_html__('Colors and Images', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				),
			) );
			
			class WPBakeryShortCode_Trx_Twitter extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Video
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_video",
				"name" => esc_html__("Video", "themerex"),
				"description" => esc_html__("Insert video player", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_video',
				"class" => "trx_sc_single trx_sc_video",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "url",
						"heading" => esc_html__("URL for video file", "themerex"),
						"description" => esc_html__("Paste URL for video file", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "ratio",
						"heading" => esc_html__("Ratio", "themerex"),
						"description" => esc_html__("Select ratio for display video", "themerex"),
						"class" => "",
						"value" => array(
							esc_html__('16:9', 'themerex') => "16:9",
							esc_html__('4:3', 'themerex') => "4:3"
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "autoplay",
						"heading" => esc_html__("Autoplay video", "themerex"),
						"description" => esc_html__("Autoplay video on page load", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array("Autoplay" => "on" ),
						"type" => "checkbox"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Select block alignment", "themerex"),
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['align']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "image",
						"heading" => esc_html__("Cover image", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for video preview", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for video background. Attention! If you use background image - specify paddings below from background margins to video block in percents!", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_top",
						"heading" => esc_html__("Top offset", "themerex"),
						"description" => esc_html__("Top offset (padding) from background image to video block (in percent). For example: 3%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_bottom",
						"heading" => esc_html__("Bottom offset", "themerex"),
						"description" => esc_html__("Bottom offset (padding) from background image to video block (in percent). For example: 3%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_left",
						"heading" => esc_html__("Left offset", "themerex"),
						"description" => esc_html__("Left offset (padding) from background image to video block (in percent). For example: 20%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_right",
						"heading" => esc_html__("Right offset", "themerex"),
						"description" => esc_html__("Right offset (padding) from background image to video block (in percent). For example: 12%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Video extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
			
			
			
			// Zoom
			//-------------------------------------------------------------------------------------
			
			vc_map( array(
				"base" => "trx_zoom",
				"name" => esc_html__("Zoom", "themerex"),
				"description" => esc_html__("Insert the image with zoom/lens effect", "themerex"),
				"category" => esc_html__('Content', 'js_composer'),
				'icon' => 'icon_trx_zoom',
				"class" => "trx_sc_single trx_sc_zoom",
				"content_element" => true,
				"is_container" => false,
				"show_settings_on_create" => true,
				"params" => array(
					array(
						"param_name" => "effect",
						"heading" => esc_html__("Effect", "themerex"),
						"description" => esc_html__("Select effect to display overlapping image", "themerex"),
						"admin_label" => true,
						"class" => "",
						"std" => "zoom",
						"value" => array(
							esc_html__('Lens', 'themerex') => 'lens',
							esc_html__('Zoom', 'themerex') => 'zoom'
						),
						"type" => "dropdown"
					),
					array(
						"param_name" => "url",
						"heading" => esc_html__("Main image", "themerex"),
						"description" => esc_html__("Select or upload main image", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "over",
						"heading" => esc_html__("Overlaping image", "themerex"),
						"description" => esc_html__("Select or upload overlaping image", "themerex"),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "align",
						"heading" => esc_html__("Alignment", "themerex"),
						"description" => esc_html__("Float zoom to left or right side", "themerex"),
						"admin_label" => true,
						"class" => "",
						"value" => array_flip($THEMEREX_GLOBALS['sc_params']['float']),
						"type" => "dropdown"
					),
					array(
						"param_name" => "bg_image",
						"heading" => esc_html__("Background image", "themerex"),
						"description" => esc_html__("Select or upload image or write URL from other site for zoom background. Attention! If you use background image - specify paddings below from background margins to video block in percents!", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "attach_image"
					),
					array(
						"param_name" => "bg_top",
						"heading" => esc_html__("Top offset", "themerex"),
						"description" => esc_html__("Top offset (padding) from background image to zoom block (in percent). For example: 3%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_bottom",
						"heading" => esc_html__("Bottom offset", "themerex"),
						"description" => esc_html__("Bottom offset (padding) from background image to zoom block (in percent). For example: 3%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_left",
						"heading" => esc_html__("Left offset", "themerex"),
						"description" => esc_html__("Left offset (padding) from background image to zoom block (in percent). For example: 20%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					array(
						"param_name" => "bg_right",
						"heading" => esc_html__("Right offset", "themerex"),
						"description" => esc_html__("Right offset (padding) from background image to zoom block (in percent). For example: 12%", "themerex"),
						"group" => esc_html__('Background', 'themerex'),
						"class" => "",
						"value" => "",
						"type" => "textfield"
					),
					$THEMEREX_GLOBALS['vc_params']['id'],
					$THEMEREX_GLOBALS['vc_params']['class'],
					$THEMEREX_GLOBALS['vc_params']['animation'],
					$THEMEREX_GLOBALS['vc_params']['css'],
					themerex_vc_width(),
					themerex_vc_height(),
					$THEMEREX_GLOBALS['vc_params']['margin_top'],
					$THEMEREX_GLOBALS['vc_params']['margin_bottom'],
					$THEMEREX_GLOBALS['vc_params']['margin_left'],
					$THEMEREX_GLOBALS['vc_params']['margin_right']
				)
			) );
			
			class WPBakeryShortCode_Trx_Zoom extends THEMEREX_VC_ShortCodeSingle {}
			

			do_action('themerex_action_shortcodes_list_vc');
			
			
			if (false && themerex_exists_woocommerce()) {
			
				// WooCommerce - Cart
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_cart",
					"name" => esc_html__("Cart", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show cart page", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_wooc_cart',
					"class" => "trx_sc_alone trx_sc_woocommerce_cart",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", "themerex"),
							"description" => esc_html__("Dummy data - not used in shortcodes", "themerex"),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_Cart extends THEMEREX_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Checkout
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_checkout",
					"name" => esc_html__("Checkout", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show checkout page", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_wooc_checkout',
					"class" => "trx_sc_alone trx_sc_woocommerce_checkout",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", "themerex"),
							"description" => esc_html__("Dummy data - not used in shortcodes", "themerex"),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_Checkout extends THEMEREX_VC_ShortCodeAlone {}
			
			
				// WooCommerce - My Account
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_my_account",
					"name" => esc_html__("My Account", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show my account page", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_wooc_my_account',
					"class" => "trx_sc_alone trx_sc_woocommerce_my_account",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", "themerex"),
							"description" => esc_html__("Dummy data - not used in shortcodes", "themerex"),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_My_Account extends THEMEREX_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Order Tracking
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "woocommerce_order_tracking",
					"name" => esc_html__("Order Tracking", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show order tracking page", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_wooc_order_tracking',
					"class" => "trx_sc_alone trx_sc_woocommerce_order_tracking",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", "themerex"),
							"description" => esc_html__("Dummy data - not used in shortcodes", "themerex"),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Woocommerce_Order_Tracking extends THEMEREX_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Shop Messages
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "shop_messages",
					"name" => esc_html__("Shop Messages", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show shop messages", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_wooc_shop_messages',
					"class" => "trx_sc_alone trx_sc_shop_messages",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => false,
					"params" => array(
						array(
							"param_name" => "dummy",
							"heading" => esc_html__("Dummy data", "themerex"),
							"description" => esc_html__("Dummy data - not used in shortcodes", "themerex"),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Shop_Messages extends THEMEREX_VC_ShortCodeAlone {}
			
			
				// WooCommerce - Product Page
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_page",
					"name" => esc_html__("Product Page", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: display single product page", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_product_page',
					"class" => "trx_sc_single trx_sc_product_page",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "sku",
							"heading" => esc_html__("SKU", "themerex"),
							"description" => esc_html__("SKU code of displayed product", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "id",
							"heading" => esc_html__("ID", "themerex"),
							"description" => esc_html__("ID of displayed product", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "posts_per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "post_type",
							"heading" => esc_html__("Post type", "themerex"),
							"description" => esc_html__("Post type for the WP query (leave 'product')", "themerex"),
							"class" => "",
							"value" => "product",
							"type" => "textfield"
						),
						array(
							"param_name" => "post_status",
							"heading" => esc_html__("Post status", "themerex"),
							"description" => esc_html__("Display posts only with this status", "themerex"),
							"class" => "",
							"value" => array(
								esc_html__('Publish', 'themerex') => 'publish',
								esc_html__('Protected', 'themerex') => 'protected',
								esc_html__('Private', 'themerex') => 'private',
								esc_html__('Pending', 'themerex') => 'pending',
								esc_html__('Draft', 'themerex') => 'draft'
							),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Product_Page extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Product
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product",
					"name" => esc_html__("Product", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: display one product", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_product',
					"class" => "trx_sc_single trx_sc_product",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "sku",
							"heading" => esc_html__("SKU", "themerex"),
							"description" => esc_html__("Product's SKU code", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "id",
							"heading" => esc_html__("ID", "themerex"),
							"description" => esc_html__("Product's ID", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Product extends THEMEREX_VC_ShortCodeSingle {}
			
			
				// WooCommerce - Best Selling Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "best_selling_products",
					"name" => esc_html__("Best Selling Products", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show best selling products", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_best_selling_products',
					"class" => "trx_sc_single trx_sc_best_selling_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Best_Selling_Products extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Recent Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "recent_products",
					"name" => esc_html__("Recent Products", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show recent products", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_recent_products',
					"class" => "trx_sc_single trx_sc_recent_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Recent_Products extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Related Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "related_products",
					"name" => esc_html__("Related Products", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show related products", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_related_products',
					"class" => "trx_sc_single trx_sc_related_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "posts_per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Related_Products extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Featured Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "featured_products",
					"name" => esc_html__("Featured Products", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show featured products", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_featured_products',
					"class" => "trx_sc_single trx_sc_featured_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Featured_Products extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Top Rated Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "top_rated_products",
					"name" => esc_html__("Top Rated Products", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show top rated products", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_top_rated_products',
					"class" => "trx_sc_single trx_sc_top_rated_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Top_Rated_Products extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Sale Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "sale_products",
					"name" => esc_html__("Sale Products", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: list products on sale", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_sale_products',
					"class" => "trx_sc_single trx_sc_sale_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Sale_Products extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Product Category
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_category",
					"name" => esc_html__("Products from category", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: list products in specified category(-ies)", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_product_category',
					"class" => "trx_sc_single trx_sc_product_category",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						),
						array(
							"param_name" => "category",
							"heading" => esc_html__("Categories", "themerex"),
							"description" => esc_html__("Comma separated category slugs", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "operator",
							"heading" => esc_html__("Operator", "themerex"),
							"description" => esc_html__("Categories operator", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('IN', 'themerex') => 'IN',
								esc_html__('NOT IN', 'themerex') => 'NOT IN',
								esc_html__('AND', 'themerex') => 'AND'
							),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Product_Category extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Products
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "products",
					"name" => esc_html__("Products", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: list all products", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_products',
					"class" => "trx_sc_single trx_sc_products",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "skus",
							"heading" => esc_html__("SKUs", "themerex"),
							"description" => esc_html__("Comma separated SKU codes of products", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "ids",
							"heading" => esc_html__("IDs", "themerex"),
							"description" => esc_html__("Comma separated ID of products", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						)
					)
				) );
				
				class WPBakeryShortCode_Products extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
			
				// WooCommerce - Product Attribute
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_attribute",
					"name" => esc_html__("Products by Attribute", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show products with specified attribute", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_product_attribute',
					"class" => "trx_sc_single trx_sc_product_attribute",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "per_page",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many products showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						),
						array(
							"param_name" => "attribute",
							"heading" => esc_html__("Attribute", "themerex"),
							"description" => esc_html__("Attribute name", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "filter",
							"heading" => esc_html__("Filter", "themerex"),
							"description" => esc_html__("Attribute value", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Product_Attribute extends THEMEREX_VC_ShortCodeSingle {}
			
			
			
				// WooCommerce - Products Categories
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "product_categories",
					"name" => esc_html__("Product Categories", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: show categories with products", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_product_categories',
					"class" => "trx_sc_single trx_sc_product_categories",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "number",
							"heading" => esc_html__("Number", "themerex"),
							"description" => esc_html__("How many categories showed", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "4",
							"type" => "textfield"
						),
						array(
							"param_name" => "columns",
							"heading" => esc_html__("Columns", "themerex"),
							"description" => esc_html__("How many columns per row use for categories output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "orderby",
							"heading" => esc_html__("Order by", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array(
								esc_html__('Date', 'themerex') => 'date',
								esc_html__('Title', 'themerex') => 'title'
							),
							"type" => "dropdown"
						),
						array(
							"param_name" => "order",
							"heading" => esc_html__("Order", "themerex"),
							"description" => esc_html__("Sorting order for products output", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => array_flip($THEMEREX_GLOBALS['sc_params']['ordering']),
							"type" => "dropdown"
						),
						array(
							"param_name" => "parent",
							"heading" => esc_html__("Parent", "themerex"),
							"description" => esc_html__("Parent category slug", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "date",
							"type" => "textfield"
						),
						array(
							"param_name" => "ids",
							"heading" => esc_html__("IDs", "themerex"),
							"description" => esc_html__("Comma separated ID of products", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "hide_empty",
							"heading" => esc_html__("Hide empty", "themerex"),
							"description" => esc_html__("Hide empty categories", "themerex"),
							"class" => "",
							"value" => array("Hide empty" => "1" ),
							"type" => "checkbox"
						)
					)
				) );
				
				class WPBakeryShortCode_Products_Categories extends THEMEREX_VC_ShortCodeSingle {}
			
				/*
			
				// WooCommerce - Add to cart
				//-------------------------------------------------------------------------------------
				
				vc_map( array(
					"base" => "add_to_cart",
					"name" => esc_html__("Add to cart", "themerex"),
					"description" => esc_html__("WooCommerce shortcode: Display a single product price + cart button", "themerex"),
					"category" => esc_html__('WooCommerce', 'js_composer'),
					'icon' => 'icon_trx_add_to_cart',
					"class" => "trx_sc_single trx_sc_add_to_cart",
					"content_element" => true,
					"is_container" => false,
					"show_settings_on_create" => true,
					"params" => array(
						array(
							"param_name" => "id",
							"heading" => esc_html__("ID", "themerex"),
							"description" => esc_html__("Product's ID", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "sku",
							"heading" => esc_html__("SKU", "themerex"),
							"description" => esc_html__("Product's SKU code", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "quantity",
							"heading" => esc_html__("Quantity", "themerex"),
							"description" => esc_html__("How many item add", "themerex"),
							"admin_label" => true,
							"class" => "",
							"value" => "1",
							"type" => "textfield"
						),
						array(
							"param_name" => "show_price",
							"heading" => esc_html__("Show price", "themerex"),
							"description" => esc_html__("Show price near button", "themerex"),
							"class" => "",
							"value" => array("Show price" => "true" ),
							"type" => "checkbox"
						),
						array(
							"param_name" => "class",
							"heading" => esc_html__("Class", "themerex"),
							"description" => esc_html__("CSS class", "themerex"),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						),
						array(
							"param_name" => "style",
							"heading" => esc_html__("CSS style", "themerex"),
							"description" => esc_html__("CSS style for additional decoration", "themerex"),
							"class" => "",
							"value" => "",
							"type" => "textfield"
						)
					)
				) );
				
				class WPBakeryShortCode_Add_To_Cart extends THEMEREX_VC_ShortCodeSingle {}
				*/
			}

		}
	}
}
?>