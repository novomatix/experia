Version 1.0
	Release

Version 1.1
	+ VC Frontend editor compatibility improved.
	+ WordPress Customizer compatibility improved.
	+ Tribe Events compatibility improved.
	+ Extra column is added in categories, posts and pages list, where changed (overriden) theme options are displayed.

Version 1.2
    + VC plugin update - Translation fixed

    Version 1.2.1
        + Translation fixed

    Version 1.2.2
        + Documentation update