<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'experia_db');

/** MySQL database username */
define('DB_USER', 'experia_user');

/** MySQL database password */
define('DB_PASSWORD', 'xUFuXPfAcvnryPCJ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'pP+/+Y i8z*t$P.8 h4%X<^.D6j6eG^aie:8hBnquVv1aYMt3`Zv5s tZ6_rFn*K');
define('SECURE_AUTH_KEY',  'I}3==dFi|rRMA%uTJ3T82[GdoD#-Vly&8S}?]_t8axM%|*?f+}u_Nc9q$YbKW%Q@');
define('LOGGED_IN_KEY',    'QH[2+DTWSD!-!xt2*U:<@ 19%V<^!gm+@Pyqg4g,GODl10GbnB-CcT`e*a=&&A~/');
define('NONCE_KEY',        '<[m&,rK@6yjnW_et67]b3PJQHsC-c[i(WY2XF#|z38`Q;U(>Hj,%qIR[8fa| K6 ');
define('AUTH_SALT',        '%AHQnj%gf$+c[WLF)&8(u1*&O1(?PL/<)kOG9>}A)ml%6S]<J6~2)xPGZm(-8Ryc');
define('SECURE_AUTH_SALT', 'KL`q#avJu0ZsWw-6AxJh*@lCf;ju>O{+1HT3XoLwaMnsb7yNcbTKQ(6f8C<sDq1W');
define('LOGGED_IN_SALT',   'xm<d|0.o>7fajhdu=-UUz[IEZ2e%on^3rn[L5PJ;oFP?HwS,LtK+R%d0e9Tp};])');
define('NONCE_SALT',       'oLGA3$!qzBCTqw#MFUL8!VW|(<*DQK[^Z7r>n@*Qk$9emZ)?v-ZZlhXA>B)<sVO-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'xp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
