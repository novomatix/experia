<?php
/* Revolution Slider support functions
------------------------------------------------------------------------------- */

// Check if RevSlider installed and activated
if ( !function_exists( 'themerex_exists_revslider' ) ) {
	function themerex_exists_revslider() {
		return function_exists('rev_slider_shortcode');
		//return class_exists('RevSliderFront');
		//return is_plugin_active('revslider/revslider.php');
	}
}

// Return Revo Sliders list, prepended inherit (if need)
if ( !function_exists( 'themerex_get_list_revo_sliders' ) ) {
	function themerex_get_list_revo_sliders($prepend_inherit=false) {
		global $THEMEREX_GLOBALS;
		if (isset($THEMEREX_GLOBALS['list_revo_sliders']))
			$list = $THEMEREX_GLOBALS['list_revo_sliders'];
		else {
			$list = array();
			if (themerex_exists_revslider()) {
				global $wpdb;
				$rows = $wpdb->get_results( "SELECT alias, title FROM " . esc_sql($wpdb->prefix) . "revslider_sliders" );
				if (is_array($rows) && count($rows) > 0) {
					foreach ($rows as $row) {
						$list[$row->alias] = $row->title;
					}
				}
			}
			$THEMEREX_GLOBALS['list_revo_sliders'] = $list = apply_filters('themerex_filter_list_revo_sliders', $list);
		}
		return $prepend_inherit ? themerex_array_merge(array('inherit' => esc_html__("Inherit", 'themerex')), $list) : $list;
	}
}
?>