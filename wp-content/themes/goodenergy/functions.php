<?php
/**
 * Theme sprecific functions and definitions
 */


/* Theme setup section
------------------------------------------------------------------- */

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) $content_width = 1170; /* pixels */

// Add theme specific actions and filters
// Attention! Function were add theme specific actions and filters handlers must have priority 1
if ( !function_exists( 'themerex_theme_setup' ) ) {
	add_action( 'themerex_action_before_init_theme', 'themerex_theme_setup', 1 );
	function themerex_theme_setup() {

		// Register theme menus
		add_filter( 'themerex_filter_add_theme_menus',		'themerex_add_theme_menus' );

		// Register theme sidebars
		add_filter( 'themerex_filter_add_theme_sidebars',	'themerex_add_theme_sidebars' );

		// Set options for importer
		add_filter( 'themerex_filter_importer_options',		'themerex_set_importer_options' );

	}
}


// Add/Remove theme nav menus
if ( !function_exists( 'themerex_add_theme_menus' ) ) {
	//add_filter( 'themerex_filter_add_theme_menus', 'themerex_add_theme_menus' );
	function themerex_add_theme_menus($menus) {
		//For example:
		//$menus['menu_footer'] = esc_html__('Footer Menu', 'themerex');
		//if (isset($menus['menu_panel'])) unset($menus['menu_panel']);
		return $menus;
	}
}


// Add theme specific widgetized areas
if ( !function_exists( 'themerex_add_theme_sidebars' ) ) {
	//add_filter( 'themerex_filter_add_theme_sidebars',	'themerex_add_theme_sidebars' );
	function themerex_add_theme_sidebars($sidebars=array()) {
		if (is_array($sidebars)) {
			$theme_sidebars = array(
				'sidebar_main'		=> esc_html__( 'Main Sidebar', 'themerex' ),
				'sidebar_outer'		=> esc_html__( 'Outer Sidebar', 'themerex' ),
				'sidebar_footer'	=> esc_html__( 'Footer Sidebar', 'themerex' )
			);
			if (themerex_exists_woocommerce()) {
				$theme_sidebars['sidebar_cart']  = esc_html__( 'WooCommerce Cart Sidebar', 'themerex' );
			}
			$sidebars = array_merge($theme_sidebars, $sidebars);
		}
		return $sidebars;
	}
}


// Set theme specific importer options
if ( !function_exists( 'themerex_set_importer_options' ) ) {
	//add_filter( 'themerex_filter_importer_options',	'themerex_set_importer_options' );
	function themerex_set_importer_options($options=array()) {
		if (is_array($options)) {
			$options['domain_dev'] = '_goodenergy.themerex.dnw';
			$options['domain_demo'] = 'goodenergy.themerex.net';
			$options['page_on_front'] = 'Home';	// Homepage title
			$options['page_for_posts'] = 'All posts';		// Blog streampage title
			$options['menus'] = array(						// Menus locations and names
				'menu-main'	  => 'Main Menu'


			);
		}
		return $options;
	}
}


/* Include framework core files
------------------------------------------------------------------- */
// If now is WP Heartbeat call - skip loading theme core files
if (!isset($_POST['action']) || $_POST['action']!="heartbeat") {
     require_once get_template_directory().'/fw/loader.php';
}
?>